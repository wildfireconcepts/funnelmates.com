<?php

$photo    = $module->get_data();
$classes  = $module->get_classes();
$src      = $module->get_src();
$link     = $module->get_link();
$alt      = $module->get_alt();
$attrs    = $module->get_attributes();
$filetype = pathinfo( $src, PATHINFO_EXTENSION );
$rel      = $module->get_rel();
$caption  = $module->get_caption();
$linktype  = $module->get_linktype();
$aff_network  = $module->get_affiliate_network();

?>
<div class="fl-photo<?php echo ( ! empty( $settings->crop ) ) ? ' fl-photo-crop-' . $settings->crop : ''; ?> fl-photo-align-<?php echo $settings->align; ?>"<?php FLBuilder::print_schema( ' itemscope itemtype="https://schema.org/ImageObject"' ); ?>>
	<div class="fl-photo-content fl-photo-img-<?php echo $filetype; ?>">
		<?php if ( ! empty( $link ) ) : 
			if($linktype == 'affiliate_link' || $linktype == 'add_new_affiliate_link'){
				// if($aff_network == ''){
					global $wpdb;
					$gettype = $wpdb->get_row("SELECT aff_network FROM `wp_setc_website_links` WHERE link_identifier='$link'");
					if($gettype->aff_network == 'jvzoo'){
						$aff_network = 'jvz';
					}else if($gettype->aff_network == 'clickbank'){
						$aff_network = 'cb';
					}else if($gettype->aff_network == 'warriorplus'){
						$aff_network = 'wp';
					}else if($gettype->aff_network == 'paykickstart'){
						$aff_network = 'pks';
					}else if($gettype->aff_network == 'external'){
						$aff_network = 'ex';
					}
				/* }else{
					if($aff_network == 'jvzoo'){
						$aff_network = 'jvz';
					}else if($aff_network == 'clickbank'){
						$aff_network = 'cb';
					}else if($aff_network == 'warriorplus'){
						$aff_network = 'wp';
					}else if($aff_network == 'external'){
						$aff_network = 'ex';
					}
				} */
				?>
				<a href="<?php echo site_url().'/'.$aff_network.'/'.$_COOKIE['CCID'].'/'.$link; ?>" target="_blank"<?php echo $rel; ?> itemprop="url">
			<?php }else{ ?>
				<a href="<?php echo $link; ?>" target="_blank"<?php echo $rel; ?> itemprop="url">
			<?php } 
		endif; ?>
		<img class="<?php echo $classes; ?>" src="<?php echo $src; ?>" alt="<?php echo $alt; ?>" itemprop="image" <?php echo $attrs; ?> />
		<?php if ( ! empty( $link ) ) : ?>
			</a>
		<?php endif; ?>
		<?php if ( 'hover' === $settings->show_caption ) : ?>
		<div class="fl-photo-caption fl-photo-caption-hover" itemprop="caption"><?php echo $caption; ?></div>
		<?php endif; ?>
	</div>
	<?php if ( 'below' === $settings->show_caption ) : ?>
	<div class="fl-photo-caption fl-photo-caption-below" itemprop="caption"><?php echo $caption; ?></div>
	<?php endif; ?>
</div>
