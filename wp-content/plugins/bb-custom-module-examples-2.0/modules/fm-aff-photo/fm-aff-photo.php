<?php

/**
 * @class FLPhotoModule
 */
class FLFMAffPhotoModule extends FLBuilderModule {

	/**
	 * @property $data
	 */
	public $data = null;

	/**
	 * @property $_editor
	 * @protected
	 */
	protected $_editor = null;

	/**
	 * @method __construct
	 */
	public function __construct() {
		parent::__construct(array(
			'name'            => __( 'Affiliate Photo', 'fl-builder' ),
			'description'     => __( 'Upload a photo or display one from the media library.', 'fl-builder' ),
			'category'        => __( 'Email Forms and Links', 'fl-builder' ),
			'icon'            => 'format-image.svg',
			'partial_refresh' => true,
		));
		
		/*global $wpdb;
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$post_id = url_to_postid($actual_link);
		if(wp_get_post_parent_id($post_id) == 0 || wp_get_post_parent_id($post_id) == false){
			$post_id = $post_id;
		}else{
			$post_id = wp_get_post_parent_id($post_id);
		}
		$get_aff = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_website_links` WHERE funnel_id='".$post_id."' AND aff_network!='creator'");
		$options_array = array();
		$options_array[''] = _x('None', 'Affiliate Link.', 'fl-builder');
		foreach($get_aff as $gaff){
			$options_array[$gaff->aff_link] = __($gaff->name, 'fl-builder');
		}
		print_r($options_array);exit;*/
	}

	/**
	 * Ensure backwards compatibility with old settings.
	 *
	 * @since 2.2
	 * @param object $settings A module settings object.
	 * @param object $helper A settings compatibility helper.
	 * @return object
	 */
	public function filter_settings( $settings, $helper ) {

		// Handle old link fields.
		if ( isset( $settings->link_target ) ) {
			$settings->link_url_target = $settings->link_target;
			unset( $settings->link_target );
		}
		// echo $settings->download_url_target;
		if ( isset( $settings->affiliate_url_target ) ) {
			$settings->link_url_target = $settings->affiliate_url_target;
			unset( $settings->affiliate_url_target );
		}
		if ( isset( $settings->link_nofollow ) ) {
			$settings->link_url_nofollow = $settings->link_nofollow;
			unset( $settings->link_nofollow );
		}
		if ( isset( $settings->download_url_nofollow ) ) {
			$settings->affiliate_url_nofollow = $settings->affiliate_url_nofollow;
			unset( $settings->download_url_nofollow );
		}

		return $settings;
	}

	/**
	 * @method enqueue_scripts
	 */
	public function enqueue_scripts() {
		$override_lightbox = apply_filters( 'fl_builder_override_lightbox', false );

		if ( $this->settings && 'lightbox' == $this->settings->link_type ) {
			if ( ! $override_lightbox ) {
				$this->add_js( 'jquery-magnificpopup' );
				$this->add_css( 'font-awesome-5' );
				$this->add_css( 'jquery-magnificpopup' );
			} else {
				wp_dequeue_script( 'jquery-magnificpopup' );
				wp_dequeue_style( 'jquery-magnificpopup' );
			}
		}
	}

	/**
	 * @method update
	 * @param $settings {object}
	 */
	public function update( $settings ) {
		// Make sure we have a photo_src property.
		if ( ! isset( $settings->photo_src ) ) {
			$settings->photo_src = '';
		}

		// Cache the attachment data.
		$settings->data = FLBuilderPhoto::get_attachment_data( $settings->photo );

		// Save a crop if necessary.
		$this->crop();
		
		if( isset($settings->link_type) ){
			if($settings->link_type == 'add_new_affiliate_link'){
				global $wpdb;
				$internal_name = $settings->affiliate_name;
				if($settings->affiliate_link_type == 'jvzoo'){
					$link = $settings->jvz_affiliate_link;
					// $identifier = $settings->jvz_link_identifier;
					$identifier_prefix = 'jvz';
					$notes = "";
				}else if($settings->affiliate_link_type == 'clickbank'){
					$link = $settings->cb_vendor_id;
					// $identifier = $settings->cb_link_identifier;
					$identifier_prefix = 'cb';
					$notes = $settings->cb_notes;
				}else if($settings->affiliate_link_type == 'warriorplus'){
					$link = $settings->wp_product_id;
					$identifier_prefix = 'wp';
					// $identifier = $settings->wp_link_identifier;
				}else if($settings->affiliate_link_type == 'external'){
					$link = $settings->external_affiliate_link;
					// $identifier = $settings->external_link_identifier;
					$identifier_prefix = 'ex';
					$notes = $settings->external_notes;
				}else if($settings->affiliate_link_type == 'paykickstart'){
					$link = $settings->pks_campaign_id;
					// $identifier = $settings->external_link_identifier;
					$identifier_prefix = 'pks';
					$notes = $settings->pks_notes;
				}
				$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				$post_id = url_to_postid($actual_link);
				if(wp_get_post_parent_id($post_id) == 0 || wp_get_post_parent_id($post_id) == false){
					$post_id = $post_id;
				}else{
					$post_id = wp_get_post_parent_id($post_id);
				}
				$identifier = $identifier_prefix.get_current_user_id().$post_id;
				$identifier = validate_link_identifier($identifier);
				if($link && $identifier && $internal_name){
					$query = $wpdb->query("INSERT INTO `".$wpdb->prefix."setc_website_links` (funnel_id, name, aff_network, aff_link, link_identifier, notes) VALUES ('".$post_id."', '".$internal_name."', '".$settings->affiliate_link_type."', '".$link."', '".$identifier."', '".$notes."')");
					if($query){
						$settings->affiliate_url = $identifier;
					}
				}
			}
		}
		
		return $settings;
	}

	/**
	 * @method delete
	 */
	public function delete() {
		$cropped_path = $this->_get_cropped_path();

		if ( fl_builder_filesystem()->file_exists( $cropped_path['path'] ) ) {
			fl_builder_filesystem()->unlink( $cropped_path['path'] );
		}
	}

	/**
	 * @method crop
	 */
	public function crop() {
		// Delete an existing crop if it exists.
		$this->delete();

		// Do a crop.
		if ( ! empty( $this->settings->crop ) ) {

			$editor = $this->_get_editor();

			if ( ! $editor || is_wp_error( $editor ) ) {
				return false;
			}

			$cropped_path = $this->_get_cropped_path();
			$size         = $editor->get_size();
			$new_width    = $size['width'];
			$new_height   = $size['height'];

			// Get the crop ratios.
			if ( 'landscape' == $this->settings->crop ) {
				$ratio_1 = 1.43;
				$ratio_2 = .7;
			} elseif ( 'panorama' == $this->settings->crop ) {
				$ratio_1 = 2;
				$ratio_2 = .5;
			} elseif ( 'portrait' == $this->settings->crop ) {
				$ratio_1 = .7;
				$ratio_2 = 1.43;
			} elseif ( 'square' == $this->settings->crop ) {
				$ratio_1 = 1;
				$ratio_2 = 1;
			} elseif ( 'circle' == $this->settings->crop ) {
				$ratio_1 = 1;
				$ratio_2 = 1;
			}

			// Get the new width or height.
			if ( $size['width'] / $size['height'] < $ratio_1 ) {
				$new_height = $size['width'] * $ratio_2;
			} else {
				$new_width = $size['height'] * $ratio_1;
			}

			// Make sure we have enough memory to crop.
			try {
				ini_set( 'memory_limit', '300M' );
			} catch ( Exception $e ) {
				//
			}

			// Crop the photo.
			$editor->resize( $new_width, $new_height, true );

			// Save the photo.
			$editor->save( $cropped_path['path'] );

			/**
			 * Let third party media plugins hook in.
			 * @see fl_builder_photo_cropped
			 */
			do_action( 'fl_builder_photo_cropped', $cropped_path, $editor );

			// Return the new url.
			return $cropped_path['url'];
		}

		return false;
	}

	/**
	 * @method get_data
	 */
	public function get_data() {
		if ( ! $this->data ) {

			// Photo source is set to "url".
			if ( 'url' == $this->settings->photo_source ) {
				$this->data                = new stdClass();
				$this->data->alt           = $this->settings->caption;
				$this->data->caption       = $this->settings->caption;
				$this->data->link          = $this->settings->photo_url;
				$this->data->url           = $this->settings->photo_url;
				$this->settings->photo_src = $this->settings->photo_url;
				$this->data->title         = ( '' !== $this->settings->url_title ) ? $this->settings->url_title : basename( $this->settings->photo_url );
			} elseif ( is_object( $this->settings->photo ) ) {
				$this->data = $this->settings->photo;
			} else {
				$this->data = FLBuilderPhoto::get_attachment_data( $this->settings->photo );
			}

			// Data object is empty, use the settings cache.
			if ( ! $this->data && isset( $this->settings->data ) ) {
				$this->data = $this->settings->data;
			}
		}
		/**
		 * Make photo data filterable.
		 * @since 2.2.6
		 * @see fl_builder_photo_data
		 */
		return apply_filters( 'fl_builder_photo_data', $this->data, $this->settings, $this->node );
	}

	/**
	 * @method get_classes
	 */
	public function get_classes() {
		$classes = array( 'fl-photo-img' );

		if ( 'library' == $this->settings->photo_source && ! empty( $this->settings->photo ) ) {

			$data = self::get_data();

			if ( is_object( $data ) ) {

				if ( isset( $data->id ) ) {
					$classes[] = 'wp-image-' . $data->id;
				}

				if ( isset( $data->sizes ) ) {

					foreach ( $data->sizes as $key => $size ) {

						if ( $size->url == $this->settings->photo_src ) {
							$classes[] = 'size-' . $key;
							break;
						}
					}
				}
			}
		}

		return implode( ' ', $classes );
	}
	
	public function get_linktype() {
		return $this->settings->link_type;
	}
	public function get_affiliate_network(){
		return $this->settings->affiliate_link_type;
	}

	/**
	 * @method get_src
	 */
	public function get_src() {
		$src = $this->_get_uncropped_url();

		// Return a cropped photo.
		if ( $this->_has_source() && ! empty( $this->settings->crop ) ) {

			$cropped_path = $this->_get_cropped_path();

			if ( fl_builder_filesystem()->file_exists( $cropped_path['path'] ) ) {
				// An existing cropped photo exists.
				$src = $cropped_path['url'];
			} else {

				// A cropped photo doesn't exist, check demo sites then try to create one.
				$post_data    = FLBuilderModel::get_post_data();
				$editing_node = isset( $post_data['node_id'] );
				$demo_domain  = FL_BUILDER_DEMO_DOMAIN;

				if ( ! $editing_node && stristr( $src, $demo_domain ) && ! stristr( $demo_domain, $_SERVER['HTTP_HOST'] ) ) {
					$src = $this->_get_cropped_demo_url();
				} elseif ( ! $editing_node && stristr( $src, FL_BUILDER_OLD_DEMO_URL ) ) {
					$src = $this->_get_cropped_demo_url();
				} else {
					$url = $this->crop();
					$src = $url ? $url : $src;
				}
			}
		}
		return $src;
	}

	/**
	 * @method get_link
	 */
	public function get_link() {
		$photo = $this->get_data();

		if ( 'url' == $this->settings->link_type ) {
			$link = $this->settings->link_url;
		} elseif ( 'affiliate_link' == $this->settings->link_type ) {
			$link = $this->settings->affiliate_url;
			$link = $this->settings->affiliate_url;
		}elseif ( 'add_new_affiliate_link' == $this->settings->link_type ) {
			$link = $this->settings->affiliate_url;
		} elseif ( isset( $photo ) && 'lightbox' == $this->settings->link_type ) {
			$link = $photo->url;
		} elseif ( isset( $photo ) && 'file' == $this->settings->link_type ) {
			$link = $photo->url;
		} elseif ( isset( $photo ) && 'page' == $this->settings->link_type ) {
			$link = $photo->link;
		} else {
			$link = '';
		}

		return $link;
	}

	/**
	 * @method get_alt
	 */
	public function get_alt() {
		$photo = $this->get_data();

		if ( ! empty( $photo->alt ) ) {
			return htmlspecialchars( $photo->alt );
		} elseif ( ! empty( $photo->description ) ) {
			return htmlspecialchars( $photo->description );
		} elseif ( ! empty( $photo->caption ) ) {
			return htmlspecialchars( $photo->caption );
		} elseif ( ! empty( $photo->title ) ) {
			return htmlspecialchars( $photo->title );
		}
	}

	/**
	 * @method get_caption
	 */
	public function get_caption() {
		$photo   = $this->get_data();
		$caption = '';

		if ( $photo && ! empty( $this->settings->show_caption ) && ! empty( $photo->caption ) ) {
			if ( ! empty( $photo->data_source ) && 'smugmug' === $photo->data_source ) {
				$caption = esc_html( $photo->caption );
			} elseif ( isset( $photo->id ) ) {
				$caption = wp_kses_post( wp_get_attachment_caption( $photo->id ) );
			} else {
				$caption = esc_html( $photo->caption );
			}
		}

		return $caption;
	}

	/**
	 * @method get_attributes
	 */
	public function get_attributes() {
		$photo = $this->get_data();
		$attrs = '';

		if ( isset( $this->settings->attributes ) ) {
			foreach ( $this->settings->attributes as $key => $val ) {
				$attrs .= $key . '="' . $val . '" ';
			}
		}

		if ( is_object( $photo ) && isset( $photo->sizes ) ) {
			foreach ( $photo->sizes as $size ) {
				if ( $size->url == $this->settings->photo_src && isset( $size->width ) && isset( $size->height ) ) {
					$attrs .= 'height="' . $size->height . '" width="' . $size->width . '" ';
				}
			}
		}

		if ( ! empty( $photo->title ) ) {
			$attrs .= 'title="' . htmlspecialchars( $photo->title ) . '" ';
		}

		if ( FLBuilderModel::is_builder_active() ) {
			$attrs .= 'onerror="this.style.display=\'none\'" ';
		}

		/**
		 * Filter image attributes as a string.
		 * @since 2.2.3
		 * @see fl_builder_photo_attributes
		 */
		return apply_filters( 'fl_builder_photo_attributes', $attrs );
	}

	/**
	 * @method _has_source
	 * @protected
	 */
	protected function _has_source() {
		if ( 'url' == $this->settings->photo_source && ! empty( $this->settings->photo_url ) ) {
			return true;
		} elseif ( 'library' == $this->settings->photo_source && ! empty( $this->settings->photo_src ) ) {
			return true;
		}

		return false;
	}

	/**
	 * @method _get_editor
	 * @protected
	 */
	protected function _get_editor() {
		if ( $this->_has_source() && null === $this->_editor ) {

			$url_path = $this->_get_uncropped_url();

			$file_path = trailingslashit( WP_CONTENT_DIR ) . ltrim( str_replace( basename( WP_CONTENT_DIR ), '', wp_make_link_relative( $url_path ) ), '/' );

			if ( fl_builder_filesystem()->file_exists( $file_path ) ) {
				$this->_editor = wp_get_image_editor( $file_path );
			} else {
				if ( ! is_wp_error( wp_safe_remote_head( $url_path, array( 'timeout' => 5 ) ) ) ) {
					$this->_editor = wp_get_image_editor( $url_path );
				}
			}
		}
		return $this->_editor;
	}

	/**
	 * @method _get_cropped_path
	 * @protected
	 */
	protected function _get_cropped_path() {
		$crop      = empty( $this->settings->crop ) ? 'none' : $this->settings->crop;
		$url       = $this->_get_uncropped_url();
		$cache_dir = FLBuilderModel::get_cache_dir();

		if ( empty( $url ) ) {
			$filename = uniqid(); // Return a file that doesn't exist.
		} else {

			if ( stristr( $url, '?' ) ) {
				$parts = explode( '?', $url );
				$url   = $parts[0];
			}

			$pathinfo = pathinfo( $url );

			if ( isset( $pathinfo['extension'] ) ) {
				$dir      = $pathinfo['dirname'];
				$ext      = $pathinfo['extension'];
				$name     = wp_basename( $url, ".$ext" );
				$new_ext  = strtolower( $ext );
				$filename = "{$name}-{$crop}.{$new_ext}";
			} else {
				$filename = $pathinfo['filename'] . "-{$crop}.png";
			}
		}

		return array(
			'filename' => $filename,
			'path'     => $cache_dir['path'] . $filename,
			'url'      => $cache_dir['url'] . $filename,
		);
	}

	/**
	 * @method _get_uncropped_url
	 * @protected
	 */
	protected function _get_uncropped_url() {
		if ( 'url' == $this->settings->photo_source ) {
			$url = $this->settings->photo_url;
		} elseif ( ! empty( $this->settings->photo_src ) ) {
			$url = $this->settings->photo_src;
		} else {
			$url = apply_filters( 'fl_builder_photo_noimage', FL_BUILDER_URL . 'img/pixel.png' );
		}

		return $url;
	}

	/**
	 * @method _get_cropped_demo_url
	 * @protected
	 */
	protected function _get_cropped_demo_url() {
		$info = $this->_get_cropped_path();
		$src  = $this->settings->photo_src;

		// Pull from a demo subsite.
		if ( stristr( $src, '/uploads/sites/' ) ) {
			$url_parts  = explode( '/uploads/sites/', $src );
			$site_parts = explode( '/', $url_parts[1] );
			return $url_parts[0] . '/uploads/sites/' . $site_parts[0] . '/bb-plugin/cache/' . $info['filename'];
		}

		// Pull from the demo main site.
		return FL_BUILDER_DEMO_CACHE_URL . $info['filename'];
	}
	
	/**
	 * @method _get_download_url
	 * @protected
	 */
	public function _get_affiliate_url() {
		global $wpdb;
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$post_id = url_to_postid($actual_link);
		if(wp_get_post_parent_id($post_id) == 0 || wp_get_post_parent_id($post_id) == false){
			$post_id = $post_id;
		}else{
			$post_id = wp_get_post_parent_id($post_id);
		}
		$get_aff = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_website_links` WHERE funnel_id='".$post_id."' AND aff_network!='creator'");
		$options_array = array();
		$options_array[''] = _x('None', 'Affiliate Link.', 'fl-builder');
		foreach($get_aff as $gaff){
			$options_array[$gaff->link_identifier] = __($gaff->name.' ['.$gaff->aff_network.']', 'fl-builder');
		}
		return $options_array;
	}
	public function _get_wp_products(){
		global $wpdb;
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, "https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=932cb1277c5faf9aadfb613c52050e6&limit=50");
		$result = curl_exec($ch);

		$json_decode_result = json_decode($result);
		if(count($json_decode_result) > 0){
			$options_array = array();
			$options_array[''] = _x('None', 'WarriorPlus Product.', 'fl-builder');
			foreach($json_decode_result->data as $data){
				$options_array[$data->offer->id] = __($data->offer->name, 'fl-builder');
			}
		}*/
		$json_decode_result = get_user_meta(get_current_user_id(),'warrior_plus_products',true);
		$json_decode_result = maybe_unserialize($json_decode_result);
		if(is_array($json_decode_result)){
		if(count($json_decode_result) > 0){
			$options_array = array();
			$options_array[''] = _x('None', 'WarriorPlus Product.', 'fl-builder');
			foreach($json_decode_result->data as $data){
				$options_array[$data->offer->id] = __($data->offer->name, 'fl-builder');
			}
		}
		}
		return $options_array;
	}

	/**
	 * Returns link rel
	 * @since 2.0.6
	 */
	public function get_rel() {
		$rel = array();
		if ( '_blank' == $this->settings->link_url_target ) {
			$rel[] = 'noopener';
		}
		if ( isset( $this->settings->link_url_nofollow ) && 'yes' == $this->settings->link_url_nofollow ) {
			$rel[] = 'nofollow';
		}
		$rel = implode( ' ', $rel );
		if ( $rel ) {
			$rel = ' rel="' . $rel . '" ';
		}
		return $rel;
	}
}

/**
 * Register the module and its form settings.
 */
 // echo 'out';exit;
FLBuilder::register_module('FLFMAffPhotoModule', array(
	'general' => array( // Tab
		'title'    => __( 'General', 'fl-builder' ), // Tab title
		'sections' => array( // Tab Sections
			'general' => array( // Section
				'title'  => '', // Section Title
				'fields' => array( // Section Fields
					'photo_source' => array(
						'type'    => 'select',
						'label'   => __( 'Photo Source', 'fl-builder' ),
						'default' => 'library',
						'options' => array(
							'library' => __( 'Media Library', 'fl-builder' ),
							'url'     => __( 'URL', 'fl-builder' ),
						),
						'toggle'  => array(
							'library' => array(
								'fields' => array( 'photo' ),
							),
							'url'     => array(
								'fields' => array( 'photo_url', 'caption', 'url_title' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'photo'        => array(
						'type'        => 'photo',
						'label'       => __( 'Photo', 'fl-builder' ),
						'connections' => array( 'photo' ),
						'show_remove' => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
					'photo_url'    => array(
						'type'        => 'text',
						'label'       => __( 'Photo URL', 'fl-builder' ),
						'placeholder' => __( 'http://www.example.com/my-photo.jpg', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
					),
					'title_hover'  => array(
						'type'    => 'select',
						'label'   => __( 'Show title attribute on mouse hover', 'fl-builder' ),
						'default' => 'no',
						'options' => array(
							'no'  => __( 'No', 'fl-builder' ),
							'yes' => __( 'Yes', 'fl-builder' ),
						),
					),
					'url_title'    => array(
						'type'        => 'text',
						'label'       => __( 'Image title attribute', 'fl-builder' ),
						'default'     => '',
						'placeholder' => __( 'Use image filename if left blank', 'fl-builder' ),
					),
				),
			),
			'caption' => array(
				'title'  => __( 'Caption', 'fl-builder' ),
				'fields' => array(
					'show_caption' => array(
						'type'    => 'select',
						'label'   => __( 'Show Caption', 'fl-builder' ),
						'default' => '0',
						'options' => array(
							'0'     => __( 'Never', 'fl-builder' ),
							'hover' => __( 'On Hover', 'fl-builder' ),
							'below' => __( 'Below Photo', 'fl-builder' ),
						),

						'toggle'  => array(
							''      => array(),
							'hover' => array(
								'fields' => array( 'caption_typography' ),
							),

							'below' => array(
								'fields' => array( 'caption_typography' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'caption'      => array(
						'type'    => 'text',
						'label'   => __( 'Caption', 'fl-builder' ),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'link'    => array(
				'title'  => __( 'Link', 'fl-builder' ),
				'fields' => array(
					'link_type' => array(
						'type'    => 'select',
						'label'   => __( 'Link Type', 'fl-builder' ),
						'options' => array(
							''         => _x( 'None', 'Link type.', 'fl-builder' ),
							// 'url'      => __( 'Custom URL', 'fl-builder' ),
							'add_new_affiliate_link'     => __( 'Add New Affiliate Link', 'fl-builder' ),
							'affiliate_link'      => __( 'Affiliate Link', 'fl-builder' ),
							//'lightbox' => __( 'Lightbox', 'fl-builder' ),
							//'file'     => __( 'Photo File', 'fl-builder' ),
							//'page'     => __( 'Photo Page', 'fl-builder' ),
						),
						'toggle'  => array(
							''     => array(),
							'add_new_affiliate_link'     => array(
								'sections' => array( 'add_new_affiliate_link' ),
							),
							'url'  => array(
								'fields' => array( 'link_url' ),
							),
							'affiliate_link'  => array(
								'fields' => array( 'affiliate_url' ),
							),
							'file' => array(),
							'page' => array(),
						),
						'help'    => __( 'Link type applies to how the image should be linked on click. You can choose a specific URL, the individual photo or a separate page with the photo.', 'fl-builder' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'link_url'  => array(
						'type'          => 'link',
						'label'         => __( 'Link URL', 'fl-builder' ),
						'show_target'   => true,
						'show_nofollow' => true,
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'url' ),
					),
					'affiliate_url'  => array(
						'type'    => 'select',
						'label'   => __( 'Affiliate URL', 'fl-builder' ),
						'default' => 'none',
						'options' => FLFMAffPhotoModule::_get_affiliate_url(),
					),
				),
			),
			'add_new_affiliate_link' => array(
				'title'  => __( 'New Affiliate Link', 'fl-builder' ),
				'fields' => array(
					'affiliate_name' => array(
						'type'        => 'text',
						'label'       => __( 'Product/Link Name', 'fl-builder' ),
						'placeholder' => 'Name to Identify the Link',
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'affiliate_link_type' => array(
						'type'    => 'select',
						'label'   => __( 'Link Type', 'fl-builder' ),
						'default' => 'JVZoo',
						'options' => array(
							'jvzoo'  => __( 'JVZoo', 'fl-builder' ),
							'clickbank' => __( 'ClickBank', 'fl-builder' ),
							'warriorplus' => __( 'Warrior Plus', 'fl-builder' ),
							'paykickstart' => __( 'PayKickStart', 'fl-builder' ),
							'external' => __( 'External', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
						'toggle'  => array(
							'jvzoo'  => array(
								'fields' => array( 'jvz_affiliate_link'/*, 'jvz_link_identifier'*/ ),
							),
							'clickbank' => array(
								'fields' => array( 'cb_vendor_id', /*'cb_link_identifier',*/ 'cb_notes' ),
							),
							'warriorplus' => array(
								'fields' => array( 'wp_product_id'/*, 'wp_link_identifier'*/ ),
							),
							'paykickstart' => array(
								'fields' => array( 'pks_campaign_id', /*'external_link_identifier',*/ 'pks_notes' ),
							),
							'external' => array(
								'fields' => array( 'external_affiliate_link', /*'external_link_identifier',*/ 'external_notes' ),
							),
						),
					),
					'jvz_affiliate_link' => array(
						'type'          => 'text',
						'label'         => __( 'JVZoo Affiliate Request Link', 'fl-builder' ),
						'placeholder'   => __( 'https://www.jvzoo.com/affiliate/affiliateinfo/index/349507', 'fl-builder' ),
						'help'    => __( 'The link users visit to request to promote the offer eg. https://www.jvzoo.com/affiliate/affiliateinfo/index/349507', 'fl-builder' ),
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'url' ),
					),
					/*'jvz_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'JVZoo Link Identifier', 'fl-builder' ),
						'placeholder' => 'linkidentifier',
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'cb_vendor_id' => array(
						'type'        => 'text',
						'label'       => __( 'ClickBank Vendor ID', 'fl-builder' ),
						'placeholder' => '352425',
						'help'    => __( 'This is the account nickname of the product you\'re promoting', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					/*'cb_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'ClickBank Link Identifier', 'fl-builder' ),
						'placeholder' => '#CB_IDENTIFIER#',
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'cb_notes' => array(
						'type'        => 'text',
						'label'       => __( 'alt page', 'fl-builder' ),
						'placeholder' => '?cbpage=',
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'wp_product_id' => array(
						'type'    => 'select',
						'label'   => __( 'WarriorPlus Product', 'fl-builder' ),
						'default' => 'none',
						'options' => FLFMAffPhotoModule::_get_wp_products(),
					),
					/*'wp_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'WarriorPlus Identifier', 'fl-builder' ),
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'pks_campaign_id' => array(
						'type'        => 'text',
						'label'       => __( 'Campaign ID', 'fl-builder' ),
						'placeholder' => '123',
						'help'    => __( 'This is the Campaign ID of the product you\'re promoting', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'pks_notes' => array(
						'type'        => 'text',
						'label'       => __( 'Notes', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'external_affiliate_link' => array(
						'type'          => 'text',
						'label'         => __( 'External Affiliate Request Link', 'fl-builder' ),
						'placeholder'   => __( 'Add Your Link', 'fl-builder' ),
						'help'    => __( 'External links can only be used on: Funnels you publish for personal use only, Funnels you publish in marketplace with Whitelabel or Exclusive rights permissions. Free and Premium funnels cannot contain external links Please note: Whitelabel and exclusive funnels can only be sold if you\'re a pro FunnelMates member.', 'fl-builder' ),
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'url' ),
					),
					/*'external_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'External Link Identifier', 'fl-builder' ),
						'placeholder' => '#EX_IDENTIFIER#',
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'external_notes' => array(
						'type'        => 'text',
						'label'       => __( 'External Notes', 'fl-builder' ),
						'placeholder' => 'Notes..',
						'help'    => __( 'Optional. Can be used to provide information to funnel customer about why you\'ve included this link, links to external affiliate program or why they should keep the link as it is.', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					//
				),
			),
		),
	),
	'style'   => array( // Tab
		'title'    => __( 'Style', 'fl-builder' ), // Tab title
		'sections' => array( // Tab Sections
			'general' => array( // Section
				'title'  => '', // Section Title
				'fields' => array( // Section Fields
					'crop'               => array(
						'type'    => 'select',
						'label'   => __( 'Crop', 'fl-builder' ),
						'default' => '',
						'options' => array(
							''          => _x( 'None', 'Photo Crop.', 'fl-builder' ),
							'landscape' => __( 'Landscape', 'fl-builder' ),
							'panorama'  => __( 'Panorama', 'fl-builder' ),
							'portrait'  => __( 'Portrait', 'fl-builder' ),
							'square'    => __( 'Square', 'fl-builder' ),
							'circle'    => __( 'Circle', 'fl-builder' ),
						),
					),
					'width'              => array(
						'type'       => 'unit',
						'label'      => __( 'Width', 'fl-builder' ),
						'responsive' => true,
						'units'      => array(
							'px',
							'vw',
							'%',
						),
						'slider'     => array(
							'px' => array(
								'min'  => 0,
								'max'  => 1000,
								'step' => 10,
							),
						),
						'preview'    => array(
							'type'      => 'css',
							'selector'  => '.fl-photo-img',
							'property'  => 'width',
							'important' => true,
						),
					),
					'align'              => array(
						'type'       => 'align',
						'label'      => __( 'Align', 'fl-builder' ),
						'default'    => 'center',
						'responsive' => true,
						'preview'    => array(
							'type'      => 'css',
							'selector'  => '.fl-photo',
							'property'  => 'text-align',
							'important' => true,
						),
					),
					'border'             => array(
						'type'       => 'border',
						'label'      => __( 'Border', 'fl-builder' ),
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.fl-photo-img',
						),
					),

					'caption_typography' => array(
						'type'       => 'typography',
						'label'      => __( 'Caption Typography', 'fl-builder' ),
						'responsive' => true,
						'preview'    => array(
							'type'      => 'css',
							'selector'  => '{node}.fl-module-photo .fl-photo-caption',
							'important' => true,
						),
					),
				),
			),
		),
	),
));
