<?php

/**
 * A module that adds a simple subscribe form to your layout
 * with third party optin integration.
 *
 * @since 1.5.2
 */
class FLCustomSubscribeFormModule extends FLBuilderModule {

	/**
	 * @since 1.5.2
	 * @return void
	 */
	public function __construct() {
		parent::__construct( array(
			'name'            => __( 'Email Form', 'fl-builder' ),
			'description'     => __( 'Adds a simple subscribe form to your layout.', 'fl-builder' ),
			'category'        => __( 'Email Forms and Links', 'fl-builder' ),
			'editor_export'   => false,
			'partial_refresh' => true,
			'icon'            => 'editor-table.svg',
		));

		add_action( 'wp_ajax_fl_builder_subscribe_form_submit_custom', array( $this, 'submit' ) );
		add_action( 'wp_ajax_nopriv_fl_builder_subscribe_form_submit_custom', array( $this, 'submit' ) );
		add_filter( 'script_loader_tag', array( $this, 'add_async_attribute' ), 10, 2 );
	}

	/**
	 * Ensure backwards compatibility with old settings.
	 *
	 * @since 2.2
	 * @param object $settings A module settings object.
	 * @param object $helper A settings compatibility helper.
	 * @return object
	 */
	public function filter_settings( $settings, $helper ) {

		// Handle old button module settings.
		$helper->filter_child_module_settings( 'button', $settings, array(
			'btn_3d'                 => 'three_d',
			'btn_style'              => 'style',
			'btn_padding'            => 'padding',
			'btn_padding_top'        => 'padding_top',
			'btn_padding_bottom'     => 'padding_bottom',
			'btn_padding_left'       => 'padding_left',
			'btn_padding_right'      => 'padding_right',
			'btn_mobile_align'       => 'mobile_align',
			'btn_align_responsive'   => 'align_responsive',
			'btn_font_size'          => 'font_size',
			'btn_font_size_unit'     => 'font_size_unit',
			'ography'                => 'typography',
			'btn_bg_color'           => 'bg_color',
			'btn_bg_hover_color'     => 'bg_hover_color',
			'btn_bg_opacity'         => 'bg_opacity',
			'btn_bg_hover_opacity'   => 'bg_hover_opacity',
			'btn_border'             => 'border',
			'btn_border_hover_color' => 'border_hover_color',
			'btn_border_radius'      => 'border_radius',
			'btn_border_size'        => 'border_size',
		) );

		// Return the filtered settings.
		return $settings;
	}

	/**
	 * @method enqueue_scripts
	 */
	public function enqueue_scripts() {
		$settings = $this->settings;
		if ( isset( $settings->show_recaptcha ) && 'show' == $settings->show_recaptcha
			&& isset( $settings->recaptcha_site_key ) && ! empty( $settings->recaptcha_site_key )
			) {

			$site_lang = substr( get_locale(), 0, 2 );
			$this->add_js(
				'g-recaptcha',
				'https://www.google.com/recaptcha/api.js?onload=onLoadFLReCaptcha&render=explicit&hl=' . $site_lang,
				array(),
				'2.0',
				true
			);
		}
	}

	/**
	 * @method  add_async_attribute for the enqueued `g-recaptcha` script
	 * @param string $tag    Script tag
	 * @param string $handle Registered script handle
	 */
	public function add_async_attribute( $tag, $handle ) {
		if ( ( 'g-recaptcha' !== $handle ) || ( 'g-recaptcha' === $handle && strpos( $tag, 'g-recaptcha-api' ) !== false ) ) {
			return $tag;
		}

		return str_replace( ' src', ' id="g-recaptcha-api" async="async" defer="defer" src', $tag );
	}

	/**
	 * Called via AJAX to submit the subscribe form.
	 *
	 * @since 1.5.2
	 * @return string The JSON encoded response.
	 */
	public function submit() {
		$name             = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : false;
		$email            = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : false;
		$terms_checked    = isset( $_POST['terms_checked'] ) && 1 == $_POST['terms_checked'] ? true : false;
		$recaptcha        = isset( $_POST['recaptcha'] ) ? $_POST['recaptcha'] : false;
		$post_id          = isset( $_POST['post_id'] ) ? $_POST['post_id'] : false;
		$node_id          = isset( $_POST['node_id'] ) ? sanitize_text_field( $_POST['node_id'] ) : false;
		$template_id      = isset( $_POST['template_id'] ) ? sanitize_text_field( $_POST['template_id'] ) : false;
		$template_node_id = isset( $_POST['template_node_id'] ) ? sanitize_text_field( $_POST['template_node_id'] ) : false;
		$result           = array(
			'action'  => false,
			'error'   => false,
			'message' => false,
			'url'     => false,
		);

		if ( $email && $node_id ) {

			// Get the module settings.
			if ( $template_id ) {
				$post_id  = FLBuilderModel::get_node_template_post_id( $template_id );
				$data     = FLBuilderModel::get_layout_data( 'published', $post_id );
				$settings = $data[ $template_node_id ]->settings;
			} else {
				$module   = FLBuilderModel::get_module( $node_id );
				$settings = $module->settings;
			}

			// Validate terms and conditions if enabled
			if ( ( isset( $settings->terms_checkbox ) && 'show' == $settings->terms_checkbox ) && ! $terms_checked ) {
				$result = array(
					'error' => __( 'You must accept the Terms and Conditions.', 'fl-builder' ),
				);
			}

			// Validate reCAPTCHA first if enabled
			if ( $recaptcha && ! $result['error'] ) {

				if ( ! empty( $settings->recaptcha_secret_key ) && ! empty( $settings->recaptcha_site_key ) ) {
					if ( version_compare( phpversion(), '5.3', '>=' ) ) {
						include FLBuilderModel::$modules['subscribe-form']->dir . 'includes/validate-recaptcha.php';
					} else {
						$result['error'] = false;
					}
				} else {
					$result['error'] = __( 'Your reCAPTCHA Site or Secret Key is missing!', 'fl-builder' );
				}
			}

			if ( ! $result['error'] ) {

				// Subscribe.
				// $instance = FLBuilderServices::get_service_instance( $settings->service );
				// $response = $instance->subscribe( $settings, $email, $name );
				global $wpdb;
				$listuid = get_post_meta($post_id,'acelle_list_uid',true);
				$author_id = get_post_field ('post_author', $post_id);
				$creator_fname = get_the_author_meta('first_name',$author_id);
				$creator_lname = get_the_author_meta('last_name',$author_id);
				$CCID = '';
				if(isset($_COOKIE['CCID'])) {
					$CCID = $_COOKIE['CCID'];
				}else{
					$user = get_user_by( 'email', $email );
					$CCID = $user->ID;
				}
				if($listuid){
					if($name){
						$subs_array = array(
							"api_token" => ACELLE_APIKEY,
							"EMAIL" => $email,
							"FIRST_NAME" => $name,
							"CCID" => $CCID
						);
					}else{
						$subs_array = array(
							"api_token" => ACELLE_APIKEY,
							"EMAIL" => $email,
							"CCID" => $CCID
						);						
					}
					
					require_once(get_stylesheet_directory().'/functions/class.submit_form.php');
					
					$query_free = '';
					if($CCID){
						$query_free = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_activated_funnels` WHERE funnel_id='$post_id' AND type='free' AND user_id='$CCID'");
					}
					
					$subscription = new FM_Submit_Landing();
					// $result_aweber = $subscription->aweber($post_id,$email,$CCID);
					$result_getresponse = $subscription->getresponse($post_id,$CCID,$email);
					$result_mailchimp = $subscription->mailchimp($post_id,$CCID,$email);
					$result_convertkit = $subscription->convertkit($post_id,$CCID,$email);
					$result_drip = $subscription->drip($post_id,$CCID,$email);
					$result_mailerlite = $subscription->mailerlite($post_id,$CCID,$email);
					$result_sendiio = $subscription->sendiio($post_id,$CCID,$email);
					$result_mailsqaud = $subscription->mailsqaud($post_id,$CCID,$email);
					if($query_free > 0){
						$result_sendlane = $subscription->sendlane($post_id,$email,$CCID);
					}
					/////////////////////////ADD HERE/////////////////////////
					$result_zapier = $subscription->zapier($email,$CCID);
					$result_gotowebinar = $subscription->gotowebinar($post_id,$CCID,$email);
					$result_everwebinar = $subscription->everwebinar($post_id,$CCID,$email);
					// print_R($result_everwebinar);exit;
					$result_profit = $subscription->category_subscription($post_id,$subs_array);
					//print_r($result_profit);exit;
					$result_acelle = $subscription->fm_subscription($post_id,$subs_array,$listuid);
					$result_acelle = json_decode($result_acelle);
					if($result_acelle->status == '1' && $result_acelle->subscriber_uid){
						$subscriber_uid = $result_acelle->subscriber_uid;
					}
					// print_r($result_acelle);exit;
					$tracking = new FM_Tracking();
					$tracking->email($post_id,$email,$CCID);
					$red_url = site_url()."/th/".$post_id.'/'.$CCID.'/';
				}else{
					$response = array(
						'error' => __( 'No List Found For The Page.', 'fl-builder' ),
					);
				}
				// echo $red_url;
				// echo '<br/>';
				// echo $email;exit;
				

				// Check for an error from the service.
				if ( $response['error'] ) {
					$result['error'] = $response['error'];
				} else {

					$settings->success_action = 'redirect';
					$result['action'] = $settings->success_action;

					if ( 'message' == $settings->success_action ) {
						$result['message'] = '';//$settings->success_message;
					} else {
						$result['url'] = $red_url;//$settings->success_url;
					}
				}
				// print_r($result);exit;
				do_action( 'fl_builder_subscribe_form_submission_complete', $response, $settings, $email, $name, $template_id, $post_id );
			}
		} else {
			$result['error'] = __( 'There was an error subscribing. Please try again.', 'fl-builder' );
		}

		echo json_encode( $result );

		die();
	}

	/**
	 * Returns an array of settings used to render a button module.
	 *
	 * @since 2.2
	 * @return array
	 */
	public function get_button_settings() {
		$settings = array(
			'link'        => '#',
			'link_target' => '_self',
			'width'       => 'full',
		);

		foreach ( $this->settings as $key => $value ) {
			if ( strstr( $key, 'btn_' ) ) {
				$key              = str_replace( 'btn_', '', $key );
				$settings[ $key ] = $value;
			}
		}

		return $settings;
	}
	
	public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
		$output = NULL;
		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
		$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
		$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
		$continents = array(
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America"
		);
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = array(
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode
						);
						break;
					case "address":
						$address = array($ipdat->geoplugin_countryName);
						if (@strlen($ipdat->geoplugin_regionName) >= 1)
							$address[] = $ipdat->geoplugin_regionName;
						if (@strlen($ipdat->geoplugin_city) >= 1)
							$address[] = $ipdat->geoplugin_city;
						$output = implode(", ", array_reverse($address));
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		}
		return $output;
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module( 'FLCustomSubscribeFormModule', array(
	'general'   => array(
		'title'    => __( 'General', 'fl-builder' ),
		'sections' => array(
			/*'service'   => array(
				'title'    => '',
				'services' => 'autoresponder',
				'template' => array(
					'id'   => 'fl-builder-service-settings',
					'file' => FL_BUILDER_DIR . 'includes/ui-service-settings.php',
				),
			),*/
			'structure' => array(
				'title'  => __( 'Structure', 'fl-builder' ),
				'fields' => array(
					'layout'              => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'fl-builder' ),
						'default' => 'stacked',
						'options' => array(
							'stacked' => __( 'Stacked', 'fl-builder' ),
							'inline'  => __( 'Inline', 'fl-builder' ),
						),
					),
					'show_name'           => array(
						'type'    => 'select',
						'label'   => __( 'Name Field', 'fl-builder' ),
						'default' => 'hide',
						'options' => array(
							'show' => __( 'Show', 'fl-builder' ),
							'hide' => __( 'Hide', 'fl-builder' ),
						),
						'toggle'  => array(
							'show' => array(
								'fields' => array( 'name_field_text' ),
							),
						),
					),
					'name_field_text'     => array(
						'type'    => 'text',
						'label'   => __( 'Name Field Text', 'fl-builder' ),
						'default' => __( 'Name', 'fl-builder' ),
					),
					'email_field_text'    => array(
						'type'    => 'text',
						'label'   => __( 'Email Field Text', 'fl-builder' ),
						'default' => __( 'Email Address', 'fl-builder' ),
					),
					'terms_checkbox'      => array(
						'type'    => 'select',
						'label'   => __( 'Terms and Conditions Checkbox', 'fl-builder' ),
						'default' => 'hide',
						'options' => array(
							'show' => __( 'Show', 'fl-builder' ),
							'hide' => __( 'Hide', 'fl-builder' ),
						),
						'toggle'  => array(
							'show' => array(
								'fields' => array( 'terms_checkbox_text', 'terms_text' ),
							),
						),
					),
					'terms_checkbox_text' => array(
						'type'    => 'text',
						'label'   => __( 'Checkbox Text', 'fl-builder' ),
						'default' => __( 'I Accept the Terms and Conditions', 'fl-builder' ),
					),
					'terms_text'          => array(
						'type'          => 'editor',
						'label'         => 'Terms and Conditions',
						'media_buttons' => false,
						'rows'          => 8,
						'preview'       => array(
							'type'     => 'text',
							'selector' => '.fl-terms-checkbox-text',
						),
						'connections'   => array( 'string' ),
					),
				),
			),
			'success'   => array(
				'title'  => __( 'Success', 'fl-builder' ),
				'fields' => array(
					'custom_subject'  => array(
						'type'        => 'text',
						'label'       => __( 'Notification Subject', 'fl-builder' ),
						'placeholder' => __( 'Subscribe Form Signup', 'fl-builder' ),
					),
					'success_action'  => array(
						'type'    => 'select',
						'label'   => __( 'Success Action', 'fl-builder' ),
						'options' => array(
							'message'  => __( 'Show Message', 'fl-builder' ),
							'redirect' => __( 'Redirect', 'fl-builder' ),
						),
						'toggle'  => array(
							'message'  => array(
								'fields' => array( 'success_message' ),
							),
							'redirect' => array(
								'fields' => array( 'success_url' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'success_message' => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
						'rows'          => 8,
						'default'       => __( 'Thanks for subscribing! Please check your email for further instructions.', 'fl-builder' ),
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'string' ),
					),
					'success_url'     => array(
						'type'        => 'link',
						'label'       => __( 'Success URL', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'url' ),
					),
				),
			),
		),
	),
	'button'    => array(
		'title'    => __( 'Button', 'fl-builder' ),
		'sections' => array(
			'btn_general' => array(
				'title'  => '',
				'fields' => array(
					'btn_text' => array(
						'type'    => 'text',
						'label'   => __( 'Button Text', 'fl-builder' ),
						'default' => __( 'Yes! I want it.', 'fl-builder' ),
						'preview' => array(
							'type'     => 'text',
							'selector' => '.fl-button-text',
						),
					),
				),
			),
			'btn_icon'    => array(
				'title'  => __( 'Button Icon', 'fl-builder' ),
				'fields' => array(
					'btn_icon'           => array(
						'type'        => 'icon',
						'label'       => __( 'Button Icon', 'fl-builder' ),
						'default'       => 'fas fa-envelope',
						'show_remove' => true,
						'show'        => array(
							'fields' => array( 'btn_icon_position', 'btn_icon_animation' ),
						),
					),
					'btn_icon_position'  => array(
						'type'    => 'select',
						'label'   => __( 'Button Icon Position', 'fl-builder' ),
						'default' => 'before',
						'options' => array(
							'before' => __( 'Before Text', 'fl-builder' ),
							'after'  => __( 'After Text', 'fl-builder' ),
						),
					),
					'btn_icon_animation' => array(
						'type'    => 'select',
						'label'   => __( 'Button Icon Visibility', 'fl-builder' ),
						'default' => 'disable',
						'options' => array(
							'disable' => __( 'Always Visible', 'fl-builder' ),
							'enable'  => __( 'Fade In On Hover', 'fl-builder' ),
						),
					),
				),
			),
			'btn_style'   => array(
				'title'  => __( 'Button Style', 'fl-builder' ),
				'fields' => array(
					'btn_padding' => array(
						'type'       => 'dimension',
						'label'      => __( 'Button Padding', 'fl-builder' ),
						'responsive' => true,
						'slider'     => true,
						'units'      => array( 'px' ),
						'preview'    => array(
							'type'     => 'css',
							'selector' => 'a.fl-button, .fl-form-field input, .fl-form-field input[type=text]',
							'property' => 'padding',
						),
					),
				),
			),
			'btn_text'    => array(
				'title'  => __( 'Button Text', 'fl-builder' ),
				'fields' => array(
					'btn_text_color'       => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Button Text Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type'      => 'css',
							'selector'  => 'a.fl-button, a.fl-button *',
							'property'  => 'color',
							'important' => true,
						),
					),
					'btn_text_hover_color' => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Button Text Hover Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type'      => 'css',
							'selector'  => 'a.fl-button:hover, a.fl-button:hover *, a.fl-button:focus, a.fl-button:focus *',
							'property'  => 'color',
							'important' => true,
						),
					),
					'btn_typography'       => array(
						'type'       => 'typography',
						'label'      => __( 'Button Typography', 'fl-builder' ),
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => 'a.fl-button, .fl-form-field input, .fl-form-field input[type=text]',
						),
					),
				),
			),
			'btn_colors'  => array(
				'title'  => __( 'Button Background', 'fl-builder' ),
				'fields' => array(
					'btn_bg_color'          => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Button Background Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
					'btn_bg_hover_color'    => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Button Background Hover Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
					'btn_style'             => array(
						'type'    => 'select',
						'label'   => __( 'Button Background Style', 'fl-builder' ),
						'default' => 'flat',
						'options' => array(
							'flat'     => __( 'Flat', 'fl-builder' ),
							'gradient' => __( 'Gradient', 'fl-builder' ),
						),
					),
					'btn_button_transition' => array(
						'type'    => 'select',
						'label'   => __( 'Button Background Animation', 'fl-builder' ),
						'default' => 'disable',
						'options' => array(
							'disable' => __( 'Disabled', 'fl-builder' ),
							'enable'  => __( 'Enabled', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'btn_border'  => array(
				'title'  => __( 'Button Border', 'fl-builder' ),
				'fields' => array(
					'btn_border'             => array(
						'type'       => 'border',
						'label'      => __( 'Button Border', 'fl-builder' ),
						'responsive' => true,
						'preview'    => array(
							'type'      => 'css',
							'selector'  => 'a.fl-button',
							'important' => true,
						),
					),
					'btn_border_hover_color' => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Button Border Hover Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
				),
			),
		),
	),
	'reCAPTCHA' => array(
		'title'       => __( 'Captcha', 'fl-builder' ),
		'sections'    => array(
			'recaptcha_general' => array(
				'title'  => '',
				'fields' => array(
					'show_recaptcha'          => array(
						'type'    => 'select',
						'label'   => __( 'reCAPTCHA Field', 'fl-builder' ),
						'default' => 'hide',
						'options' => array(
							'show' => __( 'Show', 'fl-builder' ),
							'hide' => __( 'Hide', 'fl-builder' ),
						),
						'toggle'  => array(
							'show' => array(
								'fields' => array( 'recaptcha_site_key', 'recaptcha_secret_key', 'recaptcha_validate_type', 'recaptcha_theme' ),
							),
						),
						'help'    => __( 'If you want to show this field, please provide valid Site and Secret Keys.', 'fl-builder' ),
					),
					'recaptcha_site_key'      => array(
						'type'    => 'text',
						'label'   => __( 'Site Key', 'fl-builder' ),
						'default' => '',
						'preview' => array(
							'type' => 'none',
						),
					),
					'recaptcha_secret_key'    => array(
						'type'    => 'text',
						'label'   => __( 'Secret Key', 'fl-builder' ),
						'default' => '',
						'preview' => array(
							'type' => 'none',
						),
					),
					'recaptcha_validate_type' => array(
						'type'    => 'select',
						'label'   => __( 'Validate Type', 'fl-builder' ),
						'default' => 'normal',
						'options' => array(
							'normal'    => __( '"I\'m not a robot" checkbox', 'fl-builder' ),
							'invisible' => __( 'Invisible', 'fl-builder' ),
						),
						'help'    => __( 'Validate users with checkbox or in the background.', 'fl-builder' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'recaptcha_theme'         => array(
						'type'    => 'select',
						'label'   => __( 'Theme', 'fl-builder' ),
						'default' => 'light',
						'options' => array(
							'light' => __( 'Light', 'fl-builder' ),
							'dark'  => __( 'Dark', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
		),
		/* translators: %s: Google admin url */
		'description' => sprintf( __( 'Please register keys for your website at the <a%s>Google Admin Console</a>', 'fl-builder' ), ' href="https://www.google.com/recaptcha/admin" target="_blank" rel="noopener"' ),
	),
));
