<?php
$updateMessage = '';
if(isset($_POST['sendy_submit']) && $_POST['sendy_submit'] != ''){
	// echo '<pre>';
	// print_r($_POST);
	$update = Array();
	if($_REQUEST['s_api']){
		foreach ($_REQUEST['s_api'] as $key => $value){
			$update[$key] = $value;
		}
	}
	// print_r($update);exit;
	update_option("s_api-options", $update);
	if($updateMessage == ''){
		$updateMessage = '<div class="cb_cabar_active_panel"><p><strong>Settings Updated </strong></p></div>';
	}
}
$options = get_option('s_api-options');
?>
<style>
.container{
    background: white;
    padding: 15px;
    border: 2px solid grey;
    border-radius: 5px;
    box-shadow: 0px 0px 7px 5px #DDD;
}
table.widefat.fixed.striped.tbl_sendy_api td input[type=text]{width:100%;}
table.widefat.fixed.striped.tbl_sendy_api td{font-size: 20px;font-weight: 600;}
code{    background: #f7f9fc;
    border: 1px solid #e0e3e6;
    color: #cb2626;
    padding: 2px 4px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;}
code span{color:#666;}
.instru p{font-size: 15px;font-weight: 600;}
.instru p span.info{
    font-style: italic;
    font-size: 11px;
    font-weight: normal;
    color: #999;
}
</style>
<div class="wrap">
	<h1>Sendy API Settings</h1>
	<div class="container">
	<?php if($updateMessage){ ?>
	<div class="notice notice-success is-dismissible">
        <p><?php _e( $updateMessage, 'sendy_api' ); ?></p>
    </div>
	<?php } ?>
	<h1>Sendy API Settings</h1>
	<form method="POST">
		<table class="widefat fixed striped tbl_sendy_api">
			<tr>
				<td>Sendy API Key</td>
				<td><input type="text" name="s_api[api_key]" value="<?php echo $options['api_key']; ?>" /></td>
			</tr>
			<tr>
				<td>Sendy Installation URL</td>
				<td><input type="text" name="s_api[url]" value="<?php echo $options['url']; ?>" /></td>
			</tr>
			<tr>
				<td>List ID</td>
				<td><input type="text" name="s_api[list_id]" value="<?php echo $options['list_id']; ?>" /></td>
			</tr>
			<?php /*<tr>
				<td>Success URL</td>
				<td><input type="text" name="s_api[success_url]" value="<?php echo $options['success_url']; ?>" /></td>
			</tr>
			<tr>
				<td>Fail URL</td>
				<td><input type="text" name="s_api[fail_url]" value="<?php echo $options['fail_url']; ?>" /></td>
			</tr> */ ?>
			<tr>
				<td colspan="2">
					<input type="submit" class="button button-primary" value="Save Changes" name="sendy_submit" />
				</td>
			</tr>
		</table>
	</form>
	</div>
	<div class="instru">
		<p>Subscribe <code>do_action( 'sendy_subscribe', '<span>[FULL NAME]</span>', '<span>[EMAIL]</span>', '<span>[WP_USERID]</span>', '<span>[AffID]</span>' );</code> <span class="info">// [WP_USERID] , [AffID] is a Custom Field</span></p>
		<p>Unsubscribe <code>do_action( 'sendy_unsubscribe', '<span>[EMAIL]</span>' );</code></p>
		<p>Delete Subscriber <code>do_action( 'sendy_delete_subscriber', '<span>[EMAIL]</span>' );</code></p>
		<p>Subscription Status <code>do_action( 'sendy_subscription_status', '<span>[EMAIL]</span>' );</code></p>
		<p>Get Subscriber Count <code>do_action( 'sendy_active_subscriber_count' );</code></p>
	</div>
</div>