<?php


function wpb_restore_extract() {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$action = 'truncate_table';
$sql_filename = '';
$offset = '';
$status = '';
$truncate_table = rs_importdb_logs($action,$sql_filename,$offset,$status);

$cleanup_dir = rs_cleanup_wpb_restore_backup_dir();
$return_val['msg'] .= $cleanup_dir['msg'];

$return_val['msg'] .= "Extracting backup files started <br/>";

$backup_file = sanitize_text_field(WPB_API_Authenticate::get_arg( 'backup_file' ));
$execution_time = WPB_API_Authenticate::get_arg('execution_time');
$file_calling_action = WPB_API_Authenticate::get_arg('file_calling_action');

$max_execution_time = (int) @ini_get('max_execution_time');
$max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);

if( !empty($backup_file) ) {

// GET WPBLAZER BACKUP DIR
$wpb3_backup_dir = get_option('wpb_backup_dir');

if(!empty($wpb3_backup_dir)){
$return_val['msg'] .= "wpblazer backup directory ($wpb3_backup_dir) found <br/>";
$wpb3_backup_file = $wpb3_backup_dir . '/' . $backup_file;

if (file_exists($wpb3_backup_file) ) {
$return_val['msg'] .= "wpblazer backup zip file ($wpb3_backup_file) exists <br/>";

$wpb3_backup_file_size = round(filesize($wpb3_backup_file)/1048576, 1).' MB';
$return_val['msg'] .= "wpblazer backup zip file size =  $wpb3_backup_file_size <br/>";

require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "accessing wp filesystem <br/>";

$wpcontent_dir = WP_CONTENT_DIR;
$wpupgrade_dir = $wpcontent_dir . '/upgrade/';

// Clean up contents of upgrade directory beforehand.
$upgrade_files = $wp_filesystem->dirlist($wpupgrade_dir); //print_r($upgrade_files);
if ( !empty($upgrade_files) ) {
foreach ( $upgrade_files as $file ) {
//echo "delete file = ".$wpupgrade_dir . $file['name']."<br/>";
$wp_filesystem->delete($wpupgrade_dir . $file['name'], true);
}
}

$working_dir = $wpupgrade_dir . '/wpb-restore';

$return_val['msg'] .= "extracting dir = $working_dir <br/>";

// Clean up working directory
if ( $wp_filesystem->is_dir($working_dir) ){
$wp_filesystem->delete($working_dir, true);
$return_val['msg'] .= "Clean up extracting dir $working_dir <br/>";
}

// Unzip package to working directory
if ('.zip' == strtolower(substr($wpb3_backup_file, -4, 4))) {

$return_val['msg'] .= "files extracting... <br/>";

$exec = function_exists('exec') ? 'Enabled' : 'Disabled';

$shell_exec = function_exists('shell_exec') ? 'Enabled' : 'Disabled';

$system = function_exists('system') ? 'Enabled' : 'Disabled';

$zip_class = class_exists('ZipArchive') ? 'Enabled' : 'Disabled';

$wp_unzip_file = function_exists('unzip_file') ? 'Enabled' : 'Disabled';
	
	
if ($exec == 'Enabled') {

$wpb3_backup_file = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_file))));

$working_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($working_dir))));

$return_val['msg'] .= "EXEC COMMAND processing... <br/>";

$return_val['msg'] .= "backup file = $wpb3_backup_file <br/>";

$return_val['msg'] .= "working dir = $working_dir <br/>";

$output = exec("unzip -o $wpb3_backup_file -d $working_dir", $zip_output, $zip_returnval);

//print_r($zip_output);
//echo "<br/> exec result = $zip_returnval <br/>";

if (!$zip_returnval) {
$return_val['status'] = 'success';
$return_val['msg'] .= "SUCCESS - backup file extracted <br/>";
} else {
$return_val['status'] = 'failed';
$return_val['msg'] .= "ERROR - There was an error extracting the backup file <br/>";
}

} elseif ($shell_exec == 'Enabled') {

$wpb3_backup_file = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_file))));

$working_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($working_dir))));

$return_val['msg'] .= "SHELL_EXEC COMMAND processing... <br/>";

$return_val['msg'] .= "backup file = $wpb3_backup_file <br/>";

$return_val['msg'] .= "working dir = $working_dir <br/>";

$output = shell_exec("unzip -o $wpb3_backup_file -d $working_dir 2>&1");

$return_val['status'] = 'success';
$return_val['msg'] .= "SUCCESS - backup file extracted <br/>";

} elseif ($system == 'Enabled') {

$wpb3_backup_file = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_file))));

$working_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($working_dir))));

$return_val['msg'] .= "SYSTEM COMMAND processing... <br/>";

$return_val['msg'] .= "backup file = $wpb3_backup_file <br/>";

$return_val['msg'] .= "working dir = $working_dir <br/>";

$output = system("unzip -o $wpb3_backup_file -d $working_dir");

$return_val['status'] = 'success';
$return_val['msg'] .= "SUCCESS - backup file extracted <br/>";

} elseif ($zip_class == 'Enabled') {

$wpb3_backup_file = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_file))));

$return_val['msg'] .= "ZIP_ZRCHIVE method processing... <br/>";

$zip = new ZipArchive;
$res = $zip->open($wpb3_backup_file);

if ($res === TRUE) {
$zip->extractTo($working_dir);
$zip->close();
$return_val['status'] = 'success';
$return_val['msg'] .= "SUCCESS - backup file extracted <br/>";
} else {
$return_val['status'] = 'failed';
$return_val['msg'] .= "ERROR - There was an error extracting the backup file <br/>";
}

} elseif($wp_unzip_file == 'Enabled') {

$wpb3_backup_file = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_file))));

$return_val['msg'] .= "wordpress unzip_file method processing... <br/>";

$result = unzip_file( $wpb3_backup_file, $working_dir );

if ( $result ) {
$return_val['msg'] .= "SUCCESS - backup file extracted <br/>";
$return_val['status'] = 'success';
} else {
if ( is_wp_error($result) ) {
$wp_filesystem->delete($working_dir, true);
if ( 'incompatible_archive' == $result->get_error_code() ) {
$return_val['msg'] .= "ERROR - incompatible_archive".$result->get_error_data()." <br/>";
$return_val['status'] = 'failed';
}
}
$return_val['msg'] .= "ERROR - There was an error extracting the backup file <br/>";
$return_val['status'] = 'failed';
}

}else {
$return_val['msg'] .= "ERROR - no extract method processing... <br/>";
$return_val['status'] = 'failed';
}

if($return_val['status'] == 'failed'){
$wp_filesystem->delete($working_dir, true);
$return_val['msg'] .= "Clean up extracting dir $working_dir <br/>";
}

} else {
$return_val['msg'] .= "ERROR - wpblazer backup file is not a .zip format <br/>";
$return_val['status'] = 'failed';
}

}

} else {
$return_val['msg'] .= "ERROR - wpblazer backup zip file ($backup_file) not exists <br/>";
$return_val['status'] = 'failed';
}
	
} else {
$return_val['msg'] .= "ERROR - wpblazer backup directory not found <br/>";
$return_val['status'] = 'failed';
}
	
} else {
$return_val['msg'] .= "ERROR - backup file name empty <br/>";
$return_val['status'] = 'failed';
}

return $return_val;
}



function wpb_restore_importdb() {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "data importing started <br/>";

$backup_file = sanitize_text_field(WPB_API_Authenticate::get_arg( 'backup_file' ));
$execution_time = WPB_API_Authenticate::get_arg('execution_time');
$file_calling_action = WPB_API_Authenticate::get_arg('file_calling_action');

$max_execution_time = (int) @ini_get('max_execution_time');
$max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);

$wp_db_host = defined('DB_HOST')  ? DB_HOST : '';
$wp_db_name = defined('DB_NAME')  ? DB_NAME : '';
$wp_db_user = defined('DB_USER')  ? DB_USER : '';
$wp_db_pass = defined('DB_PASSWORD')  ? DB_PASSWORD : '';

$wpcontent_dir = WP_CONTENT_DIR;
$wpupgrade_dir = $wpcontent_dir . '/upgrade/';
$working_dir = $wpupgrade_dir . '/wpb-restore';
	
$sql_file = rs_get_sqlfile($working_dir);

if (count($sql_file) == 0) {
$return_val['msg'] .= "ERROR - sql file not found";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "sql file found <br/>";
$return_val['msg'] .= "sql file count = ".count($sql_file)." <br/>";

$max_packet_size = rs_get_max_packet_size();
$return_val['msg'] .= "max packet size =  $max_packet_size <br/>";

$table_prefix = rs_get_table_prefix();
$return_val['msg'] .= "table prefix =  $table_prefix <br/>";

for ($i = 0; $i < count($sql_file); $i++) {

$sqlfile = $sql_file[$i]; 
$sqlfile = $working_dir.'/'.$sqlfile;
$sqlfile_size = round(filesize($sqlfile)/1048576, 1).' MB';

$return_val['msg'] .= "sql file(".$sql_file[$i].") size = $sqlfile_size <br/>";

$action = 'create_table';
$sql_filename = $sql_file[$i];
$offset = '';
$status = '';
$create_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($create_result['msg']) ? $create_result['msg'] : '';


$action = 'select_data';
$sql_filename = $sql_file[$i];
$offset = '';
$status = '';
$sel_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($sel_result['msg']) ? $sel_result['msg'] : '';

$log_data = isset($sel_result['data']) ? $sel_result['data'] : array();

if( count($log_data) > 0 ) {

$file_name = $log_data[0]->file_name;
$offset = $log_data[0]->offset;
$status = $log_data[0]->status;

if(!empty($file_name) && $file_name == $sql_filename && $status == 'complete') {
$return_val['msg'] .= "$file_name data alreday imported <br/>";
$return_val['status'] = 'success';
continue;
}

} else {
$action = 'insert_data';
$sql_filename = $sql_file[$i];
$offset = 0;
$status = 'start';

$insert_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($insert_result['msg']) ? $insert_result['msg'] : '';
}


if( class_exists( 'mysqli' ) ) {

$return_val['msg'] .= "SUCCESS - mysqli method processing <br/>";
$data_imports = rs_mysqli_import($sqlfile, $wp_db_host, $wp_db_name, $wp_db_user, $wp_db_pass,$file_calling_action,$max_execution_time,$offset);

//echo "<pre>";print_r($data_imports);echo "</pre>";

$import_msg = isset($data_imports['msg']) ? $data_imports['msg'] : '';
$import_status = isset($data_imports['status']) ? $data_imports['status'] : '';

if($import_status == ''){

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'failed';

} elseif($import_status == 'restart') {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'restart';

return $return_val;

} elseif($import_status == 'success') {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'success';

} elseif($import_status == 'failed') {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'failed';

} else {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'failed';

}

}
elseif( class_exists( 'mysql' ) ){

$return_val['msg'] .= "SUCCESS - mysql method processing <br/>";
$data_imports = rs_mysql_import($sqlfile, $wp_db_host, $wp_db_name, $wp_db_user, $wp_db_pass,$file_calling_action,$max_execution_time,$offset);

//echo "<pre>";print_r($data_imports);echo "</pre>";

$import_msg = isset($data_imports['msg']) ? $data_imports['msg'] : '';
$import_status = isset($data_imports['status']) ? $data_imports['status'] : '';

if($import_status == ''){

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'failed';

} elseif($import_status == 'restart') {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'restart';

return $return_val;

} elseif($import_status == 'success') {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'success';

} elseif($import_status == 'failed') {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'failed';

} else {

$return_val['msg'] .= $import_msg;
$return_val['status'] = 'failed';

}

}else {
$return_val['msg'] .= "ERROR - data importing process not happend <br/>";
$return_val['status'] = 'failed';
}

} // END : FOR LOOP

} // END : ELSE LOOP

return $return_val;
}


function wpb_restore_movefiles() {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$action = 'truncate_table';
$sql_filename = '';
$offset = '';
$status = '';
$truncate_table = rs_importdb_logs($action,$sql_filename,$offset,$status);

$return_val['msg'] .= "backup files moving started <br/>";

$backup_file = sanitize_text_field(WPB_API_Authenticate::get_arg( 'backup_file' ));
$execution_time = WPB_API_Authenticate::get_arg('execution_time');
$file_calling_action = WPB_API_Authenticate::get_arg('file_calling_action');

$max_execution_time = (int) @ini_get('max_execution_time');
$max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);


require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
//$return_val['msg'] .= "accessing wp filesystem <br/>";
//$return_val['status'] = 'success';

$home_path = get_home_path();

$dest_dir = $home_path;

$dest_dir1 = $home_path;

if( empty($home_path) ){
$return_val['msg'] .= "ERROR - home_path empty <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "home path = $home_path <br/>";
}

$wpadmin_dir = $home_path . 'wp-admin';  
$return_val['msg'] .= "wpadmin directory = $wpadmin_dir <br/>";

$wpincludes_dir = $home_path . 'wp-includes';  
$return_val['msg'] .= "wpincludes directory = $wpincludes_dir <br/>";

$wpcontent_dir = defined('WP_CONTENT_DIR')  ? WP_CONTENT_DIR : '';

$wpcontent_dir = rtrim($wpcontent_dir,"/");

$wpcontent_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpcontent_dir))));

if( empty($wpcontent_dir) ){
$return_val['msg'] .= "ERROR - wpcontent directory empty <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "wpcontent directory = $wpcontent_dir <br/>";
}

$wpupgrade_dir = $wpcontent_dir . '/upgrade/'; 

$wpupgrade_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpupgrade_dir))));

$wpupgrade_dir = rtrim($wpupgrade_dir,"/");

$working_dir = $wpupgrade_dir . '/wpb-restore';

$return_val['msg'] .= "wpblazer restore directory = $working_dir <br/>";

if ( $wp_filesystem->is_dir($working_dir) ) {
//$return_val['msg'] .= "wpblazer restore directory exists <br/>";

$restore_files = $wp_filesystem->dirlist($working_dir,true);

if( count($restore_files) > 0 ) {
//$return_val['msg'] .= "wpblazer restore directory contains".count($restore_files)." files <br/>";
$return_val['status'] = 'success';

foreach ($restore_files as $rname => $rfile) {

//echo "<pre>";print_r($rfile);echo "</pre><br/><hr/>";

if( 'd' != $rfile['type'] ) {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "$rname file moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname file moving completed <br/>";
}

} elseif( 'd' == $rfile['type'] ) {

if ($dest_dir.$rname == $wpadmin_dir) {
$return_val['msg'] .= "Skipping ($wpadmin_dir) directory <br/>";
continue;
} elseif ($dest_dir.$rname == $wpincludes_dir) {
$return_val['msg'] .= "Skipping ($wpincludes_dir) directory <br/>";
continue;
} elseif ($dest_dir.$rname == $wpcontent_dir) {
$return_val['msg'] .= "Skipping ($wpcontent_dir) directory <br/>";
//continue;

// EXISTING WPCONTENT FILES BACKUP SCRIPT
$movefiles_result1 = rs_wpcontent_existing_files_move($max_execution_time);

$move_msg1 = isset($movefiles_result1['msg']) ? $movefiles_result1['msg'] : '';
$move_status1 = isset($movefiles_result1['status']) ? $movefiles_result1['status'] : '';

if($move_status1 == ''){
$return_val['msg'] .= $move_msg1;
$return_val['status'] = 'failed';
} elseif($move_status1 == 'restart') {
$return_val['msg'] .= $move_msg1;
$return_val['status'] = 'restart';
return $return_val;
} elseif($move_status1 == 'success') {
$return_val['msg'] .= $move_msg1;
$return_val['status'] = 'success';
} elseif($move_status1 == 'failed') {
$return_val['msg'] .= $move_msg1;
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= $move_msg1;
$return_val['status'] = 'failed';
}

// EXISTING PLUGIN FILES BACKUP SCRIPT
$movefiles_result2 = rs_wpcontent_plugin_existing_files_move($max_execution_time);

$move_msg2 = isset($movefiles_result2['msg']) ? $movefiles_result2['msg'] : '';
$move_status2 = isset($movefiles_result2['status']) ? $movefiles_result2['status'] : '';

if($move_status2 == ''){
$return_val['msg'] .= $move_msg2;
$return_val['status'] = 'failed';
} elseif($move_status2 == 'restart') {
$return_val['msg'] .= $move_msg2;
$return_val['status'] = 'restart';
return $return_val;
} elseif($move_status2 == 'success') {
$return_val['msg'] .= $move_msg2;
$return_val['status'] = 'success';
} elseif($move_status2 == 'failed') {
$return_val['msg'] .= $move_msg2;
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= $move_msg2;
$return_val['status'] = 'failed';
}

// RESTORE FILE MOVING SCRIPT
$movefiles_result = rs_wpcontent_movefiles($max_execution_time);

$move_msg = isset($movefiles_result['msg']) ? $movefiles_result['msg'] : '';
$move_status = isset($movefiles_result['status']) ? $movefiles_result['status'] : '';

if($move_status == ''){

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'failed';

} elseif($move_status == 'restart') {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'restart';

return $return_val;

} elseif($move_status == 'success') {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'success';

} elseif($move_status == 'failed') {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'failed';

} else {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'failed';

}

} else {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "$rname directory moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname directory moving completed <br/>";
}

}

} else {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "$rname file moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname file moving completed <br/>";
}

}


$time_remaining = microtime(TRUE) - $start_time;

if ($time_remaining >= ( $max_execution_time - 3 )) {
$return_val['msg'] .= "restart process... <br/>";
$return_val['status'] = 'restart';
return $return_val;
}

} // END : FOR EACH

$return_val['msg'] .= "backup files moving completed <br/>";
$return_val['status'] = 'success';


} else {
$return_val['msg'] .= "ERROR - wpblazer restore directory contains zero files <br/>";
$return_val['status'] = 'failed';
}

} else {
$return_val['msg'] .= "ERROR - wpblazer restore directory not exists <br/>";
$return_val['status'] = 'failed';
}

}

return $return_val;
}


function rs_wpcontent_movefiles($max_execution_time = null){

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "wp content files moving started <br/>";

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);


require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
//$return_val['msg'] .= "accessing wp filesystem <br/>";


$home_path = get_home_path();

if( empty($home_path) ){
$return_val['msg'] .= "ERROR - home_path empty <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "home path = $home_path <br/>";
}

$dest_dir = rtrim($home_path,"/").'/wp-content/';

$dest_dir1 = rtrim($home_path,"/").'/wp-content/';

if ( $wp_filesystem->is_dir($dest_dir1) ) {
$return_val['msg'] .= "destination directory ($dest_dir1) exists <br/>";
} else {
$return_val['msg'] .= "ERROR - destination directory ($dest_dir1) not exists <br/>";
$return_val['status'] = 'failed';
}

$wpcontent_dir = defined('WP_CONTENT_DIR')  ? WP_CONTENT_DIR : '';

if( empty($wpcontent_dir) ){
$return_val['msg'] .= "ERROR - wp content directory path empty <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "wp content directory = $wpcontent_dir <br/>";
}

$wpcontent_dir = rtrim($wpcontent_dir,"/");

$wpcontent_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpcontent_dir))));

$working_dir = $wpcontent_dir . '/upgrade/wpb-restore/wp-content'; 

$return_val['msg'] .= "wpblazer working directory = $working_dir <br/>";

// GET WP UPGRADE DIR
$wpupgrade_dir = $wpcontent_dir . '/upgrade/'; 
$wpupgrade_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpupgrade_dir))));
$wpupgrade_dir = rtrim($wpupgrade_dir,"/");
$return_val['msg'] .= "wp upgrade directory = $wpupgrade_dir <br/>";

// GET WP PLUGIN DIR
$wpplugin_dir = $wpcontent_dir . '/plugins/'; 
$wpplugin_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpplugin_dir))));
$wpplugin_dir = rtrim($wpplugin_dir,"/");
$return_val['msg'] .= "wp plugin directory = $wpplugin_dir <br/>";

// GET WPBLAZER BACKUP DIR
$wpb3_backup_dir = get_option('wpb_backup_dir'); 
$wpb3_backup_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_dir))));
$return_val['msg'] .= "wpblazer backup dir = $wpb3_backup_dir <br/>";

if ( $wp_filesystem->is_dir($working_dir) ) {

//$return_val['msg'] .= "wpcontent dir exists <br/>";
$wpcontent_files = $wp_filesystem->dirlist($working_dir,true); //echo "<pre>";print_r($upgrade_files);echo "</pre>";

if( count($wpcontent_files) > 0 ) {
//$return_val['msg'] .= "wpcontent dir dir contains".count($wpcontent_files)." files <br/>";
$return_val['status'] = 'success';

foreach ($wpcontent_files as $rname => $rfile) {

//echo "<pre>";print_r($rfile);echo "</pre><br/><hr/>";

if( 'd' != $rfile['type'] ) {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "$rname file moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname file moving completed <br/>";
}


} elseif( 'd' == $rfile['type'] ) {

if ($dest_dir.$rname == $wpb3_backup_dir) {
$return_val['msg'] .= "Skipping ($wpb3_backup_dir) directory <br/>";
continue;
} elseif($dest_dir.$rname == $wpupgrade_dir) {
$return_val['msg'] .= "Skipping ($wpupgrade_dir) directory<br/>";
continue;
} elseif($dest_dir.$rname == $wpplugin_dir) {
$return_val['msg'] .= "Skipping ($wpplugin_dir) directory <br/>";
//continue;
$movefiles_result = rs_wpcontent_plugins_movefiles($max_execution_time);

$move_msg = isset($movefiles_result['msg']) ? $movefiles_result['msg'] : '';
$move_status = isset($movefiles_result['status']) ? $movefiles_result['status'] : '';

if($move_status == ''){

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'failed';

} elseif($move_status == 'restart') {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'restart';

return $return_val;

} elseif($move_status == 'success') {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'success';

} elseif($move_status == 'failed') {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'failed';

} else {

$return_val['msg'] .= $move_msg;
$return_val['status'] = 'failed';

}

} else {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "$rname directory moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname directory moving completed <br/>";
}

}

} else {


if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "$rname file moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname file moving completed <br/>";
}

}

$time_remaining = microtime(TRUE) - $start_time;

if ($time_remaining >= ( $max_execution_time - 3 )) {
$return_val['msg'] .= "restart process... <br/>";
$return_val['status'] = 'restart';
return $return_val;
}

} // END : FOR EACH

$return_val['msg'] .= "wp content files moving completed <br/>";
$return_val['status'] = 'success';

} else {
$return_val['msg'] .= "ERROR - wpcontent dir contains zero files <br/>";
$return_val['status'] = 'failed';
}

} else {
$return_val['msg'] .= "ERROR - wpcontent dir not exists <br/>";
$return_val['status'] = 'failed';
}

} // END : ELSE LOOP

return $return_val;
}


function rs_wpcontent_plugins_movefiles($max_execution_time = null){

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "wp content plugins files moving started <br/>";

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);

require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
//$return_val['msg'] .= "accessing wp filesystem <br/>";

$home_path = get_home_path();

if( empty($home_path) ){
$return_val['msg'] .= "ERROR - home_path empty <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "home path = $home_path <br/>";
}

$dest_dir = rtrim($home_path,"/").'/wp-content/plugins/';

$dest_dir1 = rtrim($home_path,"/").'/wp-content/plugins/';

if ( $wp_filesystem->is_dir($dest_dir1) ) {
$return_val['msg'] .= "destination directory ($dest_dir1) exists <br/>";
} else {
$return_val['msg'] .= "ERROR - destination directory ($dest_dir1) not exists <br/>";
$return_val['status'] = 'failed';
}

$wpcontent_dir = defined('WP_CONTENT_DIR')  ? WP_CONTENT_DIR : '';

if( empty($wpcontent_dir) ){
$return_val['msg'] .= "ERROR - wp content directory path empty <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "wp content directory = $wpcontent_dir <br/>";
}

$wpcontent_dir = rtrim($wpcontent_dir,"/");

$wpcontent_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpcontent_dir))));

$working_dir = $wpcontent_dir . '/upgrade/wpb-restore/wp-content/plugins'; 

$return_val['msg'] .= "wpblazer working directory = $working_dir <br/>";

// GET WP PLUGIN DIR
$wpplugin_dir = $wpcontent_dir . '/plugins/'; 
$wpplugin_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpplugin_dir))));
$wpplugin_dir = rtrim($wpplugin_dir,"/");
$return_val['msg'] .= "plugin directory = $wpplugin_dir <br/>";

// GET CURRENT PLUGIN DIR
$current_plugin_dir = dirname(__FILE__);
$current_plugin_dir = rtrim($current_plugin_dir,"/");
$current_plugin_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($current_plugin_dir))));
$return_val['msg'] .= "wpblazer plugin directory = $current_plugin_dir <br/>";



if ( $wp_filesystem->is_dir($working_dir) ) {

//$return_val['msg'] .= "wpcontent plugins directory exists <br/>";

$wpcontent_files = $wp_filesystem->dirlist($working_dir,true); //echo "<pre>";print_r($upgrade_files);echo "</pre>";

if( count($wpcontent_files) > 0 ) {

//$return_val['msg'] .= "wpcontent plugins directory contains".count($wpcontent_files)." files <br/>";
$return_val['status'] = 'success';

foreach ($wpcontent_files as $rname => $rfile) {

//echo "<pre>";print_r($rfile);echo "</pre><br/><hr/>";

if( 'd' != $rfile['type'] ) {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "plugin ($rname) file moving failed <br/>";
} else {
//$return_val['msg'] .= "plugin ($rname) file moving completed <br/>";
}


} elseif( 'd' == $rfile['type'] ) {

if ($dest_dir.$rname == $current_plugin_dir) {
$return_val['msg'] .= "Skipping ($current_plugin_dir) <br/>";
continue;
} else {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "plugin ($rname) directory moving failed <br/>";
} else {
//$return_val['msg'] .= "plugin ($rname) directory moving completed <br/>";
}


}

} else {

if (!$wp_filesystem->move($working_dir.'/'.$rname, $dest_dir1.'/'.$rname, true)) {
$return_val['msg'] .= "plugin($rname) file moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname file moving completed <br/>";
}


}

$time_remaining = microtime(TRUE) - $start_time;

if ($time_remaining >= ( $max_execution_time - 3 )) {
$return_val['msg'] .= "restart process... <br/>";
$return_val['status'] = 'restart';
return $return_val;
}


} // END : FOR EACH

$return_val['msg'] .= "wp content plugins files moving completed <br/>";
$return_val['status'] = 'success';

} else {
$return_val['msg'] .= "wp content plugins directory contains zero files <br/>";
//$return_val['status'] = 'failed';
}

} else {
$return_val['msg'] .= "ERROR - wpcontent plugins directory not exists <br/>";
$return_val['status'] = 'failed';
}



} // END : ELSE LOOP

return $return_val;
}


function rs_importdb_logs($action=null,$sql_filename=null,$offset=null,$status=null){

$return_val = '';
$return_val['data'] = '';
$return_val['msg'] = '';
$return_val['status'] = '';

global $wpdb;
$table_name = $wpdb->prefix . 'wpb_restore_log';

$return_val['msg'] .= 'import data log result : ';

if( $action == 'create_table' ) {

if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
$charset_collate = $wpdb->get_charset_collate();
$sql = "CREATE TABLE $table_name (
		id int(10) NOT NULL AUTO_INCREMENT,
		file_name text,
		offset text,
		status varchar(10) DEFAULT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$result = dbDelta( $sql );
	
	if($result){
	$return_val['msg'] .= 'table created <br/>';
	$return_val['status'] = 'success';
	} else {
	$return_val['msg'] .= 'ERROR - table creation failed <br/>';
	$return_val['status'] = 'failed';
	}
	} else {
	$return_val['msg'] .= 'table already exists <br/>';
	$return_val['status'] = 'success';
   }
   
   } elseif( $action == 'drop_table' ) {
   
   $drop_table = $wpdb->query( "DROP TABLE IF EXISTS $table_name" );
   if ( false === $drop_table ) {
   $return_val['msg'] .= 'ERROR - drop table query failed <br/>';
   $return_val['status'] = 'failed';
   } else {
      $return_val['msg'] .= 'drop table query executed <br/>';
      $return_val['status'] = 'success';
   }
   
   } elseif( $action == 'truncate_table' ) {
   
   $truncate_table = $wpdb->query("TRUNCATE TABLE $table_name");
   
   if ( false === $truncate_table ) {
      $return_val['msg'] .= 'ERROR - truncate table query failed <br/>';
      $return_val['status'] = 'failed';
	} else {
      $return_val['msg'] .= 'truncate table query executed <br/>';
      $return_val['status'] = 'success';
	}

   } elseif( $action == 'select_data' ) {
   
   $sql = "SELECT * FROM $table_name WHERE file_name='$sql_filename'";
   $rows = $wpdb->get_results($sql);
   $return_val['data'] = $rows;
   $return_val['msg'] .= 'select query executed <br/>';
   $return_val['status'] = 'success';
   
   } elseif( $action == 'insert_data' ) {
   
   $sql = "INSERT INTO $table_name (file_name, offset, status) VALUES ('$sql_filename', '$offset', '$status');";                  
   $insert_data = $wpdb->query($sql);
   
   if ( false === $insert_data ) { 
      $return_val['msg'] .= 'ERROR - insert query failed <br/>';
      $return_val['status'] = 'failed';
	 } else {
      $return_val['msg'] .= 'insert query executed <br/>';
      $return_val['status'] = 'success';
	 }
   
   } elseif( $action == 'update_data' ) {
   
   	$update_data = $wpdb->query("UPDATE $table_name SET file_name='$sql_filename' , offset='$offset' , status='$status' WHERE file_name='$sql_filename'");
 
   if ( false === $update_data ) {
      $return_val['msg'] .= 'ERROR - update query failed <br/>';
      $return_val['status'] = 'failed';
	} else {
      $return_val['msg'] .= 'update query executed <br/>';
      $return_val['status'] = 'success';
    }
	
   } else {
    $return_val['msg'] .= 'ERROR - nothing happend <br/>';
    $return_val['status'] = 'failed';
   }
 
return $return_val;
}


function rs_mysqli_import($sqlfile, $wp_db_host, $wp_db_name, $wp_db_user, $wp_db_pass,$file_calling_action,$max_execution_time,$offset) {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$connection = mysqli_connect($wp_db_host,$wp_db_user,$wp_db_pass,$wp_db_name);
	
if (mysqli_connect_errno()){
$return_val['msg'] .= "ERROR - Failed to connect to MySQLI: " . mysqli_connect_error()." <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "mysqli connected <br/>";

if ($file_handle = fopen($sqlfile, 'rb')) {
$start_time = microtime(TRUE);
$query = '';

$offset = !empty($offset) ? $offset : 0;

$return_val['msg'] .= "starting offset = $offset <br/>";

fseek($file_handle, $offset);
$x = 0;
while (!feof($file_handle)) {
$data = fread($file_handle, 1048576 * 1); //1MB

if (strlen($data) == 0) {
continue;
}

$lines = explode("\n",$data); 
//echo "<pre>";print_r($lines);echo "</pre>";

foreach ($lines as $line) {

//echo "$line <br/>";

// Skip it if it's a comment
if (substr($line, 0, 2) == '--' || $line == '')
continue;

if (substr($line, 0, 1) == '#' || $line == '')
continue;

// Add this line to the current segment
$query .= $line;

// If it has a semicolon at the end, it's the end of the query
if (substr(trim($line), -1, 1) == ';') {


global $wpdb;
$table_name = $wpdb->prefix . 'wpb_restore_log';

if( preg_match('/DROP TABLE IF EXISTS `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ) {
continue;
} elseif( preg_match('/CREATE TABLE `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ){
continue;
} elseif( preg_match('/CREATE TABLE IF NOT EXISTS `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ){
continue;
} elseif( preg_match('/INSERT INTO `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ){
continue;
}else {
//echo "<u>no mathces query process </u><br/>";
}

//echo "<br/> $query <br/><br/>";

if(mysqli_query($connection,$query)) {
//echo "<b>query result : </b> query executed <br/>";
} else {
//echo "<b>query result : </b> query execution failed. ".mysqli_error($connection)."<br/>";
}

$query = '';
}
 
} // END : FOR LOOP

$offset = $offset + strlen($data);
$query_offset = ftell( $file_handle );

$time_remaining = microtime(TRUE) - $start_time;
if ($time_remaining >= ( $max_execution_time - 3 )) {

$action = 'update_data';
$sql_filename = basename($sqlfile);
$status = 'restart';
$update_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($update_result['msg']) ? $update_result['msg'] : '';

$return_val['msg'] .= "Ending offset =  $offset <br/>";
$return_val['msg'] .= "restart mysqli process... <br/>";
$return_val['status'] = 'restart';
return $return_val;
}

$x++;
} // END : WHILE

$action = 'update_data';
$sql_filename = basename($sqlfile);
//$offset = 0;
$status = 'complete';
$update_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($update_result['msg']) ? $update_result['msg'] : '';

$return_val['msg'] .= "Ending offset =  $offset <br/>";
$return_val['msg'] .= "SUCCESS - data importing completed <br/>";
$return_val['status'] = 'success';

fclose($file_handle);
} // END : IF LOOP


} // END : ELSE LOOP

return $return_val;
}


function rs_mysql_import($sqlfile, $wp_db_host, $wp_db_name, $wp_db_user, $wp_db_pass,$file_calling_action,$max_execution_time,$offset) {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$connection = mysql_connect($wp_db_host, $wp_db_user, $wp_db_pass);

if (!$connection) {
$return_val['msg'] .= "ERROR - MYSQL Not Connected - Check Your Username/Password <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "mysql connected <br/>";
$db_selected = mysql_select_db($wp_db_name, $connection);

if (!$db_selected) {
$return_val['msg'] .= "ERROR - mysql database connection failed <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "mysql database connected successfully <br/>";

$exec = function_exists('exec') ? 'Enabled' : 'Disabled';

if ($exec == 'Enabled' && $wp_db_host == 'localhost') {

$return_val['msg'] .= "EXEC Method data importing process... <br/>";

$cmd = 'mysql -u ' . $wp_db_user . ' -p' . $wp_db_pass . ' ' . $wp_db_name . ' < ' . $sqlfile;
exec("$cmd 2>&1", $output, $return_var);

if (!$return_var) {
$return_val['status'] = 'success';
} else {
$return_val['status'] = 'failed';
}

} else {

if ($file_handle = fopen($sqlfile, 'rb')) {
$start_time = microtime(TRUE);
$query = '';

$offset = !empty($offset) ? $offset : 0;
$return_val['msg'] .= "starting offset = $offset <br/>";

fseek($file_handle, $offset);
$x = 0;
while (!feof($file_handle)) {
$data = fread($file_handle, 1048576 * 1); //1MB

if (strlen($data) == 0) {
continue;
}

$lines = explode("\n",$data); 
//echo "<pre>";print_r($lines);echo "</pre>";

foreach ($lines as $line) {

//echo "$line <br/>";

// Skip it if it's a comment
if (substr($line, 0, 2) == '--' || $line == '')
continue;

if (substr($line, 0, 1) == '#' || $line == '')
continue;

// Add this line to the current segment
$query .= $line;

// If it has a semicolon at the end, it's the end of the query
if (substr(trim($line), -1, 1) == ';') {

global $wpdb;
$table_name = $wpdb->prefix . 'wpb_restore_log';

if( preg_match('/DROP TABLE IF EXISTS `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ) {
continue;
} elseif( preg_match('/CREATE TABLE `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ){
continue;
} elseif( preg_match('/CREATE TABLE IF NOT EXISTS `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ){
continue;
} elseif( preg_match('/INSERT INTO `' . $table_name . '`/', "$query", $matches, PREG_OFFSET_CAPTURE) ){
continue;
}else {
//echo "<u>no mathces query process </u><br/>";
}

//echo "<br/> $query <br/><br/>";

if(mysql_query($connection,$query)) {
//echo "<b>query result : </b> query executed <br/>";
} else {
//echo "<b>query result : </b> query execution failed. ".mysqli_error($connection)."<br/>";
}
$query = '';
}
 
} // END : FOR LOOP

$offset = $offset + strlen($data);
$query_offset = ftell( $file_handle );

$time_remaining = microtime(TRUE) - $start_time;
if ($time_remaining >= ( $max_execution_time - 3 )) {

$action = 'update_data';
$sql_filename = basename($sqlfile);
$status = 'restart';
$update_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($update_result['msg']) ? $update_result['msg'] : '';

$return_val['msg'] .= "restart mysql process... <br/>";
$return_val['msg'] .= "Ending offset =  $offset <br/>";
$return_val['status'] = 'restart';
return $return_val;
}

$x++;
} // END : WHILE

$action = 'update_data';
$sql_filename = basename($sqlfile);
//$offset = 0;
$status = 'complete';
$update_result = rs_importdb_logs($action,$sql_filename,$offset,$status);
$return_val['msg'] .= isset($update_result['msg']) ? $update_result['msg'] : '';

$return_val['msg'] .= "Ending offset =  $offset <br/>";
$return_val['msg'] .= "SUCCESS - data importing completed <br/>";
$return_val['status'] = 'success';

fclose($file_handle);
} // END : IF LOOP


} // END : ELSE LOOP

} // END : ELSE LOOP

} // END : ELSE LOOP

return $return_val;
}



function rs_get_sqlfile($dir) {
$sql_file = array();
//$dir = dirname(__FILE__);
    if (is_dir($dir)) {
        $fd = @opendir($dir);
        while (($part = @readdir($fd)) == true) {
            if ($part != "." && $part != "..") {
                $file_array[] = $part;
            }
        }
        if ($fd == true) {
            closedir($fd);
        }
    }
    sort($file_array);
    reset($file_array);
    for ($i = 0; $i < count($file_array); $i++) {
        $npart = $file_array[$i];
        //echo "$npart <br/>";  
        if (strstr($npart, ".sql")) {
            //echo "$npart <br/>"; 
            array_push($sql_file, "$npart");
        }
    }
    return $sql_file;
}


function rs_get_max_packet_size() {
		global $wpdb;
		$mp = (int)$wpdb->get_var("SELECT @@session.max_allowed_packet");
		# Default to 1MB
		$mp = (is_numeric($mp) && $mp > 0) ? $mp : 1048576;
		# 32MB
		if ($mp < 33554432) {
			$save = $wpdb->show_errors(false);
			$req = @$wpdb->query("SET GLOBAL max_allowed_packet=33554432");
			$wpdb->show_errors($save);
			if (!$req) //echo "Tried to raise max_allowed_packet from ".round($mp/1048576,1)." MB to 32 MB, but failed (".$wpdb->last_error.", ".serialize($req).")";
			$mp = (int)$wpdb->get_var("SELECT @@session.max_allowed_packet");
			# Default to 1MB
			$mp = (is_numeric($mp) && $mp > 0) ? $mp : 1048576;
		}
		//echo "Max packet size: ".round($mp/1048576, 1)." MB";
		return $mp;
	}
	
	
function rs_get_table_prefix() {
		global $wpdb;
		if (is_multisite() && !defined('MULTISITE')) {
		} else {
			$prefix = $wpdb->get_blog_prefix(0);
		}
		return $prefix;
	}
	

function rs_get_plugin_data() {

	include_once ( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_path = dirname(__FILE__) . '/wpblazer.php';
	$site_infos = get_plugin_data($plugin_path);
	return $site_infos;
}


function rs_cleanup_working_dir(){

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "clean up works started <br/>";

require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "accessing wp filesystem <br/>";

$wpcontent_dir = WP_CONTENT_DIR;
$wpupgrade_dir = $wpcontent_dir . '/upgrade/';
$working_dir = $wpupgrade_dir . '/wpb-restore';

// Clean up working directory
if ( $wp_filesystem->is_dir($working_dir) ){
$wp_filesystem->delete($working_dir, true);
$return_val['msg'] .= "clean up works completed <br/>";
$return_val['status'] = 'success';
} else {
$return_val['msg'] .= "dir ($working_dir) not exists <br/>";
$return_val['msg'] .= "clean up works failed <br/>";
$return_val['status'] = 'failed';
}
}
return $return_val;

}


function rs_cleanup_wpb_restore_backup_dir(){

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "clean up works started <br/>";

require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
$return_val['msg'] .= "accessing wp filesystem <br/>";

$wpcontent_dir = defined('WP_CONTENT_DIR')  ? WP_CONTENT_DIR : '';

if( empty($wpcontent_dir) ){
$return_val['msg'] .= "ERROR - wp content directory path empty <br/>";
$return_val['status'] = 'failed';
} else {

$wpcontent_dir = rtrim($wpcontent_dir,"/");
$working_dir = $wpcontent_dir . '/wpb_restore_backup';

// Clean up working directory
if ( $wp_filesystem->is_dir($working_dir) ){
$wp_filesystem->delete($working_dir, true);
$return_val['msg'] .= "clean up works completed <br/>";
$return_val['status'] = 'success';
} else {
$return_val['msg'] .= "dir ($working_dir) not exists <br/>";
$return_val['msg'] .= "clean up works failed <br/>";
$return_val['status'] = 'failed';
}

}

}
return $return_val;

}



function rs_wpcontent_existing_files_move($max_execution_time = null) {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "wp content existing files moving started <br/>";

$max_execution_time = (int) @ini_get('max_execution_time');
$max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);

require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
//$return_val['msg'] .= "accessing wp filesystem <br/>";
//$return_val['status'] = 'success';

$wpcontent_dir = defined('WP_CONTENT_DIR')  ? WP_CONTENT_DIR : '';

if( empty($wpcontent_dir) ){
$return_val['msg'] .= "ERROR - wp content directory path empty <br/>";
$return_val['status'] = 'failed';
} else {

$wpcontent_dir = rtrim($wpcontent_dir,"/");

$wpcontent_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpcontent_dir))));

//$return_val['msg'] .= "wp content directory = $wpcontent_dir <br/>";

$wp_plugins = $wpcontent_dir.'/plugins';

$wp_upgrade = $wpcontent_dir.'/upgrade';

$wpb_restore_backup = $wpcontent_dir.'/wpb_restore_backup';

// GET WPBLAZER BACKUP DIR
$wpb3_backup_dir = get_option('wpb_backup_dir');
$wpb3_backup_dir = rtrim($wpb3_backup_dir,"/");
$wpb3_backup_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpb3_backup_dir))));
//$return_val['msg'] .= "wpblazer backup dir = $wpb3_backup_dir <br/>";


if( !$wp_filesystem->is_dir($wpb_restore_backup) ) {
//$return_val['msg'] .= "$wpb_restore_backup directory not exists <br/>";

if( $wp_filesystem->mkdir( $wpb_restore_backup ) ) {
//$return_val['msg'] .= "$wpb_restore_backup directory created <br/>";
}
else {
$return_val['msg'] .= "$wpb_restore_backup directory creation failed <br/>";
}

} else {
//$return_val['msg'] .= "$wpb_restore_backup directory already exists <br/>";
}


$wpcontent_files = $wp_filesystem->dirlist($wpcontent_dir,true); //echo "<pre>";print_r($wpcontent_files);echo "</pre>";

if( count($wpcontent_files) > 0 ) {
//$return_val['msg'] .= "wpcontent directory contains".count($wpcontent_files)." files <br/>";
$return_val['status'] = 'success';

foreach ($wpcontent_files as $rname => $rfile) {


if( 'd' != $rfile['type'] ) {

if (!$wp_filesystem->move($wpcontent_dir.'/'.$rname, $wpb_restore_backup.'/'.$rname, true)) {
$return_val['msg'] .= "($rname) file moving failed <br/>";
} else {
//$return_val['msg'] .= "($rname) file moving completed <br/>";
} 

}elseif( 'd' == $rfile['type'] ) {

if ($wpcontent_dir.'/'.$rname == $wp_plugins) {
$return_val['msg'] .= "Skipping ($wp_plugins) directory <br/>";
continue;
} elseif ($wpcontent_dir.'/'.$rname == $wp_upgrade) {
$return_val['msg'] .= "Skipping ($wp_upgrade) directory <br/>";
continue;
} elseif ($wpcontent_dir.'/'.$rname == $wpb_restore_backup) {
$return_val['msg'] .= "Skipping ($wpb_restore_backup) directory <br/>";
continue;
} elseif ($wpcontent_dir.'/'.$rname == $wpb3_backup_dir) {
$return_val['msg'] .= "Skipping ($wpb3_backup_dir) directory <br/>";
continue;
} else {

if (!$wp_filesystem->move($wpcontent_dir.'/'.$rname, $wpb_restore_backup.'/'.$rname, true)) {
$return_val['msg'] .= "($rname) directory moving failed <br/>";
} else {
//$return_val['msg'] .= "($rname) directory moving completed <br/>";
}

}

} else {

if (!$wp_filesystem->move($wpcontent_dir.'/'.$rname, $wpb_restore_backup.'/'.$rname, true)) {
$return_val['msg'] .= "($rname) file moving failed <br/>";
} else {
//$return_val['msg'] .= "$rname) file moving completed <br/>";
} 

}

$time_remaining = microtime(TRUE) - $start_time;

if ($time_remaining >= ( $max_execution_time - 3 )) {
$return_val['msg'] .= "restart process... <br/>";
$return_val['status'] = 'restart';
return $return_val;
} 

} // END : FOR EACH

$return_val['msg'] .= "wp content existing files moving completed <br/>";
$return_val['status'] = 'success';

} else {
//$return_val['msg'] .= "wp content directory contains zero files <br/>";
//$return_val['status'] = 'failed';
}

}

}

return $return_val;
}



function rs_wpcontent_plugin_existing_files_move($max_execution_time = null) {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "wp content plugin existing files moving started <br/>";

$max_execution_time = (int) @ini_get('max_execution_time');
$max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;

if ($max_execution_time == 0) {
$max_execution_time = 30;
@ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
@set_time_limit(0);
}

$start_time = microtime(TRUE);

require_once(ABSPATH .'/wp-admin/includes/file.php');

global $wp_filesystem;
if ( ! $filesystem ) {
  WP_Filesystem();
}

if ( ! $wp_filesystem || !is_object($wp_filesystem) ) {
$return_val['msg'] .= "ERROR - Could not access wp filesystem <br/>";
$return_val['status'] = 'failed';
} else {
//$return_val['msg'] .= "accessing wp filesystem <br/>";
//$return_val['status'] = 'success';

$wpcontent_dir = defined('WP_CONTENT_DIR')  ? WP_CONTENT_DIR : '';

if( empty($wpcontent_dir) ){
$return_val['msg'] .= "ERROR - wp content directory path empty <br/>";
$return_val['status'] = 'failed';
} else {
$wpcontent_dir = rtrim($wpcontent_dir,"/");

$wpcontent_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($wpcontent_dir))));

//$return_val['msg'] .= "wp content directory = $wpcontent_dir <br/>";

$wp_plugins = $wpcontent_dir.'/plugins';
$wpb_plugins_old = $wpcontent_dir.'/wpb_restore_backup/plugins';

if( !$wp_filesystem->is_dir($wpb_plugins_old) ) {
//$return_val['msg'] .= "$wpb_plugins_old directory not exists <br/>";

if( $wp_filesystem->mkdir( $wpb_plugins_old ) ) {
//$return_val['msg'] .= "$wpb_plugins_old directory created <br/>";
}
else {
$return_val['msg'] .= "$wpb_plugins_old directory creation failed <br/>";
}

} else {
//$return_val['msg'] .= "$wpb_plugins_old directory already exists <br/>";
}

// GET CURRENT PLUGIN DIR
$current_plugin_dir = dirname(__FILE__);
$current_plugin_dir = rtrim($current_plugin_dir,"/");
$current_plugin_dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($current_plugin_dir))));
//$return_val['msg'] .= "wpblazer plugin directory = $current_plugin_dir <br/>";


$wp_plugins_files = $wp_filesystem->dirlist($wp_plugins,true); //echo "<pre>";print_r($wp_plugins_files);echo "</pre>";

if( count($wp_plugins_files) > 0 ) {
//$return_val['msg'] .= "wpcontent plugins directory contains".count($wp_plugins_files)." files <br/>";
$return_val['status'] = 'success';

foreach ($wp_plugins_files as $rname => $rfile) {


if( 'd' != $rfile['type'] ) {

if (!$wp_filesystem->move($wp_plugins.'/'.$rname, $wpb_plugins_old.'/'.$rname, true)) {
$return_val['msg'] .= "plugin ($rname) file moving failed <br/>";
} else {
//$return_val['msg'] .= "plugin ($rname) file moving completed <br/>";
} 

}elseif( 'd' == $rfile['type'] ) {

if ($wp_plugins.'/'.$rname == $current_plugin_dir) {
$return_val['msg'] .= "Skipping ($current_plugin_dir) directory <br/>";
continue;
} else {

if (!$wp_filesystem->move($wp_plugins.'/'.$rname, $wpb_plugins_old.'/'.$rname, true)) {
$return_val['msg'] .= "plugin ($rname) directory moving failed <br/>";
} else {
//$return_val['msg'] .= "plugin ($rname) directory moving completed <br/>";
}

}

} else {

if (!$wp_filesystem->move($wp_plugins.'/'.$rname, $wpb_plugins_old.'/'.$rname, true)) {
$return_val['msg'] .= "plugin ($rname) file moving failed <br/>";
} else {
//$return_val['msg'] .= "plugin ($rname) file moving completed <br/>";
} 

}

$time_remaining = microtime(TRUE) - $start_time;

if ($time_remaining >= ( $max_execution_time - 3 )) {
$return_val['msg'] .= "restart process... <br/>";
$return_val['status'] = 'restart';
return $return_val;
} 

} // END : FOR EACH


$return_val['msg'] .= "wp content plugin existing files moving completed <br/>";
$return_val['status'] = 'success';

} else {
//$return_val['msg'] .= "wp content plugins directory contains zero files <br/>";
//$return_val['status'] = 'failed';
}

}

}

return $return_val;
}



function wpb_restore_testing() {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "wpblazer restore testing... <br/><br/>";

$backup_file = sanitize_text_field(WPB_API_Authenticate::get_arg( 'backup_file' ));
$execution_time = WPB_API_Authenticate::get_arg('execution_time');
$file_calling_action = WPB_API_Authenticate::get_arg('file_calling_action');
$do_action = WPB_API_Authenticate::get_arg('do_action');


if( $do_action == 'truncate_table' ) {

$action = 'truncate_table';
$sql_filename = '';
$offset = '';
$status = '';
$truncate_table = rs_importdb_logs($action,$sql_filename,$offset,$status);
echo "<pre>";print_r($truncate_table);echo "</pre>";

} elseif( $do_action == 'cleanup_working_dir' ) {

$cleanup = rs_cleanup_working_dir();
echo "<pre>";print_r($cleanup);echo "</pre>";

} else {
$return_val['msg'] .= "nothing perfomed";
}

return $return_val;
}


function wpb_restore_checking() {

$return_val = array();
$return_val['msg'] = '';
$return_val['status'] = '';

$return_val['msg'] .= "wpblazer restore checking... <br/><br/>";


$backup_file = sanitize_text_field(WPB_API_Authenticate::get_arg( 'backup_file' ));
//$execution_time = WPB_API_Authenticate::get_arg('execution_time');
//$file_calling_action = WPB_API_Authenticate::get_arg('file_calling_action');

$max_execution_time = (int) @ini_get('max_execution_time');
$return_val['msg'] .= "max_execution_time = $max_execution_time <br/><br/>";

$table_prefix = rs_get_table_prefix();
$return_val['msg'] .= "table_prefix = $table_prefix <br/><br/>";

$packet_size = rs_get_max_packet_size();
$return_val['msg'] .= "packet_size = $packet_size <br/><br/>";

$plugin_data = rs_get_plugin_data();
$plugin_version = isset($plugin_data['Version']) ? $plugin_data['Version'] : '';
$return_val['msg'] .= "wpblazer plugin version = $plugin_version <br/><br/>";

//$wpb3_backup_dir = !empty(get_option('wpb_backup_dir')) ? get_option('wpb_backup_dir') : '';
//$return_val['msg'] .= "wpblazer backup directory = $wpb3_backup_dir <br/><br/>";

//$fun_call = rs_wpcontent_existing_files_move();

//$fun_call = rs_wpcontent_plugin_existing_files_move();

$fun_call = rs_cleanup_wpb_restore_backup_dir();

$return_val['msg'] .= $fun_call['msg'];

return $return_val;

}
