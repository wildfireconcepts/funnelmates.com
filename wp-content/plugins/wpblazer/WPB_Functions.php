<?php

/**
 * Get Current WP Version
 */
function wpb_get_wp_version() {

	global $wp_version;
	
	return $wp_version;
}

/**
 * Get Site Information
 */
function wpb_get_site_info() {
    
	include_once ( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_path = dirname(__FILE__) . '/wpblazer.php';
	$site_infos = array(
		'site_url'	=> get_site_url(),
		'home_url'	=> get_home_url(),
		'admin_url'	=> get_admin_url(),
        'update_info'   => wp_get_update_data(),
        'wpblazer_version'   => get_plugin_data($plugin_path),
	);
	
	return $site_infos;
}


function wpb_precheck ($checktype) {

	switch($checktype) {

		case 'upgrade_check' :
			if ( ! _wpbr_supports_plugin_upgrade() )
			return array( 'status' => 'error', 'error' => 'WordPress version too old for plugin upgrades' );
		break;
		
		case 'update_check' :
			if ( ! _wpbr_supports_plugin_upgrade() )
			return array( 'status' => 'error', 'error' => 'WordPress version too old for theme upgrades' );
		break;
		
		case 'filesystem_access' :
			// check for filesystem access
			if ( ! _wpb_check_filesystem_access() )
			return array( 'status' => 'error', 'error' => 'The filesystem is not writable with the supplied credentials' );
		break;
		
		case 'filesystem_access_delete' :
			// check for filesystem access
			if ( ! _wpb_check_filesystem_access() || ! WP_Filesystem() )
			return array( 'status' => 'error', 'error' => 'The filesystem is not writable with the supplied credentials' );
		break;
		
	}
	
}

/**
 * Check if the site can support plugin upgrades
 */
function _wpbr_supports_plugin_upgrade() {

	include_once ( ABSPATH . 'wp-admin/includes/admin.php' );

	return class_exists( 'Plugin_Upgrader' );

}

/**
 * Check file system access
 */
function _wpb_check_filesystem_access() {

	ob_start();
	$success = request_filesystem_credentials( '' );
	ob_end_clean();

	return (bool) $success;
}

function _wpb_set_filesystem_credentials( $credentials ) {

	if ( empty( $_GET['filesystem_details'] ) )
		return $credentials;

	$_credentials = array(
		'username' => $_GET['filesystem_details']['credentials']['username'],
		'password' => $_GET['filesystem_details']['credentials']['password'],
		'hostname' => $_GET['filesystem_details']['credentials']['hostname'],
		'connection_type' => $_GET['filesystem_details']['method']
	);

	// check whether the credentials can be used
	if ( ! WP_Filesystem( $_credentials ) ) {
		return $credentials;
	}

	return $_credentials;
}
add_filter( 'request_filesystem_credentials', '_wpb_set_filesystem_credentials' );

/**
 * Upgrade Core WordPress
 */
function wpb_upgrade_core()  {

	include_once ( ABSPATH . 'wp-admin/includes/admin.php' );
	include_once ( ABSPATH . 'wp-admin/includes/upgrade.php' );
	include_once ( ABSPATH . 'wp-includes/update.php' );
	require_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
	require_once WPBLAZER_PLUGIN_URL . 'skin/wpblazer-core-upgrader-skin.php';

	// check filesystem access
	wpb_precheck ('filesystem_access');

	// force refresh
	wp_version_check();

	$updates = get_core_updates();

	if ( is_wp_error( $updates ) || ! $updates )
		return new WP_Error( 'no-update-available' );

	$update = reset( $updates );

	if ( ! $update )
		return new WP_Error( 'no-update-available' );

	if (!class_exists('WP_Upgrader')) {
            include_once(ABSPATH.'wp-admin/includes/class-wp-upgrader.php');
        }

	$upgrader_skin = new WPBLAZER_Core_Upgrader_Skin();
	$upgrader = new Core_Upgrader( $upgrader_skin );
	$result = $upgrader->upgrade($update);

	if ( is_wp_error( $result ) )
		return $result;

	global $wp_current_db_version, $wp_db_version;

	// we have to include version.php so $wp_db_version
	// will take the version of the updated version of wordpress
	require( ABSPATH . WPINC . '/version.php' );

	wp_upgrade();

	return true;
}

/**
 * Get Posts
 */
function wpb_get_posts() {

               $posts_per_page = WPB_API_Authenticate::get_arg( 'posts_per_page' );
               if(empty($posts_per_page))
                {
                  $posts_per_page = -1;
                }
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page
		);
		$query = new WP_Query( $args );
		
		if( !empty( $query->posts ) ) {
			foreach( $query->posts as $posts ) {
				$user = get_user_by( 'id', $posts->post_author );
				$list_posts[] = array(
					'ID' => $posts->ID,
					'post_author' => $posts->post_author,
					'post_author_name' => $user->display_name,
					'post_date' => $posts->post_date,
					'post_content' => $posts->post_content,
					'post_title' => $posts->post_title,
					'post_status' => $posts->post_status,
					'post_name' => $posts->post_name,
					'post_category' => wp_get_post_categories( $posts->ID ),
					'post_modified' => $posts->post_modified,
					'guid' => $posts->guid,
					'post_type' => $posts->post_type,
					'comment_count' => $posts->comment_count
				);
			}
		$actions = $list_posts;
        } else {
        $actions = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
        }
		
		return $actions;
}

/**
 * Get Post
 */
function wpb_get_post($id) {

		$post_id = (int)WPB_API_Authenticate::get_arg( 'post_id' );
		$post = get_post( $post_id );

		if ( ! $post ) {
		
			$actions = new WP_Error( 'missing-post', __( "No post found.", 'wpblazer' ) );
			
		} else {

		$actions = $post;

		}

		return $actions;
}

/**
 * Create Post
 */
function wpb_create_post() {

			$param_keys = array(
				'post_author',
				'post_content',
				'post_status',
				'post_title',
				'post_type',
                'post_category',
			);
			$args = array();
			foreach( $param_keys as $param_key ) {
				if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
					$args[$param_key] = $value;
			}
                        $tags = WPB_API_Authenticate::get_arg( 'post_tags' );

			if ( $post_id = wp_insert_post( $args ) ) {
                                wp_set_post_tags( $post_id, $tags );
				$actions = get_post( $post_id );
                        }else{
				$actions = new WP_Error( 'create-post', __( "Error creating post.", 'wpblazer' ) );
                        }

		return $actions;
}

/**
 * Update Post
 */
function wpb_update_post() {

			$param_keys = array(
				'post_author',
				'post_content',
				'post_status',
				'post_title',
				'post_type',
                'post_category',
			);
			$args = array();
			foreach( $param_keys as $param_key ) {
				if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
					$args[$param_key] = $value;
			}

			$args['ID'] = (int)WPB_API_Authenticate::get_arg( 'post_id' );

			if ( ! get_post( $args['ID'] ) ) {
				$actions = new WP_Error( 'missing-post', __( "No post found.", 'wpblazer' ) );
			}
                        
                        $tags = WPB_API_Authenticate::get_arg( 'post_tags' );

			if ( wp_update_post( $args ) ){
                                wp_set_post_tags( $args['ID'], $tags );
				$actions = get_post( $args['ID'] );
                        }else {
				$actions = new WP_Error( 'update-post', __( "Error updating post.", 'wpblazer' ) );
                        }

		return $actions;
}

/**
 * Delete Post
 */
function wpb_delete_post() {

		$post_id = (int)WPB_API_Authenticate::get_arg( 'post_id' );
		$post = get_post( $post_id );

		if ( ! $post ) {
		
			$actions = new WP_Error( 'missing-post', __( "No post found.", 'wpblazer' ) );
			
		} else {

			$actions = wp_delete_post( $post_id );

		}

		return $actions;
}

/*
 * 
 */
function wpb_get_categories() {
    
    $categories = get_categories(array('hide_empty'=>0));
    
    $a = array();
    $b = array();
    foreach($categories as $category) {
        $a[] = $category->cat_ID;
        $b[] = $category->name;
    }
    
    $result = array_combine($a, $b);
    
    return $result;
}

/*
 * 
 */
function wpb_get_category() {
    
    $post_id = (int)WPB_API_Authenticate::get_arg( 'post_id' );
    
    return wp_get_post_categories( $post_id );
    
}

/*
 * 
 */
function wpb_create_category(){
    
    require_once ABSPATH . '/wp-admin/includes/taxonomy.php';
    
    $param_keys = array(
            'cat_name',
            'category_description',
            'category_nicename',
            'category_parent',
    );
    
    $args = array();
    foreach( $param_keys as $param_key ) {
            if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
                    $args[$param_key] = $value;
    }
    
    if( $cat_id = wp_insert_category( $args ) ){
        $result = $cat_id;
    } else {
        $result = array( 'status' => 'error', 'error' => 'Category not created' );  
    }
    
    return $result;
    
}

/*
 * 
 */
function wpb_update_category() {
    
    require_once ABSPATH . '/wp-admin/includes/taxonomy.php';
    
    $param_keys = array(
            'cat_ID',
            'cat_name',
            'category_description',
            'category_nicename',
            'category_parent',
    );
    
    $args = array();
    foreach( $param_keys as $param_key ) {
            if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
                    $args[$param_key] = $value;
    }
    
    if( $cat_id = wp_update_category( $args ) ){
        $result = $cat_id;
    } else {
        $result = array( 'status' => 'error', 'error' => 'Category not updated' );  
    }
    
    return $result;
    
}

/*
 * 
 */
function wpb_delete_category() {
    
    $categ_ID = (int)WPB_API_Authenticate::get_arg( 'cat_ID' );
    
    if ( wp_delete_category( $categ_ID ) ) {        
        $result = "Category was successfully deleted";        
    } else {        
        $result = "Impossible to delete category! Make sure it exists and that it's not the default category";        
    }
    
    return $result;
}

/*
 * 
 */
function wpb_get_tags() {
    
    $tags = get_tags(array('hide_empty'=>0));
    
    return $tags;
}

/*
 * 
 */
function wpb_get_post_tags() {
    
    $post_id = (int)WPB_API_Authenticate::get_arg( 'post_id' );
    
    $tags = wp_get_post_tags($post_id);
    
    $result = array();
    foreach ( $tags as $tag ) {
	$result[] = $tag->name;
    }
    
    return $result;
}

/**
 * Get Comments
 */
function wpb_get_comments() {

               $comments_per_page = WPB_API_Authenticate::get_arg( 'comments_per_page' );
               if(empty($comments_per_page))
                {
                  $comments_per_page = '';
                }

               $args = array(
	                    'number' => $comments_per_page,
	                    'order' => 'DESC'
                       );

	      $actions = get_comments( $args );

		//$actions = get_comments();

		return $actions;
}

/**
 * Get Comment
 */
function wpb_get_comment() {

			$comment_id = (int)WPB_API_Authenticate::get_arg( 'comment_id' );
			$comment = get_comment( $comment_id );

			if ( ! $comment ) {
			
				$actions = new WP_Error( 'missing-comment', __( "No comment found.", 'wpblazer' ) );

			} else {

				$actions = $comment;

			}

		return $comment;
}

/**
 * Create Comment
 */
function wpb_create_comment() {

			$param_keys = array(
				'comment_post_ID',
				'comment_author',
				'comment_author_email',
				'comment_author_url',
				'comment_date',
				'comment_date_gmt',
				'comment_content',
				'comment_approved',
				'comment_type',
				'comment_parent',
				'user_id'
			);
			$args = array();
			foreach( $param_keys as $param_key ) {
				if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
					$args[$param_key] = $value;
			}

			if ( $comment_id = wp_insert_comment( $args ) )
				$actions = get_comment( $comment_id );
			else
				$actions = new WP_Error( 'create-comment', __( "Error creating comment.", 'wpblazer' ) );

		return $actions;
}

/**
 * Update Comment
 */
function wpb_update_comment() {

			$param_keys = array(
				'comment_author',
				'comment_content',
				'comment_approved'
			);
			$args = array();
			foreach( $param_keys as $param_key ) {
				if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
					$args[$param_key] = $value;
			}

			$args['comment_ID'] = (int)WPB_API_Authenticate::get_arg( 'comment_id' );

			if ( ! get_comment( $args['comment_ID'] ) ) {
				$actions = new WP_Error( 'missing-comment', __( "No comment found.", 'wpblazer' ) );
			}

			if ( wp_update_comment( $args ) )
				$actions = get_comment( $args['comment_ID'] );
			else
				$actions = new WP_Error( 'update-comment', __( $args, 'wpblazer' ) );

		return $actions;
}

/**
 * Delete Comment
 */
function wpb_delete_comment() {

			$comment_id = (int)WPB_API_Authenticate::get_arg( 'comment_id' );
			$comment = get_comment( $comment_id );

			if ( ! $comment ) {
			
				$actions = new WP_Error( 'missing-comment', __( "No comment found.", 'wpblazer' ) );
				
			} else {

				$actions = wp_delete_comment( $comment_id );

			}

		return $actions;
}

/*
 * Get all Users
 */
function wpb_get_users() {    
 
    $users = array_map( 'wpb_user_fields', get_users() ); 

    return $users;
    
}

/*
 * Get Admin Users
 */
function wpb_get_admin_users() {    
 
    $users = get_users( 'role=administrator' ); 

    return $users;
    
}

function wpb_save_admin_user(){
    
    $result = WPB_API_Authenticate::get_arg( 'wpb_user_login' );
    
    return add_option('wpb_admin_user', $result)  or update_option('wpb_admin_user', $result);
}

function wpb_get_admin_user(){
    
    $result = get_option('wpb_admin_user');;
    
    return $result;
}

/*
 * Get User
 */
function wpb_get_user() {    
 
    $user_id = (int)WPB_API_Authenticate::get_arg( 'user_id' );
    $user = get_user_by( 'id', $user_id );
    
    if ( ! $user ) {
        return array( 'status' => 'error', 'error' => 'No user found' );
    }
    
    $result = wpb_user_fields( $user );

    return $result;
    
}

/*
 * Create User
 */
function wpb_create_user() {
    
    $role = WPB_API_Authenticate::get_arg( 'user_role' );
    
    $args = array();
    $args['user_login'] = WPB_API_Authenticate::get_arg( 'user_login' );
    $args['user_email'] = WPB_API_Authenticate::get_arg( 'user_email' );
    $args['user_pass'] = WPB_API_Authenticate::get_arg( 'user_pass' );
    $args['role'] = !empty($role) ? $role : get_option('default_role');
    
    $action = wp_insert_user( $args );
    
    if ( is_wp_error( $action ) )
        $result = array( 'status' => 'error', 'error' => $action->get_error_message() );
    else
        $result = wpb_user_fields( get_user_by( 'id', $action ) );        
    
    return $result;
    
}

/*
 * Update User
 */
function wpb_update_user() {
    
    $role = WPB_API_Authenticate::get_arg( 'user_role' );
    
    $args = array();
    $args['ID'] = (int)WPB_API_Authenticate::get_arg( 'user_id' );
    $args['user_email'] = WPB_API_Authenticate::get_arg( 'user_email' );
    $args['first_name'] = WPB_API_Authenticate::get_arg( 'first_name' );
    $args['last_name'] = WPB_API_Authenticate::get_arg( 'last_name' );
    $args['user_pass'] = WPB_API_Authenticate::get_arg( 'user_pass' );
    $args['user_url'] = WPB_API_Authenticate::get_arg( 'user_url' );
    $args['role'] = !empty($role) ? $role : get_option('default_role');
    
    $action = wp_update_user( $args );
    
    if ( is_wp_error( $action ) )
        $result = array( 'status' => 'error', 'error' => $action );
    else
        $result = wpb_user_fields( get_user_by( 'id', $action ) );        
    
    return $result;
    
}

/*
 * Delete User
 */
function wpb_delete_user() {
    
    require_once ABSPATH . '/wp-admin/includes/user.php';
    
    $user_id = (int)WPB_API_Authenticate::get_arg( 'user_id' );
    
    $result = wp_delete_user( $user_id );
    
    return $result;
    
}

/*
 * Assign and retrieve user fields
 */
function wpb_user_fields ( $fields ) {
	$user_fields = new stdClass;

        foreach( $fields->data as $key => $value ) {
		$user_fields->$key = $value;
	}
	$user_fields->roles = $fields->roles;
	$user_fields->caps = $fields->caps;

	return $user_fields;
}

/*
 * 
 */
function wpb_get_roles() {
    global $wp_roles;

    $all_roles = $wp_roles->roles;
    $editable_roles = apply_filters('editable_roles', $all_roles);

    return $editable_roles;
}

function wpb_content_count(){
    
    global $wp_version;
    global $wpdb;
    
    $file = Back_dir::get_instance()->get_backup();        
    $result = array();
    $result['wpversion'] = $wp_version;
    $result['phpversion'] = phpversion();
    $result['mysqlversion'] = $wpdb->db_version();
    $result['memorylimit'] = ini_get( 'memory_limit' );
    $result['executiontime'] = ini_get( 'max_execution_time' );
    $result['root_dir'] = WPBLAZER_ROOT;
    $result['wpcontent_dir'] = WP_CONTENT_DIR;
    $result['backup_dir'] = get_option('wpb_backup_dir');
    $result['post_count'] = wp_count_posts()->publish;
    $result['page_count'] = wp_count_posts('page')->publish;
    $result['category_count'] = count(get_categories(array('hide_empty'=>0)));
    $result['user_count'] = count_users();
    $result['comment_count'] = wp_count_comments();
    $result['filesize'] = (int) filesize(str_replace(WP_CONTENT_URL, WP_CONTENT_DIR, $file));
    
    return $result;
}

function wpb_bytesToSize($bytes, $precision = 2) {
    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return $bytes . ' B';
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . ' KB';
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . ' MB';
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . ' GB';
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . ' TB';
    } else {
        return $bytes . ' B';
    }
}

function wpb_db_size() {

    global $wpdb;
    $sql = 'SELECT sum(data_length + index_length) FROM information_schema.TABLES WHERE table_schema = "' . DB_NAME . '"';
    $size = $wpdb->get_var( $sql );
    return wpb_bytesToSize( $size );

}

function wpb_dir_size( $path ) {

    $i = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $path ) );
    $size  = 0;
    $files = 0;

    foreach( $i as $file => $info ) {

        if( false === strpos( Back_dir::get_instance()->clear_path( $file ), Back_dir::get_instance()->get_backup_dir() ) ) {
            $size += $info->getSize();
            $files++;

        }

    }

    $ret = array(
        'size'   => wpb_bytesToSize( $size ),
        'files'  => $files,
        'time'   => time()
    );

    return $ret;

}

function wpb_phpinfo(){
    return phpinfo();
}

function wpb_sizeinfo(){
    global $wp_version;
    global $wpdb;
    
    return array(
        'wpversion' => $wp_version,
        'phpversion' => phpversion(),
        'mysqlversion' => $wpdb->db_version(),
        'memorylimit' => ini_get( 'memory_limit' ),
        'executiontime' => ini_get( 'max_execution_time' ),
        'root_dir' => WPBLAZER_ROOT,
        'wpcontent_dir' => WP_CONTENT_DIR,
        'backup_dir' => Back_dir::get_instance()->get_backup_dir(),
        'dbsize' => wpb_db_size(),
        'wpcontent_size'   => wpb_dir_size( WP_CONTENT_DIR ),
        'root_size'  => wpb_dir_size( WPBLAZER_ROOT )
    );
}

function wpb_system_info(){
        
    $result .= function_exists('curl_version') ? ' CURL : Enabled,' : 'CURL : Disabled,';
    
    $result .= function_exists('exec') ? ' EXEC : Enabled,' : ' EXEC : Disabled,';
    
    $result .= function_exists('shell_exec') ? ' SHELL_EXEC : Enabled,' : ' SHELL_EXEC : Disabled,';
    
    $result .= file_get_contents(__FILE__) ? ' File Get Content : Enabled' : ' File Get Content : Disabled';

    $result .= function_exists('system') ? ' System : Enabled' : 'System : Disabled';

    $result .= ' PHP Bin Path : ' . ini_get("extension_dir");

    $result .= ' Server Port : ' . $_SERVER['SERVER_PORT'];

    $result .= ' Disable Functions : ' . ini_get('disable_functions');
    
    return $result;
}

function wpb_xml_sitemap() {
  $postsForSitemap = get_posts(array(
    'numberposts' => -1,
    'orderby' => 'modified',
    'post_type'  => array('post','page'),
    'order'    => 'DESC'
  ));
  $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
  $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
  foreach($postsForSitemap as $post) {
    setup_postdata($post);
    $postdate = explode(" ", $post->post_modified);
    $sitemap .= '<url>'.
      '<loc>'. get_permalink($post->ID) .'</loc>'.
      '<lastmod>'. $postdate[0] .'</lastmod>'.
      '<changefreq>monthly</changefreq>'.
    '</url>';
  }
  $sitemap .= '</urlset>';
  $fp = fopen(ABSPATH . "sitemap.xml", 'w');
  fwrite($fp, $sitemap);
  fclose($fp);
}

function wpb_sitemap() {
    $sitemap = array();

    // GET POST DETAILS 
    $posts = get_posts();
    $i = 0;
    foreach ($posts as $spost) {
        $sitemap['posts'][$i]['post_link'] = $spost->guid;
        $sitemap['posts'][$i]['post_title'] = $spost->post_title;
        $i++;
    }

    // GET CATEGORIES DETAILS
    $args = array(
        'offset' => 0,
        'category' => '',
        'category_name' => '',
        'orderby' => 'date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' => '',
        'post_type' => 'post',
        'post_mime_type' => '',
        'post_parent' => '',
        'author' => '',
        'post_status' => 'publish',
        'suppress_filters' => true
    );
    $cats = get_categories($args);
    $i = 0;
    foreach ($cats as $cat) {
        $sitemap['categories'][$i]['cat_link'] = get_category_link($cat->term_id);
        $sitemap['categories'][$i]['cat_name'] = $cat->cat_name;
        $i++;
    }

    // GET PAGES DETAILS
    $pages_args = array(
        'exclude' => '',
        'post_type' => 'page',
        'post_status' => 'publish'
    );
    $pages = get_pages($pages_args);
    $i = 0;
    foreach ($pages as $page) {
        $sitemap['pages'][$i]['page_link'] = get_page_link($page->ID);
        $sitemap['pages'][$i]['page_title'] = $page->post_title;
        $i++;
    }

    // GET TAGS DETAILS
    $tags = get_tags();
    $i = 0;
    foreach ($tags as $tag) {
        $sitemap['tags'][$i]['tag_link'] = get_tag_link($tag->term_id);
        $sitemap['tags'][$i]['tag_name'] = $tag->name;
        $i++;
    }

    return $sitemap;
}

function wpb_generate_robots_txt(){
    
    $robot_content = WPB_API_Authenticate::get_arg( 'robot_content' );
    if(!empty($robot_content)){
        $fp = fopen(ABSPATH . "robots.txt", 'w');
        fwrite($fp, $robot_content);
        fclose($fp);
        $status = 'success';        
    } else {
        $status = 'failed';        
    }
    
    return $status;
}

function wpb_set_single_post(){
    $id = WPB_API_Authenticate::get_arg( 'id' );
    $url = get_permalink( $id );
    $facebook_count = get_fb_counts($url); 
    $google_count = get_google_count( $url );
    $stumble_count = get_stumbleupon_count( $url );
    $pinterest_count = get_pinterest_count( $url );
    add_post_meta( $id, "WPB_FACEBOOK_COUNT", $facebook_count, true ) or update_post_meta( $id, "WPB_FACEBOOK_COUNT", $facebook_count);
    add_post_meta( $id, "WPB_GOOGLE_COUNT", $google_count, true ) or update_post_meta( $id, "WPB_GOOGLE_COUNT", $google_count);
    add_post_meta( $id, "WPB_STUMBLE_COUNT", $stumble_count, true ) or update_post_meta( $id, "WPB_STUMBLE_COUNT", $stumble_count);
    add_post_meta( $id, "WPB_PINTEREST_COUNT", $pinterest_count, true ) or update_post_meta( $id, "WPB_PINTEREST_COUNT", $pinterest_count);
    
    return 'success';
}

function wpb_set_all_posts(){
    
    $actions = array();
    $all_data = array();
    $i = 1;
    
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1
        );
    $query = new WP_Query( $args );
    
    if( !empty( $query->posts ) ) {
        foreach( $query->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $list_posts[] = array(
                'ID' => $posts->ID,
                'post_title' => $posts->post_title,
                'post_status' => $posts->post_status,
                'post_name' => $posts->post_name,
                'post_modified' => $posts->post_modified,
                'guid' => $posts->guid,
                'post_type' => $posts->post_type
                    );
        }
        $post = $list_posts;
    } else {
        $post = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
    }
    
    foreach ($post as $value){
        $id = $value['ID'];
        $url = get_permalink( $id );
        $facebook_count = get_fb_counts($url); 
        $google_count = get_google_count( $url );
        $stumble_count = get_stumbleupon_count( $url );
        $pinterest_count = get_pinterest_count( $url );
        add_post_meta( $id, "WPB_FACEBOOK_COUNT", $facebook_count, true ) or update_post_meta( $id, "WPB_FACEBOOK_COUNT", $facebook_count);
        add_post_meta( $id, "WPB_GOOGLE_COUNT", $google_count, true ) or update_post_meta( $id, "WPB_GOOGLE_COUNT", $google_count);
        add_post_meta( $id, "WPB_STUMBLE_COUNT", $stumble_count, true ) or update_post_meta( $id, "WPB_STUMBLE_COUNT", $stumble_count);
        add_post_meta( $id, "WPB_PINTEREST_COUNT", $pinterest_count, true ) or update_post_meta( $id, "WPB_PINTEREST_COUNT", $pinterest_count);
        
        $actions[$i]['id'] = $id;
        $actions[$i]['url'] = $url;
        $actions[$i]['fb'] = $facebook_count; 
        $actions[$i]['google'] = $google_count; 
        $actions[$i]['stumble'] = $stumble_count; 
        $actions[$i]['pinterest'] = $pinterest_count; 
        $i=$i+1;
    }
    
    $args1 = array(
        'post_type' => 'page',
        'posts_per_page' => -1
        );
    $query1 = new WP_Query( $args1 );
    
    if( !empty( $query1->posts ) ) {
        foreach( $query1->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $list_pages[] = array(
                'ID' => $posts->ID,
                'post_title' => $posts->post_title,
                'post_status' => $posts->post_status,
                'post_name' => $posts->post_name,
                'post_modified' => $posts->post_modified,
                'guid' => $posts->guid,
                'post_type' => $posts->post_type
                    );
        }
        $pages = $list_pages;
    } else {
        $pages = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
    }
    
    foreach ($pages as $value){  
        $id = $value['ID'];
        $url = get_permalink( $id );
        $facebook_count = get_fb_counts($url); 
        $google_count = get_google_count( $url );
        $stumble_count = get_stumbleupon_count( $url );
        $pinterest_count = get_pinterest_count( $url );
        add_post_meta( $id, "WPB_FACEBOOK_COUNT", $facebook_count, true ) or update_post_meta( $id, "WPB_FACEBOOK_COUNT", $facebook_count);
        add_post_meta( $id, "WPB_GOOGLE_COUNT", $google_count, true ) or update_post_meta( $id, "WPB_GOOGLE_COUNT", $google_count);
        add_post_meta( $id, "WPB_STUMBLE_COUNT", $stumble_count, true ) or update_post_meta( $id, "WPB_STUMBLE_COUNT", $stumble_count);
        add_post_meta( $id, "WPB_PINTEREST_COUNT", $pinterest_count, true ) or update_post_meta( $id, "WPB_PINTEREST_COUNT", $pinterest_count);
        
        $actions[$i]['id'] = $id;
        $actions[$i]['url'] = $url;
        $actions[$i]['fb'] = $facebook_count; 
        $actions[$i]['google'] = $google_count; 
        $actions[$i]['stumble'] = $stumble_count; 
        $actions[$i]['pinterest'] = $pinterest_count; 
        $i=$i+1;
    }
    
    return $actions;
}

function wpb_get_all_social_count(){
    
    global $wpdb;
    $a = $wpdb->get_results( "SELECT s.post_id, s.meta_key as fb_key, s.meta_value as fb_value, p.ID, p.guid, p.post_title, 'facebook' as source FROM ".$wpdb->prefix."posts p LEFT JOIN ".$wpdb->prefix."postmeta s on s.post_id = p.ID WHERE s.meta_key LIKE 'WPB_FACEBOOK_COUNT';");
    $b = $wpdb->get_results( "SELECT s.post_id, s.meta_key as goo_key, s.meta_value as goo_value, p.ID, p.guid, p.post_title, 'google' as source FROM ".$wpdb->prefix."posts p LEFT JOIN ".$wpdb->prefix."postmeta s on s.post_id = p.ID WHERE s.meta_key LIKE 'WPB_GOOGLE_COUNT';");
    $c = $wpdb->get_results( "SELECT s.post_id, s.meta_key as stum_key, s.meta_value as stum_value, p.ID, p.guid, p.post_title, 'stumble' as source FROM ".$wpdb->prefix."posts p LEFT JOIN ".$wpdb->prefix."postmeta s on s.post_id = p.ID WHERE s.meta_key LIKE 'WPB_STUMBLE_COUNT';");
    $d = $wpdb->get_results( "SELECT s.post_id, s.meta_key as pin_key, s.meta_value as pin_value, p.ID, p.guid, p.post_title, 'pinterest' as source FROM ".$wpdb->prefix."posts p LEFT JOIN ".$wpdb->prefix."postmeta s on s.post_id = p.ID WHERE s.meta_key LIKE 'WPB_PINTEREST_COUNT';");
    $a = json_decode(json_encode($a), true);
    $b = json_decode(json_encode($b), true);
    $c = json_decode(json_encode($c), true);
    $d = json_decode(json_encode($d), true);
    $result = array_replace_recursive($a, $b, $c, $d);
    return $result;
}

function wpb_get_all_posts(){
    
    $actions = array();
    $i = 1;
    
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1
        );
    $query = new WP_Query( $args );
    
    if( !empty( $query->posts ) ) {
        foreach( $query->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $list_posts[] = array(
                'ID' => $posts->ID,
                'post_title' => $posts->post_title,
                'post_status' => $posts->post_status,
                'post_name' => $posts->post_name,
                'post_modified' => $posts->post_modified,
                'guid' => $posts->guid,
                'post_type' => $posts->post_type
                    );
        }
        $post = $list_posts;
    } else {
        $post = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
    }
    
    foreach ($post as $value){
        $id = $value['ID'];
        $url = get_permalink( $id );        
        $actions[$i]['id'] = $id;
        $actions[$i]['url'] = $url;
        $actions[$i]['fb'] = get_post_meta( $id, "WPB_FACEBOOK_COUNT", true);
        $actions[$i]['google'] = get_post_meta( $id, "WPB_GOOGLE_COUNT", true);
        $actions[$i]['stumble'] = get_post_meta( $id, "WPB_STUMBLE_COUNT", true); 
        $actions[$i]['pinterest'] = get_post_meta( $id, "WPB_PINTEREST_COUNT", true); 
        $i=$i+1;
    }
    
    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1
        );
    $query = new WP_Query( $args );
    
    if( !empty( $query->posts ) ) {
        foreach( $query->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $list_pages[] = array(
                'ID' => $posts->ID,
                'post_title' => $posts->post_title,
                'post_status' => $posts->post_status,
                'post_name' => $posts->post_name,
                'post_modified' => $posts->post_modified,
                'guid' => $posts->guid,
                'post_type' => $posts->post_type
                    );
        }
        $pages = $list_pages;
    } else {
        $pages = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
    }
    
    foreach ($pages as $value){        
        $id = $value['ID'];
        $url = get_permalink( $id );        
        $actions[$i]['id'] = $id;
        $actions[$i]['url'] = $url;
        $actions[$i]['fb'] = get_post_meta( $id, "WPB_FACEBOOK_COUNT", true);
        $actions[$i]['google'] = get_post_meta( $id, "WPB_GOOGLE_COUNT", true);
        $actions[$i]['stumble'] = get_post_meta( $id, "WPB_STUMBLE_COUNT", true); 
        $actions[$i]['pinterest'] = get_post_meta( $id, "WPB_PINTEREST_COUNT", true); 
        $i=$i+1;
    }
    
    return $actions;
}

function get_fb_counts($url){
    
    //$query = "select total_count,like_count,comment_count,share_count,click_count from link_stat where url='{$url}'";
    //$call = "https://api.facebook.com/method/fql.query?query=" . rawurlencode($query) . "&format=json";
    
    $call = "https://graph.facebook.com/v2.8/?fields=og_object{likes.summary(true).limit(0)},share&id=".$url."&access_token=384450621898954|zdlSAJxqFcisTVd56vpi3SSrNII";
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $call);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3000);
    $output = curl_exec($ch);
    
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    
    if ($errmsg != '' || $err != '') {
        return 0;
    }
    
    if( empty( $output ) ){
        return 0;
    }
    curl_close($ch);
    
    $output = json_decode($output,true);
    if(isset($output['share'])){
	$output = $output['share']['share_count'];
	return $output;
    } else {
	return 0;
    }
}

function get_google_count($url){
    
    $call = "https://plusone.google.com/u/0/_/+1/fastbutton?url=".urlencode($url)."&count=true";
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $call);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    
    if ($errmsg != '' || $err != '') {
        return 0;
    }
    
    $dom = new DOMDocument;
    $dom->preserveWhiteSpace = false;
    @$dom->loadHTML($output);
    $domxpath = new DOMXPath($dom);
    $newDom = new DOMDocument;
    $newDom->formatOutput = true;
    
    $filtered = $domxpath->query("//div[@id='aggregateCount']");
    
    if( count( $filtered ) == 0 ){
        return 0;
    }
    
    return (int)$filtered->item(0)->nodeValue;
}

function get_stumbleupon_count($url) {
    
    $call = "http://www.stumbleupon.com/services/1.01/badge.getinfo?url=".$url;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $call);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    
    if ($errmsg != '' || $err != '') {
        return 0;
    }
        
    $response = json_decode( $output, true );
    
    if( !isset( $response['result'] ) || !isset( $response['result']['views']) ){
        return 0;
    }
    
    return (int)$response['result']['views'];
}

function get_pinterest_count($url) {
    $json_string = file_get_contents( 'http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url='.$url);
    $raw_json = preg_replace('/^receiveCount\((.*)\)$/', "\\1", $json_string);
    $json = json_decode($raw_json, true);
    return isset($json['count'])?intval($json['count']):0;
}

add_action('wpb-update-stats','wpb_cron_update_stat');

function wpb_cron_update_stat($run=true){
	
        $mytime = time();
        $mytime += 24*3600;
        
        $status = WPB_API_Authenticate::get_arg( 'run' );
        if($status == 'false'){
            $run = false;
        }
        
	if (!wp_next_scheduled('wpb-update-stats')){
            wp_schedule_single_event($mytime, 'wpb-update-stats'); 
            wpb_set_all_posts();
        }

	if ($run === false)
            return;

	wpb_set_all_posts();
}

function wpb_prepare_data(){
    
    $all_data = array();
    
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1
        );
    $query = new WP_Query( $args );
    
    if( !empty( $query->posts ) ) {
        foreach( $query->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $all_data[] = $posts->ID;
        }
    }
        
    $args1 = array(
        'post_type' => 'page',
        'posts_per_page' => -1
        );
    $query1 = new WP_Query( $args1 );
    
    if( !empty( $query1->posts ) ) {
        foreach( $query1->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $all_data[] = $posts->ID;
        }
    }
    
    return json_encode($all_data);
}
    
function wpb_add_social_networks(){
    
    $setting = WPB_API_Authenticate::get_arg( 'setting' );
    
    $fburl = WPB_API_Authenticate::get_arg( 'fburl' );
    $fbappid = WPB_API_Authenticate::get_arg( 'fbappid' );
    $fbappsec = WPB_API_Authenticate::get_arg( 'fbappsec' );
    $fbmsgformat = WPB_API_Authenticate::get_arg( 'fbmsgformat' );
    
    $twurl = WPB_API_Authenticate::get_arg( 'twurl' );
    $twappkey = WPB_API_Authenticate::get_arg( 'twappkey' );
    $twappsec = WPB_API_Authenticate::get_arg( 'twappsec' );
    $twtoken = WPB_API_Authenticate::get_arg( 'twtoken' );
    $twtokensec = WPB_API_Authenticate::get_arg( 'twtokensec' );
    $twmsgformat = WPB_API_Authenticate::get_arg( 'twmsgformat' );
    
    if($setting == 'twitter'){
        $get_social = get_option('wpb_social_networks');
        
        $social = array(
            'tw' => array(
                'twurl' => $twurl,
                'twappkey' => $twappkey,
                'twappsec' => $twappsec,
                'twtoken' => $twtoken,
                'twtokensec' => $twtokensec,
                'twmsgformat' => $twmsgformat,
            ),
        );
        
        $result = array_merge((array) $get_social,(array) $social);
        
        return add_option('wpb_social_networks', $result)  or update_option('wpb_social_networks', $result);
    }
    
    if($setting == 'facebook'){
    $fbPgID = $fburl; 
    if (substr($fbPgID, -1)=='/') $fbPgID = substr($fbPgID, 0, -1);  $fbPgID = substr(strrchr($fbPgID, "/"), 1); 
    if (strpos($fbPgID, '?')!==false) $fbPgID = substr($fbPgID, 0, strpos($fbPgID, '?'));  
    if (strpos($fbPgID, '-')!==false) { $posID = substr(strrchr($fbPgID, "-"), 1);
    $fbPgID = (strlen($posID)>10 && is_numeric($posID))?$posID:$fbPgID; } 
    
    $fbcode = WPB_API_Authenticate::get_arg( 'fbaccess' );
    
    if ($fbcode != ''){
        $get_social = get_option('wpb_social_networks');
        
        $fbappsec = $get_social['fb']['fbappsec'];
        $page_id = $get_social['fb']['fbPageId'];
        $appsecret_proof = hash_hmac('sha256', $fbcode, $fbappsec); 
        $query = array('access_token'=>$fbcode, 'appsecret_proof'=>$appsecret_proof, 'method'=>'get'); 
        
        $res = wp_remote_get( "https://graph.facebook.com/v2.3/$page_id?fields=access_token&".http_build_query($query, null, '&')); 
        $token = json_decode($res['body'], true);
        $pageToken = '';
        if (!empty($token)) {
            if (!empty($token['access_token'])) {
                $pageToken = $token['access_token'];
            }
        } 
        
        $social = array(
            'fb' => array(
                'fburl' => $get_social['fb']['fburl'],
                'fbappid' => $get_social['fb']['fbappid'],
                'fbappsec' => $fbappsec,
                'fbmsgformat' => $get_social['fb']['fbmsgformat'],
                'fbPageId' => $get_social['fb']['fbPageId'],
                'fbAuthToken' => $fbcode,
                'fbPageAuthToken' => $pageToken,
            ),
        );
    } else {
        $get_social = get_option('wpb_social_networks');
        $social = array(
            'fb' => array(
                'fburl' => $fburl,
                'fbappid' => $fbappid,
                'fbappsec' => $fbappsec,
                'fbmsgformat' => $fbmsgformat,
                'fbPageId' => $fbPgID,
                'fbAuthToken' => '',
                'fbPageAuthToken' => '',
            ),
        );
    }
    $result = array_merge((array) $get_social,(array) $social);
    return add_option('wpb_social_networks', $result)  or update_option('wpb_social_networks', $result);
    }
    return FALSE;
}

function wpb_get_social_networks(){
    $fb = WPB_API_Authenticate::get_arg( 'fb' );
    $gplus = WPB_API_Authenticate::get_arg( 'gplus' );
    $pinterest = WPB_API_Authenticate::get_arg( 'pinterest' );
    
    $social = get_option('wpb_social_networks');
    
    if(!empty($fb)){
        $result = $social['fb'];
    }
    
    return $social;
}

function wpb_ajax_repost($postID){
    
    $id = WPB_API_Authenticate::get_arg( 'pid' );
    $dest = WPB_API_Authenticate::get_arg( 'dest' );
    
    if(!empty($id)){
        if(!empty($dest)){
            $result = wpb_ajax_post($id,$dest);
        }
    }
    
    return $result;
    
}

function wpb_ajax_post($postID,$dest=false){
    
    if($dest==false){
        wpb_facebook($postID);
        wpb_twitter($postID);
        return 'success';
    } else {
        if($dest == 'fb'){
            return wpb_facebook($postID);
        }
        if($dest == 'tw'){
            return wpb_twitter($postID);
        }
    }
    
    
}

function wpb_facebook($postID){
    $get_social = get_option('wpb_social_networks');
    
    $fbAppSec = $get_social['fb']['fbappsec'];
    $fbPageAuthToken = $get_social['fb']['fbPageAuthToken'];
    $msgformat = $get_social['fb']['fbmsgformat'];
    $page_id = $get_social['fb']['fbPageId'];
    
    if(empty($fbAppSec)){
        return 'Empty Secret key';
    }
    
    if(empty($fbPageAuthToken)){
        return 'Authentication required';
    }    
        
    $appsecret_proof = hash_hmac('sha256', $fbPageAuthToken, $fbAppSec); 
    $desc=='';
    if ($postID=='0') { 
        $msgformat = 'Test Post, Please Ignore';
        $desc = 'Test Post, Description';
        $urlTitle = 'Test Post - Title';
        $urlToGo = home_url();    
    } else { 
        
        $urlToGo = get_permalink($postID);
        
        $post = get_post($postID);
        
        $urlTitle = $post->post_title;
        if ($desc=='') 
            $desc = trim($post->post_excerpt);
        if ($desc=='')
            $desc = trim($post->post_content);
        
        $imgURL == '';        
        if (function_exists("get_post_thumbnail_id") && function_exists('has_post_thumbnail') && has_post_thumbnail($postID) ){ 
            $imgURL = wp_get_attachment_image_src(get_post_thumbnail_id($postID));
            $imgURL = $imgURL[0];
            if ((trim($imgURL)!='')  && substr($imgURL, 0, 4)!='http') 
                    $imgURL = site_url($imgURL);
        } 
        
        if($imgURL == ''){
            $postImgs = array();
            $desc = str_replace("'",'"',$desc);
            $result = preg_match_all( '/< *img[^>]*src *= *["\']?([^"\']*)/i', $desc, $images );
            foreach ($images[1] as $image) {
                if (!preg_match('/^https?:\/\//', $image ) )
                        $image = site_url( '/' ) . ltrim( $image, '/' );
                $postImgs[] = $image;
            } 
            if (is_array($postImgs) && count($postImgs)>0)
                $imgURL = $postImgs[0];  
        }
        
        $desc = trim(apply_filters( 'the_content', $desc ));
        $desc = strip_tags($desc);
    
    }
    
    if (preg_match('/%TITLE%/', $msgformat)) { $heading = str_ireplace("%TITLE%", $urlTitle, $msgformat); } 

    $blogTitle = htmlspecialchars_decode(get_bloginfo('name'), ENT_QUOTES);
    
    if (preg_match('/%SITENAME%/', $heading)) { $heading = str_ireplace("%SITENAME%", $blogTitle, $heading);}
        
    $mssg = array('access_token'=>$fbPageAuthToken, 'appsecret_proof'=>$appsecret_proof, 'method'=>'post', 'message'=>$heading);
    
    $msg = array('name' => $urlTitle, 'caption' => $blogTitle, 'link' =>$urlToGo, 'description' => $desc);
    if (trim($imgURL)!='')
        $msg['picture'] = $imgURL; 
    
    $body = array_merge($mssg, $msg);

    $fbWhere = 'feed'; 
    $destURL = "https://graph.facebook.com/$page_id/".$fbWhere;    
    $response = wp_remote_post( $destURL, array( 'method' => 'POST', 'httpversion' => '1.1', 'timeout' => 30, 'sslverify'=>false, 'redirection' => 0, 'body' => $body)); 
    if (is_wp_error($response) || empty($response['body'])) return "ERROR: ".print_r($response, true);
    $res = json_decode($response['body'], true);    
    if (empty($res)) 
        $error = "JSON ERROR: ".print_r($response, true);
    if (!empty($res['error'])) 
        if (!empty($res['error']['message'])) {
            $error .= $res['error']['message'];
        } 
        else 
            $error = print_r($res['error'], true);
        
        if(!empty($error)){
            wpb_addToLog($post->ID, $post->post_type, 'fb', 'Error', $error); 
            return $error;
        } else {
            wpb_addToLog($post->ID, $post->post_type, 'fb', 'Posted', 'Message Posted'); 
        }
        
    return 'success';
}

function wpb_twitter($postID){
    
    $get_social = get_option('wpb_social_networks');
    
    $twappkey = $get_social['tw']['twappkey'];
    $twappsec = $get_social['tw']['twappsec'];
    $twtoken = $get_social['tw']['twtoken'];
    $twtokensec = $get_social['tw']['twtokensec'];
    $twmsgformat = $get_social['tw']['twmsgformat'];
    
    if(empty($twappsec)){
        return 'Empty Secret key';
    }
        
    $urlToGo = get_permalink($postID);
        
        $post = get_post($postID);
        
        $urlTitle = $post->post_title;
        if ($desc=='') 
            $desc = trim($post->post_excerpt);
        if ($desc=='')
            $desc = trim($post->post_content);
        
        $imgURL == '';        
        if (function_exists("get_post_thumbnail_id") && function_exists('has_post_thumbnail') && has_post_thumbnail($postID) ){ 
            $imgURL = wp_get_attachment_image_src(get_post_thumbnail_id($postID));
            $imgURL = $imgURL[0];
            if ((trim($imgURL)!='')  && substr($imgURL, 0, 4)!='http') 
                    $imgURL = site_url($imgURL);
        } 
        
        if($imgURL == ''){
            $postImgs = array();
            $desc = str_replace("'",'"',$desc);
            $result = preg_match_all( '/< *img[^>]*src *= *["\']?([^"\']*)/i', $desc, $images );
            foreach ($images[1] as $image) {
                if (!preg_match('/^https?:\/\//', $image ) )
                        $image = site_url( '/' ) . ltrim( $image, '/' );
                $postImgs[] = $image;
            } 
            if (is_array($postImgs) && count($postImgs)>0)
                $imgURL = $postImgs[0];  
        }
        
        $desc = trim(apply_filters( 'the_content', $desc ));
        $desc = strip_tags($desc);
        
        $imgData = '';
        if ($imgURL!='') {
        $imgData = wp_remote_get($imgURL);
        if(is_wp_error($imgData) || empty($imgData['body']) || (!empty($imgData['headers']['content-length']) && (int)$imgData['headers']['content-length']<200) || 
          $imgData['headers']['content-type'] == 'text/html' ||  $imgData['response']['code'] == '403' ) { 
            wpb_addToLog($post->ID, $post->post_type, 'tw', 'Error', 'Could not get image ('.$imgURL.'), will post without it - ', print_r($imgData, true));
            return 'Could not get image ('.$imgURL.')';
        } else $imgData = $imgData['body']; 
        }
        
        if (preg_match('/%TITLE%/', $twmsgformat)) { $heading = str_ireplace("%TITLE%", $urlTitle, $twmsgformat); }
        
        if (preg_match('/%URL%/', $heading)) { $heading = str_ireplace("%URL%", $urlToGo, $heading);}
        
        require_once WPBLAZER_PLUGIN_URL . 'apis/tmhOAuth.php';  
        
        $tmhOAuth = new tmhOAuth(
                array( 'consumer_key' => $twappkey,
                    'consumer_secret' => $twappsec,
                    'user_token' => $twtoken,
                    'user_secret' => $twtokensec));      
        if ($imgURL!='') 
            $params_array =array( 'media[]' => $imgData, 'status' => $heading); else $params_array = array('status' =>$heading);
            
        if ($imgURL!='') 
            $code = $tmhOAuth -> request('POST', 'https://api.twitter.com/1.1/statuses/update_with_media.json', $params_array, true, true);
        else 
            $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), $params_array);
        
        $error = '';
        if ( $code=='403' && stripos($tmhOAuth->response['response'], 'User is over daily photo limit')!==false && $imgURL!='') { 
            $error .= "User is over daily photo limit. Will post without image\r\n"; 
            $code = $tmhOAuth->request('POST', $tmhOAuth->url('1.1/statuses/update'), array('status' =>$heading));
        }    
        
        if ($code == 200){
            wpb_addToLog($post->ID, $post->post_type, 'tw', 'Posted', 'Message Posted');             
        } else { 
            $error .= print_r($tmhOAuth->response['error'], true); 
        }
        
        if(!empty($error)){
            wpb_addToLog($post->ID, $post->post_type, 'tw', 'Error', $error); 
            return $error;
        }
        
        return 'success';
}

function wpb_addToLog($pid, $type, $destination, $status, $msg = NULL){ 
    
    global $wpdb;
    
    $table_name = $wpdb->prefix . "wpb_log";
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            id bigint(20) NOT NULL AUTO_INCREMENT,
            date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            pid bigint(20) NOT NULL,
            type VARCHAR(50) DEFAULT '' NOT NULL,
            destination VARCHAR(50) DEFAULT '' NOT NULL,
            post_status VARCHAR(255) DEFAULT '' NOT NULL,
            msg text NOT NULL,     
            UNIQUE KEY id (id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php'); dbDelta($sql);
    }
    
    $numLogRows = 150;
    
    $logItem = array('date'=>date_i18n('Y-m-d H:i:s'), 'pid'=>$pid, 'type'=>$type, 'destination'=>$destination,  'post_status'=>$status, 'msg'=> strip_tags($msg)); 
    $nxDB = $wpdb->insert( $wpdb->prefix . "wpb_log", $logItem );
    $lid = $wpdb->insert_id; 
    $lid = $lid-$numLogRows;
    //if ($lid>0) $wpdb->query( 'DELETE FROM '.$wpdb->prefix . 'wpb_log WHERE id<'.$lid ); 
    return 'success';
}

function wpb_get_posts_only(){
    
    global $wpdb;
    $actions = array();
    $i = 1;
    
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1
        );
    $query = new WP_Query( $args );
    
    if( !empty( $query->posts ) ) {
        foreach( $query->posts as $posts ) {
            $user = get_user_by( 'id', $posts->post_author );
            $list_posts[] = array(
                'ID' => $posts->ID,
                'post_title' => $posts->post_title,
                'post_status' => $posts->post_status,
                'post_name' => $posts->post_name,
                'post_modified' => $posts->post_modified,
                'guid' => $posts->guid,
                'post_type' => $posts->post_type
                    );
        }
        $post = $list_posts;
    } else {
        $post = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
    }
    $log = $wpdb->get_results( "SELECT id,MAX(date) as date,pid,type,GROUP_CONCAT(destination),MAX(post_status)as status,MAX(msg) as msg FROM ". $wpdb->prefix . "wpb_log GROUP by pid ORDER BY date DESC", ARRAY_A );

    foreach ($post as $value){
        $id = $value['ID'];
        $url = get_permalink( $id );        
        $actions[$i]['id'] = $id;
        $actions[$i]['url'] = $url;
        $status = '';
        $msg = '';
        foreach ($log as $logs) {
            if($id == $logs['pid']){
                $status = $logs['status'];
                $destination = $logs['destination'];
                $destination = explode(",", $destination);
                $msg = $logs['msg'];
            }        
        }
        $actions[$i]['status'] = $status;
        $actions[$i]['destination'] = $destination;
        $actions[$i]['msg'] = $msg; 
        $i=$i+1;
    }
    
    
    return $actions;
}

/**
 *
 */
?>