<?php
class WPB_API_Authenticate {

	static $actions = array();
	static $args = array();

	static function wpb_validate_request() {

		// Check the API Key
		if ( ! wpbr_get_api_keys() ) {

			echo json_encode( 'blank-api-key' );
			exit;

		} elseif ( isset( $_POST['wpb_verify_key'] ) ) {

			$verify = $_POST['wpb_verify_key'];
			unset( $_POST['wpb_verify_key'] );

			$hash = self::generate_hashes( $_POST );

			if ( ! in_array( $verify, $hash, true ) ) {
				echo json_encode( 'bad-verify-key' );
				exit;
			}

			if ( (int) $_POST['timestamp'] > time() + 360 || (int) $_POST['timestamp'] < time() - 360 ) {
				echo json_encode( 'bad-timestamp' );
				exit;	
			}
                        
                        $timestamp = (int) $_POST['timestamp'];
                        if ($timestamp) {
                            add_option('wpb_timestamp', $timestamp) or update_option('wpb_timestamp', $timestamp);
                        }
                        
			self::$actions = $_POST['actions'];
			self::$args = $_POST;


		} else {
			exit;
		}

		return true;

	}

	static function generate_hashes( $vars ) {

		$api_key = wpbr_get_api_keys();
		if ( ! $api_key )
			return array();

		$hashes = array();
		foreach( $api_key as $key ) {
			$hashes[] = hash_hmac( 'sha256', serialize( $vars ), $key );			
		}
		return $hashes;

	}

	static function get_actions() {
		return self::$actions;
	}

	static function get_args() {
		return self::$args;
	}

	static function get_arg( $arg ) {
		return ( isset( self::$args[$arg] ) ) ? self::$args[$arg] : '';
	}
}

WPB_API_Authenticate::wpb_validate_request();
?>