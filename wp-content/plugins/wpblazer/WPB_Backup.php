<?php

Class WPB_Backup {

    // variable declaration
    private $sqlfilename;
    private $sqlfile;
    private $backfiles;
    private $backup_type = '';
    private $home_path;
    private $db_connect;
    private $excludes = array();
    private $dump_start;
    private $host = '';
    private $socket = '';
    private $port = 0;
    private $sql_tables = array();
    private $zip = NULL;

    function __construct() {

        //@ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
        //@set_time_limit( 0 );
    }

    /*
     * Get BackUp Type
     */

    public function backup_type() {

        if (empty($this->backup_type))
            $this->set_backup_type('complete');

        return $this->backup_type;
    }

    /*
     * Set BackUp Type
     */

    public function set_backup_type($backup_type) {

        if (!is_string($backup_type) || !in_array($backup_type, array('file', 'database', 'complete')))
            return array('status' => 'error', 'error' => 'Invalid backup type %s must be one of (string) file, database or complete');

        $this->backup_type = $backup_type;
    }

    /*
     * Get BackUp directory
     */

    public function get_backup_dir() {

        if (empty($this->dir))
            $this->set_backup_dir(self::clear_path());

        return $this->dir;
    }

    /*
     * Get BackUp directory
     */

    public function set_backup_dir($dir) {

        if (empty($dir) || !is_string($dir))
            return array('status' => 'error', 'error' => 'Invalid backup directory');

        $this->dir = self::clear_path($dir);
    }

    public static function clear_path($dir) {

        // Assume empty dir is root
        if (!$dir)
            $dir = '/';

        $dir = untrailingslashit(str_replace('\\', '/', str_replace('//', '/', trim($dir))));

        return (string) $dir;
    }

    /*
     * Run backup
     */

    public function runbackup() {

        $this->dump_start = microtime(TRUE);
        $backuptype = $this->backup_type();

        switch ($backuptype) {

            case 'database' : // calling db backup 

                return $this->backupdb();

                break;

            case 'file': // calling File backup 

                return $this->backupfiles();

                $this->do_action('backup_complete');

                return $this->backfiles;

                break;

            case 'complete': // calling complete backup 

                return $this->backupdb();

                break;
        }
    }

    /*
     * Backup Database
     */

    public function backupdb() {
        
        $restart = get_option('wpb_zip_restart');
        
        if ($restart == '1') {
            return $this->backupfiles();
        }
        
        $sql_method = WPB_API_Authenticate::get_arg('sqlmethod');
        
        if(!empty($sql_method) && $sql_method == 'mysqli'){
            return $this->mysqlidump_back();
        } else if(!empty($sql_method) && $sql_method == 'mysql'){
            return $this->mysqldump_back();
        }

        if (!class_exists('mysqli')) {
            return $this->mysqldump_back();
        } else {
            return $this->mysqlidump_back();
        }

        //Sql Dumb
        return $this->mysqldump_back();

        //File Backup
        $this->backupfiles();

        // Delete the database dump
        if (file_exists($this->sqlfile))
            unlink($this->sqlfile);

        $this->do_action('backup_complete');

        return $this->backfiles;
    }

    /**
     * Better addslashes for SQL queries.
     * Taken from phpMyAdmin.
     */
    private function sql_addslashes($a_string = '', $is_like = false) {

        if ($is_like)
            $a_string = str_replace('\\', '\\\\\\\\', $a_string);
        else
            $a_string = str_replace('\\', '\\\\', $a_string);

        return str_replace('\'', '\\\'', $a_string);
    }

    /**
     * Add backquotes to tables and db-names in
     * SQL queries. Taken from phpMyAdmin.
     */
    private function backquote($a_name) {

        if (!empty($a_name) && $a_name != '*') {

            if (is_array($a_name)) {

                $result = array();

                reset($a_name);

                while (list($key, $val) = each($a_name))
                    $result[$key] = '`' . $val . '`';

                return $result;
            } else {

                return '`' . $a_name . '`';
            }
        } else {

            return $a_name;
        }
    }

    /*
     * Backup Files
     */

    public function backupfiles($files = null) {

        if ($this->wpb_is_shellinstalled()) {

            $zip_service = 'zip';
        } elseif (class_exists("ZipArchive")) {

            $zip_service = 'ziparchive';
        } else {

            $zip_service = 'pclzip';
        }

        $zip_method = WPB_API_Authenticate::get_arg('zipmethod');
        $zip_service = !empty($zip_method) ? $zip_method : $zip_service;
        
        $restart = get_option('wpb_zip_restart');
        $backuptype = $this->backup_type();
        
        if (empty($restart)) {
            $filename = implode('-', array(sanitize_title(str_ireplace(array('http://', 'https://', 'www'), '', home_url())), $backuptype, ' backup', date('Y-m-d-H-i-s'))) . '.zip';
            $back_filename = sanitize_file_name($filename);
            add_option('wpb_backname', $back_filename) or update_option('wpb_backname', $back_filename);
            $backup_file = trailingslashit($this->get_backup_dir()) . $back_filename;
        } else {
            $backup_file = trailingslashit($this->get_backup_dir()) . get_option( 'wpb_backname' );
        }
        
        delete_option('wpb_dumbed_table');
        delete_option('wpb_dumb_start');
        delete_option('wpb_dumb_restart');

        switch ($zip_service) {

            case 'zip':
                $this->zip_backup($backup_file);
                return true;
                break;

            case 'ziparchive':
                $result = $this->zip_archeive_backup($backup_file);
                return $result;                  
                break;

            case 'pclzip':
                $this->pclzip_backup($backup_file);
                break;

            default :
                reporterror('no zip class found');
                break;
        }
    }

    public function get_wp_files_excludes($dir, $files = NULL) {

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        $handle = scandir($dir);

        foreach ($handle as $result) {
            if ($result === '.' or $result === '..')
                continue;

            if (is_dir($home_path . '/' . $result)) {
                if ($result != 'wp-admin' && $result != 'wp-content' && $result != 'wp-includes') {
                    $files .= " '" . $result . "*'";
                }
            }
        }

        return $files;
    }

    public function get_wp_files_exclude() {

        $pieces = WPB_API_Authenticate::get_arg('backup_excludes');

        $rule = '';

        foreach ($pieces as $key => &$value) {
            $value = "'" . $value . "*'";
        }

        return implode(' ', $pieces);
    }

    /*
     * Create a zip file
     */

    public function zip_backup($backup_file) {

        $this->do_action('archive_started');

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        $path = $this->get_backup_dir();
        $excludes = "'" . str_replace($home_path, '', $path) . "*' '**backups*' ";
        $excludes .= $this->get_wp_files_exclude();
        $excludes = rtrim($excludes, " ");

        if ($this->backup_type() == 'complete') {
            $command[] = 'cd ' . escapeshellarg($home_path);
            $command[] = '&& ' . escapeshellcmd('zip') . ' -rq';
            $command[] = escapeshellarg($backup_file) . ' ./';
            $command[] = '-x ' . $excludes;
            $command = implode(' ', $command);
        }

        // Add the database
        if ($this->backup_type() == 'database') {
            $command[] = 'cd ' . escapeshellarg($path);
            $command[] = '&& ' . escapeshellcmd('zip') . ' -uq';
            $command[] = escapeshellarg($backup_file);
            $command[] = escapeshellarg($this->sqlfilename);
            $command = implode(' ', $command);
        }

        $process = new Symfony\Component\Process\Process($command);
        $process->setTimeout(3600);

        try {
            $process->run();
        } catch (Exception $e) {
            $this->set_status('Backup Process Failed...');
            set_log('Backup Process Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        }

        if (!$process->isSuccessful()) {
            $this->set_status('Backup Process Failed...');
            if ($process->getExitCode() !== 18) {
                set_log('Backup Process Error: Exit code - ' . $process->getExitCode() . ', Working Directory - '. $process->getWorkingDirectory());
            }
        }
        set_log('Backup Process Completed...');

        $this->backfiles = $backup_file;
    }

    /*
     * Create a zip archive file
     */

    public function zip_archeive_backup($backup_file) {
        $this->do_action('ziparchive_started');

        $max_execution_time = (int) @ini_get('max_execution_time');
        $execution_time = WPB_API_Authenticate::get_arg('execution_time');
        $max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;
        if ($max_execution_time == 0) {
            $max_execution_time = 30;
            @ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
            @set_time_limit(0);
        }

        $this->zip = new ZipArchive();
        $this->zip->open($backup_file, ZipArchive::CREATE);

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        if ($this->backup_type() !== 'database') {

            $folders = $this->get_folder();

            if (empty($folders)) {       
                $this->set_folder($home_path);
                $this->get_wp_folder_fallback($home_path);
                $folders = $this->get_folder();
            }

            $zipfolders = get_option('wpb_zip_folders');
            $zipfolders = !empty($zipfolders) ? $zipfolders : array();
            $zipfolder = get_option('wpb_zip_folder');

            while ($folder = array_shift( $folders )) {
                //leave already done folders
                if (in_array($folder, $zipfolders[$home_path], true) && $zipfolder != $folder) {
                    continue;
                }
                
                $time_remaining = microtime(TRUE) - $this->dump_start;
                if ($time_remaining >= ( $max_execution_time - 3 )) {
                    add_option('wpb_zip_restart', 1) or update_option('wpb_zip_restart', 1);
                    unset($this->zip);
                    return 'restart';
                }

                $zipfolders[$home_path][str_ireplace($home_path, '', $folder)] = $folder;
                add_option('wpb_zip_folders', $zipfolders) or update_option('wpb_zip_folders', $zipfolders);
                add_option('wpb_zip_folder', $folder) or update_option('wpb_zip_folder', $folder);

                if (is_dir($folder))
                    $this->zip->addEmptyDir(str_ireplace($home_path, '', $folder));

                $zipfiles = get_option('wpb_zip_files');
                $zipfiles = !empty($zipfiles) ? $zipfiles : array();
                $zipfile = get_option('wpb_zip_file');
                
                $files = $this->get_files();

                if (empty($files)) {
                    $this->get_wp_file_fallback($folder);
                    $files = $this->get_files();
                }

                while ($file = array_shift( $files )) {

                    $time_remaining = microtime(TRUE) - $this->dump_start;
                    if ($time_remaining >= ( $max_execution_time - 3 )) {
                        add_option('wpb_zip_restart', 1) or update_option('wpb_zip_restart', 1);
                        unset($this->zip);
                        return 'restart';
                    }

                    if (in_array($file, $zipfiles[$home_path], true) && $zipfile != $file) {
                        continue;
                    }

                    $zipfiles[$home_path][str_ireplace($home_path, '', $file)] = $file;
                    add_option('wpb_zip_files', $zipfiles) or update_option('wpb_zip_files', $zipfiles);
                    add_option('wpb_zip_file', $file) or update_option('wpb_zip_file', $file);

                    $filepath = str_ireplace($home_path, '', $file);

                    $file_size = filesize($file);

                    if ($file_entry_detail = $this->zip->statName($filepath)) {
                        if ($file_entry_detail['size'] != $file_size) {
                            $this->zip->deleteName($filepath);
                        }
                    }

                    if ($file_size < ( 1024 * 1024 * 2 )) {
                        if (!$this->zip->addFromString($filepath, file_get_contents($file))) {
                            set_log('Cannot add ' . $filepath . ' - ' . $file_size . ' to zip archive!');
                            continue;
                        }
                    } else {
                        $this->zip->addFile($file, $filepath);
                    }
                    
                    $this->unset_file($file);
                }

                delete_option('wpb_zip_files');
                delete_option('wpb_zip_file');
                
                if (file_exists(trailingslashit($this->get_backup_dir()) . 'backup-file.txt'))
                        unlink(trailingslashit($this->get_backup_dir()) . 'backup-file.txt');

                $this->unset_folder($folder);
            }
        }
        // Add the database
        if ($this->backup_type() !== 'file')
            $this->zip->addFile($this->sqlfile, $this->sqlfilename);

        $this->backfiles = $backup_file;

        $this->zip->close();

        delete_option('wpb_zip_folders');
        delete_option('wpb_zip_folder');
        delete_option('wpb_zip_restart');

        if (file_exists(trailingslashit($this->get_backup_dir()) . 'backup-folder.txt'))
            unlink(trailingslashit($this->get_backup_dir()) . 'backup-folder.txt');
        
        $this->do_action('backup_complete');

        return true;
    }

    /*
     * use PclZip library file to zip
     */

    public function pclzip_backup($backup_file) {

        $this->do_action('ziparchive_started');

        define('PCLZIP_TEMPORARY_DIR', $this->get_backup_dir());
        require_once( ABSPATH . 'wp-admin/includes/class-pclzip.php' );

        $path = $this->get_backup_dir();

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        //handle pclzip
        $zip = new PclZip($backup_file);

        if ($this->backup_type() !== 'database')
            $result = $zip->add($home_path, PCLZIP_OPT_REMOVE_PATH, $home_path, PCLZIP_CB_PRE_ADD, 'pclcallback');

        // Add the database
        if ($this->backup_type() !== 'file')
            $result = $zip->add($this->sqlfile, PCLZIP_OPT_REMOVE_PATH, $path);

        if ($result == 0) {

            return array('status' => 'error', 'error' => $archive->errorInfo(true));
        }

        $this->backfiles = $backup_file;
    }

    public function get_wp_files() {

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        if (!empty($files))
            return $files;

        $files = array();

        if (defined('RecursiveDirectoryIterator::FOLLOW_SYMLINKS')) {

            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($home_path, RecursiveDirectoryIterator::FOLLOW_SYMLINKS), RecursiveIteratorIterator::SELF_FIRST, RecursiveIteratorIterator::CATCH_GET_CHILD);
        } else {

            $files = $this->get_wp_files_fallback($home_path);
        }

        return $files;
    }

    /**
     * 
     */
    private function get_wp_folder_fallback($dir, $files = array()) {

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        $exclude = str_ireplace(self::clear_path(trailingslashit(WP_CONTENT_DIR)) . '/', '', $this->get_backup_dir());
        $file_excludes = WPB_API_Authenticate::get_arg('backup_excludes');
        $file_excludes = !empty($file_excludes) ? $file_excludes : array();
        array_push($file_excludes, $exclude);

        $handle = opendir($dir);

        while ($file = readdir($handle)) :

            if ($file === '.' || $file === '..')
                continue;

            if (in_array($file, $file_excludes))
                continue;

            $filepath = self::clear_path(trailingslashit($dir) . $file);
            //$file = str_ireplace($home_path, '', $filepath);

            if (is_dir($filepath)) {
                $files = $this->get_wp_folder_fallback($filepath, $files);
                $this->set_folder(trailingslashit($filepath));
            }

        endwhile;

        return $files;
    }
    
    /**
     * 
     */
    private function get_wp_file_fallback($dir, $files = array()) {

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();
        
        $dir = trailingslashit( $dir );
        $exclude = '.tmp,.svn,.git,desktop.ini,.DS_Store';
        $file_excludes = explode( ',', trim( $exclude ) );
        $handle = opendir($dir);

        while ($file = readdir($handle)) :

            if ($file === '.' || $file === '..')
                continue;

            if (is_dir($dir . $file))
                continue;

            if (in_array($file, $file_excludes))
                continue;

            $filepath = self::clear_path(trailingslashit($dir) . $file);
            
            $file_size = filesize($filepath);

            if (!is_int($file_size) || $file_size < 0 || $file_size > 2147483647) {
                set_log('File size of ' . $filepath . ' - ' . $file_size . ' cannot be retrieved. File might be too large and will not be added to queue.');
                continue;
            }

            if (is_link($filepath)) {
                set_log('Link '.$filepath.' not following.');
            } elseif (!is_readable($filepath)) {
                set_log('File '.$filepath.' is not readable!');
            }

            $this->set_files($filepath);

        endwhile;
        closedir( $file );

        return true;
    }
    
        /**
     * 
     */
    private function get_wp_files_fallback($dir, $files = array()) {
        
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        $handle = opendir($dir);

        while ($file = readdir($handle)) :

            if ($file === '.' || $file === '..')
                continue;

            $filepath = self::clear_path(trailingslashit($dir) . $file);
            $file = str_ireplace($home_path, '', $filepath);

            $files[] = new SplFileInfo($filepath);

            if (is_dir($filepath))
                $files = $this->get_wp_files_fallback($filepath, $files);

        endwhile;

        return $files;
    }
    
    public function set_files($string, $type = false) {

        $dir = get_option('wpb_backup_dir');

        $dir = $dir . '/backup-file.txt';

        if (!$type) {
            if (!$file = fopen($dir, 'a'))
                return;
        } else {
            if (!$file = fopen($dir, 'w+'))
                return;
        }

        if (is_array($string) || is_object($string)) {
            $string = json_encode($string);
        }

        fwrite($file, $string . '' . PHP_EOL);

        fclose($file);
    }

    public function get_files() {

        $dir = get_option('wpb_backup_dir');

        $file = $dir . '/backup-file.txt';

        if (!file_exists($file)) {
            return array();
        }

        $files = array();

        $file_data = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($file_data as $file) {
            $file = trim($file);
            $files[] = $file;
        }
        $files = array_unique($files);
        sort($files);

        return $files;
    }
    
    public function unset_file($line) {

        $dir = get_option('wpb_backup_dir');

        $filepath = $dir . '/backup-file.txt';

        if (!file_exists($filepath)) {
            return array();
        }

        $file_data = file($filepath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($file_data as $key => $file) {
            
            if(stristr($file, $line))
                    unset($file_data[$key]);

        }
        $files = implode( '' . PHP_EOL, array_values($file_data));
        
        $file = fopen($filepath, 'w+');
        fwrite($file, $files);
        fclose($file);

        return TRUE;
    }
    
    public function set_folder($string, $type = false) {

        $dir = get_option('wpb_backup_dir');

        $dir = $dir . '/backup-folder.txt';

        if (!$type) {
            if (!$file = fopen($dir, 'a'))
                return;
        } else {
            if (!$file = fopen($dir, 'w+'))
                return;
        }

        if (is_array($string) || is_object($string)) {
            $string = json_encode($string);
        }

        fwrite($file, $string . '' . PHP_EOL);

        fclose($file);
    }

    public function get_folder() {

        $dir = get_option('wpb_backup_dir');

        $file = $dir . '/backup-folder.txt';

        if (!file_exists($file)) {
            return array();
        }

        $folders = array();

        $file_data = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($file_data as $folder) {
            $folder = trim($folder);
            if (!empty($folder) && is_dir($folder)) {
                $folders[] = $folder;
            }
        }
        $folders = array_unique($folders);
        sort($folders);
        //$count_folder = count( $folders );

        return $folders;
    }

    public function unset_folder($line) {

        $dir = get_option('wpb_backup_dir');

        $filepath = $dir . '/backup-folder.txt';

        if (!file_exists($filepath)) {
            return array();
        }

        $file_data = file($filepath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($file_data as $key => $file) {
            
            if($file == $line)
                    unset($file_data[$key]);

        }
        $files = implode( '' . PHP_EOL, array_values($file_data));
        
        $file = fopen($filepath, 'w+');
        fwrite($file, $files);
        fclose($file);

        return TRUE;
    }

    public function wpb_is_safe_mode() {
        $result = ini_get("safe_mode");
        if ((int) $result === 0 || strtolower($result) === "off") {
            return false;
        }

        return true;
    }

    /*
     * Check if shell installed
     */

    public function wpb_is_shellinstalled() {

        if ($this->wpb_is_safe_mode()) {
            return false;
        }

        $blocklist = array_merge(explode(',', ini_get('disable_functions')), explode(',', ini_get('suhosin.executor.func.blacklist')));
        $blocklist = array_map('trim', $blocklist);
        $blocklist = array_map('strtolower', $blocklist);
        $blocklist = array_filter($blocklist);

        $required = array('proc_get_status', 'proc_open');

        if (count(array_diff($required, $blocklist)) != count($required)) {
            return false;
        }

        return true;
    }

    /*
     * error reporting
     */

    function reporterror($error) {

        throw new Exception($error);
    }

    /*
     * destructor
     */

    function __destruct() {

        // clear all memory        
        // delete backup files

        $this->zip->close(); 
    }

    public function mysqlidump_back() {
        $this->do_action('mysqldump_started');
        $max_execution_time = (int) @ini_get('max_execution_time');
        $execution_time = WPB_API_Authenticate::get_arg('execution_time');
        $max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;
        //@set_time_limit( 300 );
        if ($max_execution_time == 0) {
            $max_execution_time = 30;
            @ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
            @set_time_limit(0);
        }

        global $wpdb;
        $this->port = NULL;
        $this->socket = NULL;
        $this->host = DB_HOST;

        if (strstr($this->host, ':')) {
            $port_or_socket = explode(':', $this->host, 2);
            $port_or_socket[0] = trim($port_or_socket[0]);
            $port_or_socket[1] = trim($port_or_socket[1]);
            if (empty($port_or_socket[0]))
                $this->host = NULL;
            else
                $this->host = $port_or_socket[0];
            if (is_numeric($port_or_socket[1]))
                $this->port = (int) $port_or_socket[1];
            else
                $this->socket = $port_or_socket[1];
        }

        if (!class_exists('mysqli')) {
            set_log('mysqli class not exist');
        }

        $dbclientflags = defined('MYSQL_CLIENT_FLAGS') ? MYSQL_CLIENT_FLAGS : 0;

        $this->db_connect = mysqli_init();

        if (!$this->db_connect) {
            set_log('Cannot init MySQLi database connection');
        }

        //connect to Database
        if (!$this->db_connect->real_connect($this->host, DB_USER, DB_PASSWORD, DB_NAME, $this->port, $this->socket, $dbclientflags)) {
            set_log('Cannot connect to MySQL database');
        }
        $tables = $this->db_connect->query('SHOW TABLES');

        while ($table = $tables->fetch_array(MYSQLI_NUM)) {
            $this->sql_tables[] = $table[0];
        }
        $tables->close();

        if (get_option('wpb_dumb_restart') == '') {

            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            $home_path = get_home_path();

            $dirname = 'WPB-database-backups/';
            $upload_dir = wp_upload_dir();

            if ((!is_dir($home_path) && !is_writable(dirname($home_path)) ) || ( is_dir($home_path) && !is_writable($home_path) ))
                $home_path = self::clear_path(trailingslashit($upload_dir['basedir']) . $dirname);

            if (!is_dir($home_path) && is_writable(dirname($home_path)))
                mkdir($home_path, 0755);

            $filename = 'database_' . DB_NAME . '.sql';
            $this->sqlfilename = strtolower(sanitize_file_name(remove_accents($filename)));
            $this->sqlfile = trailingslashit($home_path) . $this->sqlfilename;

            // Delete the database dump
            if (file_exists($this->sqlfile))
                unlink($this->sqlfile);

            $this->sqlfile = trailingslashit($this->get_backup_dir()) . $this->sqlfilename;
            // Delete the database dump
            if (file_exists($this->sqlfile))
                unlink($this->sqlfile);

            // Header information in SQL-file
            $sql_statements = "# MySQL database backup\n";
            $sql_statements .= "#\n";
            $sql_statements .= "# Generated: " . date("l j. F Y H:i T") . "\n";
            $sql_statements .= "# Hostname: " . $this->host . "\n";
            $sql_statements .= "# Database: " . $this->backquote(DB_NAME) . "\n";
            $this->save_sql($sql_statements);
            $sql_statements = "";
        }

        $dumpval = get_option('wpb_dumbed_table');
        $ival = !empty($dumpval['i']) ? $dumpval['i'] : 0;

        // Process the tables to backup
        for ($i = $ival; $i < count($this->sql_tables); $i++) {

            $curr_table = $this->sql_tables[$i];
            if ($dumpval['curr_table'] != $curr_table) {
                // Create the SQL statements
                $sql_statements .= "# --------------------------------------------------------\n";
                $sql_statements .= "# Table: " . $this->backquote($curr_table) . "\n";
                $sql_statements .= "# --------------------------------------------------------\n";
                $this->save_sql($sql_statements);
                $sql_statements = "";
            }
            $startoption = get_option('wpb_dumb_start');
            $start = !empty($startoption['name'][$table]['start']) ? $startoption['name'][$table]['start'] : 0;
            $sql_statements = '';
            $res = $this->make_isql($sql_statements, $curr_table, $i);

            $startVal = !empty($startoption) ? $startoption : array();
            $startVal['name'][$curr_table] = array('start' => $start + $res);
            add_option('wpb_dumb_start', $startVal) or update_option('wpb_dumb_start', $startVal);

            $time_remaining = microtime(TRUE) - $this->dump_start;
            if ($time_remaining >= ( $max_execution_time - 3 )) {
                add_option('wpb_dumb_restart', 1) or update_option('wpb_dumb_restart', 1);
                return 'restart';
            }
        } // for ($i = 0; $i < mysql_num_rows($tables); $i++)
        $this->db_connect->close();
        set_log('Backup MySQliDump Finish...');
        $this->set_status('Backup MySQliDump Finish...');
        $this->do_action('ziparchive_started');
        return $this->backupfiles();
    }

    private function make_isql($sql_statements, $table, $i) {
        global $wpdb;
        $this->port = NULL;
        $this->socket = NULL;
        $this->host = DB_HOST;

        $max_execution_time = (int) @ini_get('max_execution_time');
        $execution_time = WPB_API_Authenticate::get_arg('execution_time');
        $max_execution_time = !empty($execution_time) ? $execution_time : $max_execution_time;
        if ($max_execution_time == 0) {
            $max_execution_time = 30;
            @ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
            @set_time_limit(0);
        }

        if (strstr($this->host, ':')) {
            $port_or_socket = explode(':', $this->host, 2);
            $port_or_socket[0] = trim($port_or_socket[0]);
            $port_or_socket[1] = trim($port_or_socket[1]);
            if (empty($port_or_socket[0]))
                $this->host = NULL;
            else
                $this->host = $port_or_socket[0];
            if (is_numeric($port_or_socket[1]))
                $this->port = (int) $port_or_socket[1];
            else
                $this->socket = $port_or_socket[1];
        }

        if (!class_exists('mysqli')) {
            set_log('mysqli class not exist');
        }

        $dbclientflags = defined('MYSQL_CLIENT_FLAGS') ? MYSQL_CLIENT_FLAGS : 0;

        $this->db_connect = mysqli_init();

        if (!$this->db_connect) {
            set_log('Cannot init MySQLi database connection');
        }

        //connect to Database
        if (!$this->db_connect->real_connect($this->host, DB_USER, DB_PASSWORD, DB_NAME, $this->port, $this->socket, $dbclientflags)) {
            set_log('Cannot connect to MySQL database');
        }

        $startoption = get_option('wpb_dumb_start');
        $start = !empty($startoption['name'][$table]['start']) ? $startoption['name'][$table]['start'] : 0;
        add_option('wpb_dumbed_table', array('curr_table' => $table, 'i' => $i)) or update_option('wpb_dumbed_table', array('curr_table' => $table, 'i' => $i));
        if ($start == 0) {
            $sql_statements = "";

            // Add SQL statement to drop existing table
            $sql_statements .= "\n";
            $sql_statements .= "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "# Delete any existing table " . $this->backquote($table) . "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "\n";
            $sql_statements .= "DROP TABLE IF EXISTS " . $this->backquote($table) . ";\n";

            // Table structure
            // Comment in SQL-file
            $sql_statements .= "\n";
            $sql_statements .= "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "# Table structure of table " . $this->backquote($table) . "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "\n";

            // Get table structure
            $query = "SHOW CREATE TABLE " . $this->backquote($table);
            $result = $this->db_connect->query($query);

            if ($result) {
                $sql_create_arr = $result->fetch_assoc();
                $result->close();
                $sql_statements .= $sql_create_arr['Create Table'] . ";\n";
            } // ($result)
            $this->save_sql($sql_statements);
            $sql_statements = "";
            // Table data contents
            // Get table contents
            $query = "SELECT * FROM " . $this->backquote($table);
            $result = $this->db_connect->query($query, MYSQLI_USE_RESULT);
        }

        if ($start != 0) {
            $query = "SELECT * FROM " . $this->backquote($table) . " LIMIT " . $start . ", 18446744073709551615";
            $result = $this->db_connect->query($query, MYSQLI_USE_RESULT);
        }

        $field_set = array();
        $field_info = array();

        // Comment in SQL-file
        $sql_statements .= "\n";
        $sql_statements .= "\n";
        $sql_statements .= "#\n";
        $sql_statements .= "# Data contents of table " . $table . "\n";
        $sql_statements .= "#\n";

        $fields = $result->fetch_fields();
        $i = 0;
        foreach ($fields as $filed) {
            $field_set[$i] = $filed->orgname;
            $field_info[$field_set[$i]] = $filed;
            $i ++;
        }

        // Sets the scheme
        $entries = 'INSERT INTO ' . $this->backquote($table) . ' VALUES (';
        $search = array("\x00", "\x0a", "\x0d", "\x1a");  //\x08\\x09, not required
        $replace = array('\0', '\n', '\r', '\Z');
        $current_row = 0;

        //$startVal = !empty($startoption) ? $startoption : array();
        //$startVal['name'][$table] = array('start' => $start + $current_row);
        //add_option('wpb_dumb_start', $startVal) or update_option('wpb_dumb_start', $startVal);

        while ($row = $result->fetch_assoc()) {
            $values = array();
            foreach ($row as $key => $value) {
                if (is_null($value) || !isset($value)) {
                    $value = "NULL";
                } elseif (in_array((int) $field_info[$key]->type, array(MYSQLI_TYPE_DECIMAL, MYSQLI_TYPE_TINY, MYSQLI_TYPE_SHORT, MYSQLI_TYPE_LONG, MYSQLI_TYPE_FLOAT, MYSQLI_TYPE_DOUBLE, MYSQLI_TYPE_LONGLONG, MYSQLI_TYPE_INT24), true)) {//is value numeric no esc
                    $value = empty($value) ? 0 : $value;
                } else {
                    $value = "'" . $this->db_connect->real_escape_string($value) . "'";
                }
                $values[] = $value;
            }

            $sql_statements .= " \n" . $entries . implode(', ', $values) . ');';

            $this->save_sql($sql_statements);
            $sql_statements = '';
            // write the rows in batches of 100
            /* if ($batch_write === 100) {
              $batch_write = 0;
              $this->save_sql($sql_statements);
              $sql_statements = '';
              } */

            //$batch_write++;

            $current_row ++;

            //$startVal = !empty($startoption) ? $startoption : array();
            //$startVal['name'][$table] = array('start' => $start + $current_row);
            //add_option('wpb_dumb_start', $startVal) or update_option('wpb_dumb_start', $startVal);

            $time_remaining = microtime(TRUE) - $this->dump_start;
            if ($time_remaining >= ( $max_execution_time - 3 )) {
                $result->close();
                add_option('wpb_dumb_restart', 1) or update_option('wpb_dumb_restart', 1);
                return $current_row;
            }
            unset($values);
        }

        $result->close();
        // Create footer/closing comment in SQL-file
        $sql_statements .= "\n";
        $sql_statements .= "#\n";
        $sql_statements .= "# End of data contents of table " . $table . "\n";
        $sql_statements .= "# --------------------------------------------------------\n";
        $sql_statements .= "\n";
        $this->save_sql($sql_statements);
        return $current_row;
    }

    public function mysqldump_back() {

        $this->do_action('mysqldump_started');
        $max_execution_time = (int) @ini_get('max_execution_time');

        if ($max_execution_time == 0) {
            $max_execution_time = 30;
            @ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
            @set_time_limit(0);
        }

        global $wpdb;
        $this->db_connect = @mysql_pconnect(DB_HOST, DB_USER, DB_PASSWORD);

        if (!$this->db_connect)
            $this->db_connect = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);

        mysql_select_db(DB_NAME, $this->db_connect);

        $tables = mysql_query('SHOW TABLES');

        if (get_option('wpb_dumb_restart') == '') {

            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            $home_path = get_home_path();

            $dirname = 'WPB-database-backups/';
            $upload_dir = wp_upload_dir();

            if ((!is_dir($home_path) && !is_writable(dirname($home_path)) ) || ( is_dir($home_path) && !is_writable($home_path) ))
                $home_path = self::clear_path(trailingslashit($upload_dir['basedir']) . $dirname);

            if (!is_dir($home_path) && is_writable(dirname($home_path)))
                mkdir($home_path, 0755);

            $filename = 'database_' . DB_NAME . '.sql';
            $sqlfilename = strtolower(sanitize_file_name(remove_accents($filename)));
            $sqlfile = trailingslashit($home_path) . $sqlfilename;

            // Delete the database dump
            if (file_exists($sqlfile))
                unlink($sqlfile);

            $sqlfile = trailingslashit($this->get_backup_dir()) . $sqlfilename;
            // Delete the database dump
            if (file_exists($sqlfile))
                unlink($sqlfile);

            // Header information in SQL-file
            $sql_statements = "# MySQL database backup\n";
            $sql_statements .= "#\n";
            $sql_statements .= "# Generated: " . date("l j. F Y H:i T") . "\n";
            $sql_statements .= "# Hostname: " . DB_HOST . "\n";
            $sql_statements .= "# Database: " . $this->backquote(DB_NAME) . "\n";
        }

        $dumpval = get_option('wpb_dumbed_table');
        $ival = !empty($dumpval['i']) ? $dumpval['i'] : 0;

        // Process the tables to backup
        for ($i = 0; $i < mysql_num_rows($tables); $i++) {

            $curr_table = mysql_tablename($tables, $i);
            if ($dumpval['curr_table'] != $curr_table) {
                // Create the SQL statements
                $sql_statements .= "# --------------------------------------------------------\n";
                $sql_statements .= "# Table: " . $this->backquote($curr_table) . "\n";
                $sql_statements .= "# --------------------------------------------------------\n";
            }
            $startoption = get_option('wpb_dumb_start');
            $start = !empty($startoption['name'][$curr_table]['start']) ? $startoption['name'][$curr_table]['start'] : 0;

            $result = $this->make_sql($sql_statements, $curr_table, $i);
            
            $startVal = !empty($startoption) ? $startoption : array();
            $startVal['name'][$curr_table] = array('start' => $start + $result);
            add_option('wpb_dumb_start', $startVal) or update_option('wpb_dumb_start', $startVal);

            $time_remaining = microtime(TRUE) - $this->dump_start;
            if ($time_remaining >= ( $max_execution_time - 3 )) {
                add_option('wpb_dumb_restart', 1) or update_option('wpb_dumb_restart', 1);
                return 'restart';
            }
        } // for ($i = 0; $i < mysql_num_rows($tables); $i++)

        set_log('Backup MySQlDump Finish...');
        $this->set_status('Backup MySQlDump Finish...');
        $this->do_action('ziparchive_started');
        return $this->backupfiles();
    }

    /**
     * 
     * Reads the Database table in $table and creates
     * SQL Statements for recreating structure and data
     * Taken partially from phpMyAdmin and partially from
     * Alain Wolf, Zurich - Switzerland
     * Website: http://restkultur.ch/personal/wolf/scripts/db_backup/
     * 
     * @param type $table
     * @return string
     */
    private function make_sql($sql_statements, $table, $i) {
        global $wpdb;

        $this->db_connect = @mysql_pconnect(DB_HOST, DB_USER, DB_PASSWORD);

        if (!$this->db_connect)
            $this->db_connect = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);

        mysql_select_db(DB_NAME, $this->db_connect);

        $max_execution_time = (int) @ini_get('max_execution_time');

        if ($max_execution_time == 0) {
            $max_execution_time = 30;
            @ini_set('memory_limit', apply_filters('admin_memory_limit', WP_MAX_MEMORY_LIMIT));
            @set_time_limit(0);
        }

        $startoption = get_option('wpb_dumb_start');
        $start = !empty($startoption['name'][$table]['start']) ? $startoption['name'][$table]['start'] : 0;
        if ($start == 0) {
            $sql_statements .= "";

            // Add SQL statement to drop existing table
            $sql_statements .= "\n";
            $sql_statements .= "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "# Delete any existing table " . $this->backquote($table) . "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "\n";
            $sql_statements .= "DROP TABLE IF EXISTS " . $this->backquote($table) . ";\n";

            // Table structure
            // Comment in SQL-file
            $sql_statements .= "\n";
            $sql_statements .= "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "# Table structure of table " . $this->backquote($table) . "\n";
            $sql_statements .= "#\n";
            $sql_statements .= "\n";

            // Get table structure
            $query = "SHOW CREATE TABLE " . $this->backquote($table);
            $result = mysql_query($query, $this->db_connect);

            if ($result) {

                if (mysql_num_rows($result) > 0) {
                    $sql_create_arr = mysql_fetch_array($result);
                    $sql_statements .= $sql_create_arr[1];
                }
                mysql_free_result($result);
                $sql_statements .= " ;";
            } // ($result)
            // Table data contents
            // Get table contents
            $query = "SELECT * FROM " . $this->backquote($table);
            $result = mysql_query($query, $this->db_connect);
            if ($result) {
                $fields_cnt = mysql_num_fields($result);
                $rows_cnt = mysql_num_rows($result);
            } // if ($result)
        }

        if ($start != 0) {
            $query = "SELECT * FROM " . $this->backquote($table) . " LIMIT " . $start . ", 18446744073709551615";
            $result = mysql_query($query, $this->db_connect);
            if ($result) {
                $fields_cnt = mysql_num_fields($result);
                $rows_cnt = mysql_num_rows($result);
            } // if ($result)
        }

        add_option('wpb_dumbed_table', array('curr_table' => $table, 'i' => $i)) or update_option('wpb_dumbed_table', array('curr_table' => $table, 'i' => $i));

        // Comment in SQL-file
        $sql_statements .= "\n";
        $sql_statements .= "\n";
        $sql_statements .= "#\n";
        $sql_statements .= "# Data contents of table " . $table . " (" . $rows_cnt . " records)\n";
        $sql_statements .= "#\n";

        // Checks whether the field is an integer or not
        for ($j = 0; $j < $fields_cnt; $j++) {
            $field_set[$j] = $this->backquote(mysql_field_name($result, $j));
            $type = mysql_field_type($result, $j);
            if ($type == 'tinyint' || $type == 'smallint' || $type == 'mediumint' || $type == 'int' ||
                    $type == 'bigint') {
                $field_num[$j] = TRUE;
            } else {
                $field_num[$j] = FALSE;
            }
        } // end for
        // Sets the scheme
        $entries = 'INSERT INTO ' . $this->backquote($table) . ' VALUES (';
        $search = array("\x00", "\x0a", "\x0d", "\x1a");  //\x08\\x09, not required
        $replace = array('\0', '\n', '\r', '\Z');
        $current_row = 0;

        //$startVal = !empty($startoption) ? $startoption : array();
        //$startVal['name'][$table] = array('start' => $start + $current_row);
        //add_option('wpb_dumb_start', $startVal) or update_option('wpb_dumb_start', $startVal);
        while ($row = mysql_fetch_row($result)) {
            $current_row++;
            for ($j = 0; $j < $fields_cnt; $j++) {
                if (!isset($row[$j])) {
                    $values[] = 'NULL';
                } else if ($row[$j] == '0' || $row[$j] != '') {
                    // a number
                    if ($field_num[$j]) {
                        $values[] = $row[$j];
                    } else {
                        $values[] = "'" . str_replace($search, $replace, $this->sql_addslashes($row[$j])) . "'";
                    } //if ($field_num[$j])
                } else {
                    $values[] = "''";
                } // if (!isset($row[$j]))
            } // for ($j = 0; $j < $fields_cnt; $j++)
            $sql_statements .= " \n" . $entries . implode(', ', $values) . ') ;';

            // write the rows in batches of 100
            if ($batch_write === 100) {
                $batch_write = 0;
                $this->save_sql($sql_statements);
                $sql_statements = '';
            }

            $batch_write++;

            //$startVal = !empty($startoption) ? $startoption : array();
            //$startVal['name'][$table] = array('start' => $start + $current_row);
            //add_option('wpb_dumb_start', $startVal) or update_option('wpb_dumb_start', $startVal);

            $time_remaining = microtime(TRUE) - $this->dump_start;
            if ($time_remaining >= ( $max_execution_time - 3 )) {
                add_option('wpb_dumb_restart', 1) or update_option('wpb_dumb_restart', 1);
                return $current_row;
            }

            unset($values);
        } // while ($row = mysql_fetch_row($result))
        mysql_free_result($result);

        // Create footer/closing comment in SQL-file
        $sql_statements .= "\n";
        $sql_statements .= "#\n";
        $sql_statements .= "# End of data contents of table " . $table . "\n";
        $sql_statements .= "# --------------------------------------------------------\n";
        $sql_statements .= "\n";
        $this->save_sql($sql_statements);
        return $current_row;
    }

    private function save_sql($sql) {

        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        $home_path = get_home_path();

        $dirname = 'WPB-database-backups';
        $upload_dir = wp_upload_dir();

        if ((!is_dir($home_path) && !is_writable(dirname($home_path)) ) || ( is_dir($home_path) && !is_writable($home_path) ))
            $home_path = self::clear_path(trailingslashit($upload_dir['basedir']) . $dirname);

        if (!is_dir($home_path) && is_writable(dirname($home_path)))
            mkdir($home_path, 0755);

        $filename = 'database_' . DB_NAME . '.sql';
        $this->sqlfilename = strtolower(sanitize_file_name(remove_accents($filename)));
        if ($this->backup_type() == 'complete') {
            $this->sqlfile = trailingslashit($home_path) . $this->sqlfilename;
        } else {
            $this->sqlfile = trailingslashit($this->get_backup_dir()) . $this->sqlfilename;
        }

        if (is_writable($this->sqlfile) || !file_exists($this->sqlfile)) {

            //Append to a File
            $handle = @fopen($this->sqlfile, 'a') or die('Cannot open file:  ' . $this->sqlfile);

            @fwrite($handle, $sql);

            @fclose($handle);

            return true;
        }
    }

}

/*
 * PclZip call back function
 */

function pclcallback($p_event, $p_header) {

    $file_excludes = str_replace(" ", '', Back_dir::get_instance()->get_wp_files_exclude());
    $excludes = "([\s\S]*?)backups|" . rtrim(str_replace("*", '|', str_replace("'", '', $file_excludes)), "|");
    $excludes = rtrim($excludes, "|");

    // Don't try to add unreadable files.
    if (!is_readable($p_header['filename']) || !file_exists($p_header['filename']))
        return false;

    // Match everything else past the exclude list
    elseif (preg_match('(' . $excludes . ')', $p_header['stored_filename']))
        return false;

    return true;
}
