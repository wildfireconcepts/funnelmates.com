<?php

function wpb_fbpixel_management() {

    $return_val = array();
    $return_val['msg'] = '';
    $return_val['status'] = '';
    $return_val['fbpixel_id'] = '';

    $pixelid = WPB_API_Authenticate::get_arg('pixelid');
    $pixel_action = WPB_API_Authenticate::get_arg('pixel_action');

    if (!empty($pixelid) && $pixel_action == 'add') {
        add_option('wpb_fbpixel_id', $pixelid) or update_option('wpb_fbpixel_id', $pixelid);
        $return_val['status'] = 'success';
        $return_val['msg'] = 'facebook pixel id inserted';
    } elseif ($pixel_action == 'select') {
        $wpb_fbpixel_id = get_option('wpb_fbpixel_id');
        $wpb_fbpixel_id = !empty($wpb_fbpixel_id) ? $wpb_fbpixel_id : null;
        $return_val['status'] = 'success';
        $return_val['fbpixel_id'] = $wpb_fbpixel_id;
        $return_val['msg'] = 'facebook pixel id selected';
    } elseif ($pixel_action == 'delete') {
        $wpb_fbpixel_id = delete_option('wpb_fbpixel_id');
        $return_val['status'] = 'success';
        $return_val['msg'] = 'facebook pixel id deleted';
    } else {
        $return_val['status'] = 'failed';
        $return_val['msg'] = 'empty fields not allowed';
    }

    return $return_val;
}

function wpb_other_pixel_management() {


    $return_val = array();
    $return_val['msg'] = '';
    $return_val['status'] = '';
    $return_val['otherpixel_script'] = '';

    $pixelscript = WPB_API_Authenticate::get_arg('pixelscript');
    $pixel_action = WPB_API_Authenticate::get_arg('pixel_action');

    if (!empty($pixelscript) && $pixel_action == 'add') {
        add_option('wpb_otherpixel_script', $pixelscript) or update_option('wpb_otherpixel_script', $pixelscript);
        $return_val['status'] = 'success';
        $return_val['msg'] = 'other pixel script inserted';
    } elseif ($pixel_action == 'select') {
        $wpb_otherpixel_script = get_option('wpb_otherpixel_script');
        $wpb_otherpixel_script = !empty($wpb_otherpixel_script) ? $wpb_otherpixel_script : null;
        $return_val['status'] = 'success';
        $return_val['otherpixel_script'] = $wpb_otherpixel_script;
        $return_val['msg'] = 'other pixel script selected';
    } elseif ($pixel_action == 'delete') {
        $wpb_otherpixel_script = delete_option('wpb_otherpixel_script');
        $return_val['status'] = 'success';
        $return_val['msg'] = 'other pixel script deleted';
    } else {
        $return_val['status'] = 'failed';
        $return_val['msg'] = 'empty fields not allowed';
    }

    return $return_val;
}

?>