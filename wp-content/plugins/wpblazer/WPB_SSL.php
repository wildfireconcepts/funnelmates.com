<?php

class wpb_ssl {

    private $wpb_cf_email = '';
    private $wpb_cf_apikey = '';
    private $wpb_cf_zoneid = '';
    private $wpb_ssl_mode = '';

    public function __construct($wpb_cf_email = null, $wpb_cf_apikey = null, $wpb_cf_zoneid = null, $wpb_ssl_mode = null) {
        $this->wpb_cf_email = $wpb_cf_email;
        $this->wpb_cf_apikey = $wpb_cf_apikey;
        $this->wpb_cf_zoneid = $wpb_cf_zoneid;
        $this->wpb_ssl_mode = $wpb_ssl_mode;
    }

    public function run() {
        if ($this->wpb_ssl_mode == 'off') {
            wp_cache_flush();
            $this->get_disable_url();
            $this->purge_caching_plugins();
            $this->purge_cloudflare();

            if ($_SERVER['REQUEST_SCHEME'] == 'https' || $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
                $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: ' . $url, true, 301);
                exit();
            } else {
                return;
            }
        }
        if ($this->wpb_ssl_mode == 'cloudflare') {
            if (!$this->is_cloudflare()) {
                return;
            }
            if ($this->wpb_cf_zoneid) {
                $actions = array(
                    'switch_theme',
                    'customize_save_after',
                    'publish_post',
                    'future_to_publish',
                    'autoptimize_action_cachepurged',
                    'deleted_post',
                    'edit_post',
                    'delete_attachment',
                );

                foreach ($actions as $action) {
                    add_action($action, array('wpb_ssl', 'purge_cloudflare'), 10, 2);
                }
            }
            if (!(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
                $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: ' . $redirect);
                exit();
            } else {
                return;
            }
        }
        $rewrite_domains = array($_SERVER['HTTP_HOST']);
        $this->configure_rewrites($rewrite_domains);
        add_filter('wp_cache_ob_callback_filter', array($this, 'content_filter'));
        ob_start(array($this, 'content_filter'));
    }

    public static function is_cloudflare() {
        return array_key_exists('HTTP_CF_CONNECTING_IP', $_SERVER);
    }

    public function purge_cloudflare() {
        if (!$this->is_cloudflare()) {
            return;
        }
        $url = "https://api.cloudflare.com/client/v4/zones/{$this->wpb_cf_zoneid}/purge_cache";
        $headers = array(
            'X-Auth-Email' => $this->wpb_cf_email,
            'X-Auth-Key' => $this->wpb_cf_apikey,
            'Content-Type' => 'application/json'
        );
        $args = array(
            'method' => 'DELETE',
            'sslverify' => false,
            'headers' => $headers,
            'body' => '{"purge_everything":true}'
        );
        $response = wp_remote_request($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        if ($response['response']['code'] != '200') {
            return false;
        }
        $response = json_decode($response['body']);
        if ($response->success) {
            return true;
        }
    }

    public function patch_proxy_connection() {
        if (array_key_exists('HTTP_X_FORWARDED_PROTO', $_SERVER)) {
            if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
                $_SERVER['REQUEST_SCHEME'] = 'https';
                $_SERVER['HTTPS'] = 'on';
                $_SERVER['SERVER_PORT'] = 443;
            }
        }
    }

    public function redirect_non_ssl($redirect_type) {
        if (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == 'on') {
            return;
        }
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header("Location: $redirect", true, $redirect_type);
        exit();
    }

    public function get_disable_url() {
        return get_home_url(null, '', 'http');
    }

    public function purge_caching_plugins() {
        //W3 TC
        if (class_exists('W3_Plugin_TotalCacheAdmin')) {
            echo "W3_Plugin_TotalCacheAdmin exists <br/>";
            $plugin_totalcacheadmin = & w3_instance('W3_Plugin_TotalCacheAdmin');
            $plugin_totalcacheadmin->flush_all();
        }
        //WP Super Cache
        if (function_exists('wp_cache_clean_cache')) {
            echo "wp_cache_clean_cache exists <br/>";
            global $file_prefix;
            wp_cache_clean_cache($file_prefix);
        }
    }

    public function content_filter($content) {
        return str_replace($this->from, $this->to, $content);
    }

    public function configure_rewrites($domains) {
        $from = array();
        $to = array();

        foreach ($domains as $domain) {
            $from[] = 'http://' . $domain;
            $to[] = 'https://' . $domain;

            $from[] = json_encode('http://' . $domain);
            $to[] = json_encode('https://' . $domain);
        }
        $this->from = $from;
        $this->to = $to;
    }

}

function wpb_ssl_init() {

    $return_val = array();
    $return_val['msg'] = '';
    $return_val['url'] = '';
    $return_val['status'] = '';

    $wpb_cf_email = WPB_API_Authenticate::get_arg('wpb_cf_email');
    $wpb_cf_apikey = WPB_API_Authenticate::get_arg('wpb_cf_apikey');
    $wpb_cf_zoneid = WPB_API_Authenticate::get_arg('wpb_cf_zoneid');
    $wpb_ssl_mode = WPB_API_Authenticate::get_arg('wpb_ssl_mode');

    if (!empty($wpb_cf_email) && !empty($wpb_cf_apikey) && !empty($wpb_cf_zoneid) && !empty($wpb_ssl_mode)) {

        $return_val['status'] = 'success';
        $return_val['msg'] .= "SUCESS - wpblazer ssl management initialized <br/>";

        add_option('wpb_cf_email', $wpb_cf_email) or update_option('wpb_cf_email', $wpb_cf_email);
        add_option('wpb_cf_apikey', $wpb_cf_apikey) or update_option('wpb_cf_apikey', $wpb_cf_apikey);
        add_option('wpb_cf_zoneid', $wpb_cf_zoneid) or update_option('wpb_cf_zoneid', $wpb_cf_zoneid);
        add_option('wpb_ssl_mode', $wpb_ssl_mode) or update_option('wpb_ssl_mode', $wpb_ssl_mode);
    } else {
        $return_val['status'] = 'failed';
        $return_val['msg'] .= "ERROR - fileds are empty <br/>";
    }
    return $return_val;
}

?>