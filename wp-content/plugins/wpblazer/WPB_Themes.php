<?php
/**
 * Get all themes
 */
function wpb_get_theme() {

	require_once( ABSPATH . '/wp-admin/includes/theme.php' );

	if ( function_exists( 'wp_get_themes' ) )
		$themes = wp_get_themes();
	else
		$themes = get_themes();	
 
	return $themes;

}

/**
 * Get all update themes
 */
function wpb_get_themes() {

	require_once( ABSPATH . '/wp-admin/includes/theme.php' );

	if ( function_exists( 'wp_get_themes' ) )
		$themes = wp_get_themes();
	else
		$themes = get_themes();
		
	@wp_update_themes();

	// Get the active theme
	$active  = get_option( 'current_theme' );

	$current = get_site_transient( 'update_themes' );

	if ( function_exists( 'get_site_transient' ) && $transient = $current )
		$current = $transient;

	elseif ( $transient = get_transient( 'update_themes' ) )
		$current = $transient;

	else
		$current = get_option( 'update_themes' );

	foreach ( (array) $themes as $key => $theme ) {

		// WordPress 3.4+
		if ( is_object( $theme ) && is_a( $theme, 'WP_Theme' ) ) {

			$new_version = isset( $current->response[$theme['Template']] ) ? $current->response[$theme['Template']]['new_version'] : null;

			$theme_array = array(
				'Name'           => $theme->get( 'Name' ),
				'active'         => $active == $theme->get( 'Name' ),
				'Template'       => $theme->get_template(),
				'Stylesheet'     => $theme->get_stylesheet(),
				'Screenshot'     => $theme->get_screenshot(),
				'AuthorURI'      => $theme->get( 'AuthorURI' ),
				'Author'         => $theme->get( 'Author' ),
				'latest_version' => $new_version ? $new_version : $theme->get( 'Version' ),
				'Version'        => $theme->get( 'Version' ),
				'ThemeURI'       => $theme->get( 'ThemeURI' )
			);

			$themes[$key] = $theme_array;

		} else {

			$new_version = isset( $current->response[$theme['Template']] ) ? $current->response[$theme['Template']]['new_version'] : null;

			if ( $active == $theme['Name'] )
				$themes[$key]['active'] = true;

			else
				$themes[$key]['active'] = false;

			if ( $new_version ) {

				$themes[$key]['latest_version'] = $new_version;
				$themes[$key]['latest_package'] = $current->response[$theme['Template']]['package'];

			} else {

				$themes[$key]['latest_version'] = $theme['Version'];

			}
		}
	}
	return $themes;
}

/**
 * Activate  theme
 */
function wpb_activate_theme() {

$theme = sanitize_text_field(WPB_API_Authenticate::get_arg( 'theme' ));

$active  = get_option( 'current_theme' );
$theme_name = wp_get_theme( $theme );

	if ( $active == $theme_name->Name )
		return array( 'status' => 'error', 'error' => 'Theme already activated.' );

	if ( ! wp_get_theme( $theme )->exists() )
		return array( 'status' => 'error', 'error' => 'Theme is not installed.' );

	switch_theme( $theme );
	return array( 'status' => 'success' );
}

/**
 * Update a theme
 */
function wpb_update_theme() {

	$theme = sanitize_text_field(WPB_API_Authenticate::get_arg( 'theme' ));

	include_once ( ABSPATH . 'wp-admin/includes/admin.php' );
	require_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
	require_once WPBLAZER_PLUGIN_URL . 'skin/wpblazer-theme-upgrader-skin.php';

	// check filesystem access
	wpb_precheck ('filesystem_access');

	$skin = new WPBLAZER_Theme_Upgrader_Skin();
	$upgrader = new Theme_Upgrader( $skin );

	// Do the upgrade
	ob_start();
	$output = $upgrader->upgrade( $theme );
	$data = ob_get_contents();
	ob_clean();

	if ( ( ! $output && ! is_null( $output ) ) || $data )
		return array( 'status' => 'error', 'error' => 'file_permissions_error' );

	elseif ( is_wp_error( $output ) )
		return array( 'status' => 'error', 'error' => $output->get_error_code() );

	if ( $skin->error )
		return array( 'status' => 'error', 'error' => $skin->error );

	return array( 'status' => 'success' );

}

/**
 * Install a theme
 */
function wpb_install_theme() {

    $theme = sanitize_text_field(WPB_API_Authenticate::get_arg('theme_url'));
    $activate = sanitize_text_field(WPB_API_Authenticate::get_arg('activate'));

    include_once ABSPATH . 'wp-admin/includes/admin.php';
    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    include_once ABSPATH . 'wp-includes/update.php';
    require_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
    require_once WPBLAZER_PLUGIN_URL . 'skin/wpblazer-theme-upgrader-skin.php';

    $skin = new WPBLAZER_Theme_Upgrader_Skin();
    $upgrader = new Theme_Upgrader($skin);

    //$result = $upgrader->install( $theme );
    $result = @$upgrader->run(
                    array(
                        'package' => $theme,
                        'destination' => WP_CONTENT_DIR . '/themes',
                        'clear_destination' => true, //Do not overwrite files.
                        'clear_working' => true,
                        'hook_extra' => array(),
                    )
    );

    if (is_wp_error($result))
        return array('status' => 'error', 'error' => $result->get_error_code());
    else if (!$result)
        return array('status' => 'error', 'error' => 'Unknown error installing theme.');

    if ($activate == 1) {
        require_once( ABSPATH . '/wp-admin/includes/theme.php' );

        if (function_exists('wp_get_themes'))
            $themes = wp_get_themes();
        else
            $themes = get_themes();

        foreach ($themes as $theme_slug => $theme) {
            if (function_exists('wp_get_themes')) {
                if ($result['destination_name'] == $theme->Template) {
                    $output = switch_theme($theme->Template);
                }
            } else {
                if ($result['destination_name'] == $theme['Template']) {
                    $output = switch_theme($theme['Template']);
                }
            }
        }
    }

    return array('status' => 'success');
}

/**
 * Delete a theme.
 */
function wpb_delete_theme() {

	$theme = sanitize_text_field(WPB_API_Authenticate::get_arg( 'theme' ));
	
	global $wp_filesystem;

	if ( ! wp_get_theme( $theme )->exists() )
		return array( 'status' => 'error', 'error' => 'Theme is not installed.' );

	include_once ABSPATH . 'wp-admin/includes/admin.php';
	include_once ABSPATH . 'wp-admin/includes/upgrade.php';
	include_once ABSPATH . 'wp-includes/update.php';

	wpb_precheck ('filesystem_access_delete');

	$themes_dir = $wp_filesystem->wp_themes_dir();
	if ( empty( $themes_dir ) )
		return array( 'status' => 'error', 'error' => 'Unable to locate WordPress theme directory.' );

	$themes_dir = trailingslashit( $themes_dir );
	$theme_dir = trailingslashit( $themes_dir . $theme );
	$deleted = $wp_filesystem->delete( $theme_dir, true );

	if ( ! $deleted )
		return array( 'status' => 'error', 'error' => sprintf( 'Could not fully delete the theme: %s.', $theme ) );

	// Force refresh of theme update information
	delete_site_transient('update_themes');

	return array( 'status' => 'success' );
}