<?php

/*
 * Add auto update plugin list
 */

function wpb_add_auto_update_plugin($plugins = array()) {

    $plugin = urldecode(WPB_API_Authenticate::get_arg('slug'));
    $value = WPB_API_Authenticate::get_arg('state');

    $plugins_list = get_option('wpb_automatic_plugin_updates');

    if ($value == 'false') {
        $result = array_diff($plugins_list, array($plugin));
        return add_option('wpb_automatic_plugin_updates', $result) or update_option('wpb_automatic_plugin_updates', $result);
    }

    if (empty($plugins_list)) {
        $plugins[] = $plugin;
    } else {
        if (in_array($plugin, $plugins_list)) {
            return TRUE;
        } else {
            $plugins_list[] = $plugin;
            return add_option('wpb_automatic_plugin_updates', $plugins_list) or update_option('wpb_automatic_plugin_updates', $plugins_list);
        }
    }

    return add_option('wpb_automatic_plugin_updates', $plugins) or update_option('wpb_automatic_plugin_updates', $plugins);
}

/*
 * Auto update plugin list
 */

function wpb_auto_update_plugin_list() {

    $plugins_list = get_option('wpb_automatic_plugin_updates');

    return $plugins_list;
}

/*
 * Add auto update theme list
 */
function wpb_add_auto_update_theme($themes = array()) {

    $theme = urldecode(WPB_API_Authenticate::get_arg('slug'));
    $value = WPB_API_Authenticate::get_arg('state');

    $themes_list = get_option('wpb_automatic_theme_updates');

    if ($value == 'false') {
        $result = array_diff($themes_list, array($theme));
        return add_option('wpb_automatic_theme_updates', $result) or update_option('wpb_automatic_theme_updates', $result);
    }

    if (empty($themes_list)) {
        $themes[] = $theme;
    } else {
        if (in_array($theme, $themes_list)) {
            return TRUE;
        } else {
            $themes_list[] = $theme;
            return add_option('wpb_automatic_theme_updates', $themes_list) or update_option('wpb_automatic_theme_updates', $themes_list);
        }
    }

    return add_option('wpb_automatic_theme_updates', $themes) or update_option('wpb_automatic_theme_updates', $themes);
}

/*
 * Auto update theme list
 */

function wpb_auto_update_theme_list() {

    $themes_list = get_option('wpb_automatic_theme_updates');

    return $themes_list;
}

function wpb_security_scan() {
    $url = home_url();
    $scanUrl = 'http://sitecheck.sucuri.net/scanner/?scan=' . urlencode($url) . '&serialized&clear=true';
    $runScan = wp_remote_get($scanUrl, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking' => true,
        'headers' => array(),
        'body' => null,
        'cookies' => array()
            )
    );
    if (is_wp_error($runScan)) {
        $result = sprintf('Something went wrong. Couldn\'t get Malware results. Details: %s', $runScan->get_error_message());
    } else {
        $runScan = $runScan['body'];
        $result = maybe_unserialize($runScan);
    }
    return $result;
}

function wpb_plugin_updates() {
    include_once ( ABSPATH . 'wp-admin/includes/plugin.php' );
    wp_clean_plugins_cache();
    ob_start();
    wp_update_plugins(); // Check for Plugin updates
    ob_end_clean();

    $update_plugins = get_site_transient('update_plugins');

    if (isset($update_plugins->response)) {
        $available_plugin_updates = array_keys($update_plugins->response);
    } else {
        $available_plugin_updates = array('No update available');
    }

    include_once ( ABSPATH . 'wp-admin/includes/admin.php' );
    include_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );

    // unhook this functions that output things before we send our response header.
    remove_action('upgrader_process_complete', array('Language_Pack_Upgrader', 'async_upgrade'), 20);
    remove_action('upgrader_process_complete', 'wp_version_check');
    remove_action('upgrader_process_complete', 'wp_update_themes');

    $result = false;

    $plugins_list = get_option('wpb_automatic_plugin_updates');

    foreach ($plugins_list as $plugin) {

        if (!in_array($plugin, $available_plugin_updates)) {
            continue;
        }

        // Object created inside the for loop to clean the messages for each plugin
        $skin = new Automatic_Upgrader_Skin();
        // The Automatic_Upgrader_Skin skin shouldn't output anything.
        $upgrader = new Plugin_Upgrader($skin);
        // This avoids the plugin to be deactivated.
        // Using bulk upgrade puts the site into maintenance mode during the upgrades
        $result = $upgrader->bulk_upgrade(array($plugin));
        $is_active = is_plugin_active($plugin);

        // re-activate plugin
        if ($is_active) {
            activate_plugin($plugin, '', false, true);
        }
        
        $res[$plugin] = $upgrader->skin->get_upgrade_messages();
    }

    return $res;
}

function wpb_theme_updates() {

    wp_clean_themes_cache();
    ob_start();
    wp_update_themes(); // Check for Plugin updates
    ob_end_clean();

    $update_themes = get_site_transient('update_themes');

    if (isset($update_themes->response)) {
        $available_theme_updates = array_keys($update_themes->response);
    } else {
        $available_theme_updates = array();
    }

    include_once ( ABSPATH . 'wp-admin/includes/admin.php' );
    include_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );

    // unhook this functions that output things before we send our response header.
    remove_action('upgrader_process_complete', array('Language_Pack_Upgrader', 'async_upgrade'), 20);
    remove_action('upgrader_process_complete', 'wp_version_check');
    remove_action('upgrader_process_complete', 'wp_update_plugins');

    $result = false;

    $themes_list = get_option('wpb_automatic_theme_updates');

    foreach ($themes_list as $theme) {

        if (!in_array($theme, $available_theme_updates)) {
            continue;
        }

        // Object created inside the for loop to clean the messages for each plugin
        $skin = new Automatic_Upgrader_Skin();
        // The Automatic_Upgrader_Skin skin shouldn't output anything.
        $upgrader = new Theme_Upgrader($skin);
        // Using bulk upgrade puts the site into maintenance mode during the upgrades
        $result = $upgrader->bulk_upgrade(array($theme));
        
        $res[$theme] = $upgrader->skin->get_upgrade_messages();
    }

    return $res;
}

/**
 * Get Pages
 */
function wpb_get_pages() {

               $posts_per_page = WPB_API_Authenticate::get_arg( 'posts_per_page' );
               if(empty($posts_per_page))
                {
                  $posts_per_page = -1;
                }
		$args = array(
			'post_type' => 'page',
			'posts_per_page' => $posts_per_page
		);
		$query = new WP_Query( $args );
		
		if( !empty( $query->posts ) ) {
			foreach( $query->posts as $posts ) {
				$user = get_user_by( 'id', $posts->post_author );
				$list_posts[] = array(
					'ID' => $posts->ID,
					'post_author' => $posts->post_author,
					'post_author_name' => $user->display_name,
					'post_date' => $posts->post_date,
					'post_content' => $posts->post_content,
					'post_title' => $posts->post_title,
					'post_status' => $posts->post_status,
					'post_name' => $posts->post_name,
					'post_category' => wp_get_post_categories( $posts->ID ),
					'post_modified' => $posts->post_modified,
					'guid' => $posts->guid,
					'post_type' => $posts->post_type,
					'comment_count' => $posts->comment_count
				);
			}
		$actions = $list_posts;
        } else {
        $actions = array( 'status' => 'error', 'error' => 'Could not find any post in your site' );
        }
		
		return $actions;
}

/**
 * Create Page
 */
function wpb_create_page() {

			$param_keys = array(
				'post_author',
				'post_content',
				'post_status',
				'post_title',
			);
			$args = array();
			foreach( $param_keys as $param_key ) {
				if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
					$args[$param_key] = $value;
			}
			$args['post_type'] = 'page';

			if ( $page_id = wp_insert_post( $args ) ) {
			    $actions = get_post( $page_id );
			}else{
			    $actions = new WP_Error( 'create-page', __( "Error creating page.", 'wpblazer' ) );
			}

		return $actions;
}

/**
 * Update Page
 */
function wpb_update_page() {

			$param_keys = array(
				'post_author',
				'post_content',
				'post_status',
				'post_title',
			);
			$args = array();
			foreach( $param_keys as $param_key ) {
				if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
					$args[$param_key] = $value;
			}
			$args['post_type'] = 'page';
			$args['ID'] = (int)WPB_API_Authenticate::get_arg( 'post_id' );

			if ( ! get_post( $args['ID'] ) ) {
				$actions = new WP_Error( 'missing-page', __( "No page found.", 'wpblazer' ) );
			}

			if ( wp_update_post( $args ) ){
			    $actions = get_post( $args['ID'] );
			}else {
			    $actions = new WP_Error( 'update-page', __( "Error updating page.", 'wpblazer' ) );
			}

		return $actions;
}

/*
 * 
 */
function wpb_get_wp_categories() {
    
    $categories = get_categories(array('hide_empty'=>0));

    return $categories;
}

/*
 * 
 */
function wpb_get_wp_category() {
    
    $cid = (int)WPB_API_Authenticate::get_arg( 'cid' );
    
    return get_term( $cid );
    
}


/*
 * 
 */
function wpb_remove_category() {
    
    $cid = (int)WPB_API_Authenticate::get_arg( 'cid' );
    
    return wp_delete_term( $cid, 'category' );
    
}

/*
 * 
 */
function wpb_remove_tag() {
    
    $cid = (int)WPB_API_Authenticate::get_arg( 'cid' );
    
    return wp_delete_term( $cid, 'post_tag' );
    
}

/*
 * 
 */
function wpb_create_tag() {
    
    require_once ABSPATH . '/wp-admin/includes/taxonomy.php';
    
    $param_keys = array(
            'name',
            'description',
            'slug',
    );
    
    $args = array();
    foreach( $param_keys as $param_key ) {
            if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
                    $args[$param_key] = $value;
    }
    
    $args['taxonomy'] = (int)WPB_API_Authenticate::get_arg( 'post_tag' );
    
    if( $cat_id = wp_insert_term($args['name'], 'post_tag', $args ) ){
        $result = $cat_id;
    } else {
        $result = array( 'status' => 'error', 'error' => 'Tag not created' );  
    }
    
    return $result;
}

/*
 * 
 */
function wpb_update_tag() {
    
    require_once ABSPATH . '/wp-admin/includes/taxonomy.php';
    
    $param_keys = array(
            'cid',
            'name',
            'description',
            'slug',
    );
    
    $args = array();
    foreach( $param_keys as $param_key ) {
            if ( null !== ( $value = WPB_API_Authenticate::get_arg( $param_key ) ) )
                    $args[$param_key] = $value;
    }
    
    $args['taxonomy'] = (int)WPB_API_Authenticate::get_arg( 'post_tag' );
    
    if( $cat_id = wp_update_term($args['cid'], 'post_tag', $args ) ){
        $result = $cat_id;
    } else {
        $result = array( 'status' => 'error', 'error' => 'Tag not updated' );  
    }
    
    return $result;
}

?>