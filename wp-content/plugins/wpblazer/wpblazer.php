<?php
/*
Plugin Name: WP Blazer
Plugin URI: http://wpblazer.com
Description: Manage all your WordPress site with WP Blazer.
Version: 1.0.14
Author: Touch Logic Technologies Pvt Ltd.
Author URI: http://touchlogictech.com/
License: Copyright 2017, All rights reserved
*/

/*
 * Plugin Updater
 */
error_reporting(0);
require dirname( __FILE__ ).'/skin/plugin-update-checker.php';
$MyUpdateChecker = new PluginUpdateChecker_1_5( 'http://app.wpblazer.com/userlist/metadata1.0.php', __FILE__, 'wp-blazer' );

/*
 * Plugin Administration Menu
 *
 * Usage: Create admin menu
 *
 * Register the wpblazer_api_key settings
 */
function wpblazer_admin_menu() {
		add_menu_page( __( 'WP Blazer' ), __( 'WP Blazer' ), 'manage_options', 'wp-blazer', 'wpblazer_add_api_key' ,plugins_url( 'images/wpblazer-icon.ico', __FILE__ ) );
		register_setting( 'wpblazer-settings', 'wpblazer_api_key', 'wpb_activate' );
	}

add_action('admin_menu', 'wpblazer_admin_menu');
	
define( 'WPBLAZER_PLUGIN_SLUG', 'wp-blazer' );
define( 'WPBLAZER_PLUGIN_URL', plugin_dir_path( __FILE__ ) );
define( 'WPBLAZER_ROOT',  rtrim(str_replace("\\", "/", ABSPATH), "/\\") . '/');

if ( ! defined( 'WPBLAZER_URL' ) )
	define( 'WPBLAZER_URL', 'http://wpblazer.com/' );

if ( ! defined( 'WPBLAZER_API_URL' ) )
	define( 'WPBLAZER_API_URL', 'http://app.wpblazer.com/pluginapi' );
	
if (version_compare(phpversion(), '5.2.4', '<')) {

	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	deactivate_plugins( WPBLAZER_PLUGIN_SLUG . '/wpblazer.php' );

	if ( isset( $_GET['action'] ) && ( $_GET['action'] == 'activate' || $_GET['action'] == 'error_scrape' ) )
		die( __( 'WP Blazer plugin requires PHP version 5.2.4 or higher.', 'wp-blazer' ) );

}

require_once( WPBLAZER_PLUGIN_URL . '/admin/wpblazer.php' );

/*
 * API calls and load the API
 */
function wpblazer_api_call() {

	if ( empty( $_POST['wpb_verify_key'] ) )
		return;

	require_once( WPBLAZER_PLUGIN_URL . '/WPB_Plugins.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/WPB_Themes.php' );
    //require_once( WPBLAZER_PLUGIN_URL . '/WPB-DB.php' );
    require_once( WPBLAZER_PLUGIN_URL . '/WPB-S3.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/WPB_Backup.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/back-dir.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/WPB_Restore.php' );
    require_once( WPBLAZER_PLUGIN_URL . '/WPB_Pixel_Management.php' );
    require_once( WPBLAZER_PLUGIN_URL . '/WPB_SSL.php' );
    require_once( WPBLAZER_PLUGIN_URL . '/WPB_Seo.php' );  

	require_once( WPBLAZER_PLUGIN_URL . '/WPB_API_Authenticate.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/WPB_Functions.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/WPB_Services.php' );
	require_once( WPBLAZER_PLUGIN_URL . '/WPB_API.php' );
	
	$plugin = wpb_plugin_updates();
	$themes = wpb_theme_updates();

	exit;

}
add_action( 'init', 'wpblazer_api_call', 100 );
include_once dirname( __FILE__ ) . '/WPB_Seo.php';
register_activation_hook( __FILE__,'wpb_activation');
remove_action('admin_init', 'send_frame_options_header');
remove_action('login_init', 'send_frame_options_header');

add_action( 'transition_post_status', 'wpb_post_publish', 10, 3 );

function wpb_post_publish( $new_status, $old_status, $post ) { 
    require_once( WPBLAZER_PLUGIN_URL . '/WPB_Functions.php' );
    clean_post_cache( $post->ID );
    if ( $old_status!='publish' && $old_status!='trash' && $new_status == 'publish' ) {
        wpb_addToLog($post->ID, $post->post_type, 'Posted', ' Status Changed: '."{$old_status}_to_{$new_status}".'. Autopost requested.'); 
        wpb_ajax_post($post->ID);
    }
}

/*
 * Check stored WPBLAZER API key
 */
function wpbr_get_api_keys() {
	$keys = apply_filters( 'wpblazer_api_keys', get_option( 'wpblazer_api_key' ) );
	if ( ! empty( $keys ) )
		return (array)$keys;
	else
		return array();
}

function wpb_activate($input) {

    $api = $_POST['wpblazer_api_key'];
 
	// data to send in our API request
	$api_params = array(
					'action'=> 'add',
					'api_key' => $api,
					'admin_url' => admin_url()
				);
	// Call the custom API.
	$response = wp_remote_get( add_query_arg( $api_params, WPBLAZER_API_URL ) );
 
	// make sure the response came back okay
	if ( is_wp_error( $response ) )
	return false;
 
	// decode the api data
	$api_data = json_decode( wp_remote_retrieve_body( $response ) );

	return $input;
}

function wpb_deactivate() {

	$api = get_option( 'wpblazer_api_key' );
 
	// data to send in our API request
	$api_params = array(
					'action'=> 'delete',
					'api_key' => $api,
					'admin_url' => admin_url()
				);
	// Call the custom API.
	$response = wp_remote_get( add_query_arg( $api_params, WPBLAZER_API_URL ) );
 
	// make sure the response came back okay
	if ( is_wp_error( $response ) )
	return false;
 
	// decode the api data
	$api_data = json_decode( wp_remote_retrieve_body( $response ) );
	
	delete_option( 'wpblazer_api_key' );
}
register_deactivation_hook(__FILE__, 'wpb_deactivate');
register_uninstall_hook(__FILE__, 'wpb_deactivate');

/*
 * 
 */
function wpb_signature() {
    $verify = $_GET['signature'];
    unset($_GET['signature']);
    $timestamp = $_GET['username'];

    $vars = array();
    $vars["timestamp"] = $timestamp;
    $api_key = wpbr_get_api_keys();
    if (!$api_key)
        return array();

    $hashes = array();
    foreach ($api_key as $key) {
        $hashes[] = hash_hmac('sha256', serialize($vars), $key);
    }
    $hash = $hashes;

    include_once( ABSPATH . 'wp-includes/pluggable.php' );
    if (!in_array($verify, $hash, true)) {
        wp_safe_redirect(wp_login_url());
        exit();
    }

    if ((int) $timestamp > time() + 360 || (int) $timestamp < time() - 360) {
        wp_safe_redirect(wp_login_url());
        exit();
    }
}

function wpb_get_user_info( $user_info = false, $info = 'login' ){

	if( !function_exists('is_user_logged_in') )
		include_once( ABSPATH.'wp-includes/pluggable.php' );	
			
	if($user_info === false)
		return false;
		
	if( strlen( trim( $user_info ) ) == 0)
		return false;
			
			
	global $wp_version;
	if (version_compare($wp_version, '3.2.2', '<=')){
		return get_userdatabylogin( $user_info );
	} else {
		return get_user_by( $info, $user_info );
	}

}

/*
 * WPB Login
 */
function wpb_automatic_login() {

    $where = isset($_GET['wpb_goto']) ? $_GET['wpb_goto'] : false;
    $auto_login = isset($_GET['WPB_login']) ? $_GET['WPB_login'] : 0;

    if (wpb_signature() == '') {
        
        add_option('wpb_timestamp', '') or update_option('wpb_timestamp', '');

        if (!function_exists('is_user_logged_in'))
            include_once( ABSPATH . 'wp-includes/pluggable.php' );

        if ($auto_login && !is_user_logged_in()) {

            $username = get_option('wpb_admin_user');
            
            if(!empty($username)){
                $user = wpb_get_user_info($username);
        		wp_set_current_user($user->ID);
        			
        		wp_cookie_constants();				
        		wp_set_auth_cookie($user->ID);
            } else {
                $user = get_users( 'role=administrator' );
                wp_set_current_user( $user[0]->ID );

                wp_cookie_constants();
                wp_set_auth_cookie( $user[0]->ID );
            }

            if (function_exists('wp_safe_redirect') && function_exists('admin_url')) {
                wp_safe_redirect(admin_url($where));
                exit();
            }
        } elseif (is_user_logged_in()) {
            if (isset($where)) {
                if (function_exists('wp_safe_redirect') && function_exists('admin_url')) {
                    wp_safe_redirect(admin_url($where));
                    exit();
                }
            }
        }
    }
}

function wpblazer_fbpixel_init() {
    $wpb_fbpixel_id = get_option('wpb_fbpixel_id');
    $plugin_name = 'WpBlazer';
    if (!empty($wpb_fbpixel_id)) { ?>
        <!-- WpBlazer Facebook Pixel Integration Begin -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo "$wpb_fbpixel_id"; ?>', {}, {
                "agent": "<?php echo "$plugin_name"; ?>"
            });
            fbq('track', 'PageView', {
                "source": "<?php echo "$plugin_name"; ?>",
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo "$wpb_fbpixel_id"; ?>&ev=PageView&noscript=1"/></noscript>
        <!-- WpBlazer Facebook Pixel Integration End -->
        <?php
    }
}

add_action('wp_head', 'wpblazer_fbpixel_init');

function wpblazer_pixel_script_init() {
    $wpb_otherpixel_script = get_option('wpb_otherpixel_script');
    if (!empty($wpb_otherpixel_script)) {
        echo "$wpb_otherpixel_script";
    }
}

add_action('wp_head', 'wpblazer_pixel_script_init');

function wpb_ssl_redirect() {
    $wpb_cf_email = get_option('wpb_cf_email');
    $wpb_cf_apikey = get_option('wpb_cf_apikey');
    $wpb_cf_zoneid = get_option('wpb_cf_zoneid');
    $wpb_ssl_mode = get_option('wpb_ssl_mode');

    if (!empty($wpb_cf_email) && !empty($wpb_cf_apikey) && !empty($wpb_cf_zoneid) && !empty($wpb_ssl_mode)) {
        require_once( WPBLAZER_PLUGIN_URL . '/WPB_SSL.php' );
        $wpb_ssl_management = new wpb_ssl($wpb_cf_email, $wpb_cf_apikey, $wpb_cf_zoneid, $wpb_ssl_mode);
        $custom_ssl = $wpb_ssl_management->run();
        return;
    } else {
        return false;
    }
}

add_action('template_redirect', 'wpb_ssl_redirect', 1);

/*
 *
 */
if (isset($_GET['WPB_login']))
    $actions = wpb_automatic_login();	

/*
 * 
 */