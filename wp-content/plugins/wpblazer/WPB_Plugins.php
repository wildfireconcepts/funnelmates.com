<?php
/**
 * Get all plugins
 */
function wpb_get_plugin() {

	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	
	$plugins = get_plugins();	
 
	return $plugins;

}

/**
 * Get all update plugins
 */
function wpb_get_plugins() {

	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	
	$plugins = get_plugins();
	
	@wp_update_plugins();
	
	$current = get_site_transient( 'update_plugins' );

	if( function_exists( 'get_site_transient' ) && $transient = $current )
		$current = $transient;

	elseif( $transient = get_transient( 'update_plugins' ) )
		$current = $transient;

	else
		$current = get_option( 'update_plugins' );
	
	foreach ($plugins as $plugin_path => $plugin) {

		if ( isset( $current->response[$plugin_path] ) ) {
			$plugins[$plugin_path]['latest_version'] = $current->response[$plugin_path]->new_version;
		} else {
			$plugins[$plugin_path]['latest_version'] = $plugin['Version'];
		}
		
		//Check if plugin active		
		if ( is_plugin_active( $plugin_path ) )
	    	$plugins[$plugin_path]['active'] = true;
	    else
	    	$plugins[$plugin_path]['active'] = false;
    }
	return $plugins;
}

/**
 * Update a plugin
 */
function wpb_update_plugin() {

	$plugin = sanitize_text_field(WPB_API_Authenticate::get_arg( 'plugin' ));

	include_once ( ABSPATH . 'wp-admin/includes/admin.php' );
	require_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
	require_once WPBLAZER_PLUGIN_URL . 'skin/wpblazer-plugin-upgrader-skin.php';

	wpb_precheck ('upgrade_check');

	// check filesystem access
	wpb_precheck ('filesystem_access');

	$upgrader_skin = new WPBLAZER_Plugin_Upgrader_Skin();
	$upgrader = new Plugin_Upgrader( $upgrader_skin );
	$is_active = is_plugin_active( $plugin );

	wp_update_plugins();

	// Do the upgrade
	ob_start();
	$output = $upgrader->upgrade( $plugin );
	$data = ob_get_contents();
	ob_clean();

	if ( ( ! $output && ! is_null( $output ) ) || $data )
		$output = array( 'status' => 'error', 'error' => 'file_permissions_error' );

	elseif ( is_wp_error( $output ) )
		$output = array( 'status' => 'error', 'error' => $output->get_error_code() );

	elseif ( $skin->error )
		$output = array( 'status' => 'error', 'error' => $skin->error );
		
	else
		$output = array( 'status' => 'success' );

	// re-activate plugin
	if ( $is_active )
		activate_plugin( $plugin, '', false, true );

	return $output;
}

/**
 * Install plugin
 */
function wpb_install_plugin() {

    $plugin = sanitize_text_field(WPB_API_Authenticate::get_arg('plugin_url'));
    $activate = sanitize_text_field(WPB_API_Authenticate::get_arg('activate'));

    include_once ABSPATH . 'wp-admin/includes/admin.php';
    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    include_once ABSPATH . 'wp-includes/update.php';
    require_once ( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
    require_once WPBLAZER_PLUGIN_URL . 'skin/wpblazer-plugin-upgrader-skin.php';

    $skin = new WPBLAZER_Plugin_Upgrader_Skin();
    $upgrader = new Plugin_Upgrader($skin);

    $result = @$upgrader->run(
                    array(
                        'package' => $plugin,
                        'destination' => WP_PLUGIN_DIR,
                        'clear_destination' => true, //Do not overwrite files.
                        'clear_working' => true,
                        'hook_extra' => array(),
                    )
    );

    if (is_wp_error($result))
        return array('status' => 'error', 'error' => $result->get_error_code());
    else if (!$result)
        return array('status' => 'error', 'error' => 'Unknown error installing plugin.');

    if ($activate == 1) {
        include_once ABSPATH . 'wp-admin/includes/plugin.php';
        $plugins = get_plugins();
        foreach ($plugins as $plugin_slug => $plugin) {
            $plugin_dir = preg_split('/\//', $plugin_slug);
            if ($result['destination_name'] == $plugin_dir[0]) {
                $output = activate_plugin($plugin_slug);
            }
        }
    }

    return array('status' => 'success');
}

/**
 * Activate plugin
 */

function wpb_activate_plugin() {

	$plugin = sanitize_text_field(WPB_API_Authenticate::get_arg( 'plugin' ));

	include_once ABSPATH . 'wp-admin/includes/plugin.php';

	$output = activate_plugin( $plugin );

	if ( is_wp_error( $output ) )
		$output = $output;
	else
	    $output = array( 'status' => 'success' );

	return $output;
}

/**
 * Deactivate plugin
 */
function wpb_deactivate_plugin() {

	$plugin = sanitize_text_field(WPB_API_Authenticate::get_arg( 'plugin' ));

	include_once ABSPATH . 'wp-admin/includes/plugin.php';
	
	if ( is_plugin_active( $plugin ) )
		$output = deactivate_plugins( $plugin );

	if ( is_wp_error( $output ) )
		$output = $output;
	else
	    $output = array( 'status' => 'success' );

	return $output;
}

/**
 * Delete plugin
 */
function wpb_delete_plugin() {

	$plugin = sanitize_text_field(WPB_API_Authenticate::get_arg( 'plugin' ));
	
	global $wp_filesystem;

	include_once ABSPATH . 'wp-admin/includes/admin.php';
	include_once ABSPATH . 'wp-admin/includes/upgrade.php';
	include_once ABSPATH . 'wp-includes/update.php';

	wpb_precheck ('filesystem_access_delete');

	//Get the base plugin folder
	$plugins_dir = $wp_filesystem->wp_plugins_dir();
	if ( empty( $plugins_dir ) )
		return array( 'status' => 'error', 'error' => 'Unable to locate WordPress Plugin directory.' );

	$plugins_dir = trailingslashit( $plugins_dir );

	// Run Uninstall hook
	if ( is_uninstallable_plugin( $plugin ) )
		uninstall_plugin( $plugin );

	$this_plugin_dir = trailingslashit( dirname( $plugins_dir . $plugin ) );
	// If plugin is in its own directory, recursively delete the directory.
	if ( strpos( $plugin, '/' ) && $this_plugin_dir != $plugins_dir ) //base check on if plugin includes directory separator AND that it's not the root plugin folder
		$deleted = $wp_filesystem->delete( $this_plugin_dir, true );
	else
		$deleted = $wp_filesystem->delete( $plugins_dir . $plugin );
		
	if ( ! $deleted ) {
		return array( 'status' => 'error', 'error' => 'Plugin uninstalled, but not deleted.' );
	} else {
		if ( $current = get_site_transient('update_plugins') ) {
			unset( $current->response[$plugin] );
			set_site_transient('update_plugins', $current);
		}
		return array( 'status' => 'success' );
	}

}

/**
 * Delete plugin
 */
function wpb_uninstall_plugin() {

	$plugin = sanitize_text_field(WPB_API_Authenticate::get_arg( 'plugin' ));
	
	global $wp_filesystem;

	include_once ABSPATH . 'wp-admin/includes/admin.php';
	include_once ABSPATH . 'wp-admin/includes/upgrade.php';
	include_once ABSPATH . 'wp-includes/update.php';

	wpb_precheck ('filesystem_access_delete');

	//Get the base plugin folder
	$plugins_dir = $wp_filesystem->wp_plugins_dir();
	if ( empty( $plugins_dir ) )
		return array( 'status' => 'error', 'error' => 'Unable to locate WordPress Plugin directory.' );

	$plugins_dir = trailingslashit( $plugins_dir );

	// Run Uninstall hook
	if ( is_uninstallable_plugin( $plugin ) )
		uninstall_plugin( $plugin );

	$this_plugin_dir = trailingslashit( dirname( $plugins_dir . $plugin ) );
	// If plugin is in its own directory, recursively delete the directory.
	if ( strpos( $plugin, '/' ) && $this_plugin_dir != $plugins_dir ) //base check on if plugin includes directory separator AND that it's not the root plugin folder
		$deleted = $wp_filesystem->delete( $this_plugin_dir, true );
	else
		$deleted = $wp_filesystem->delete( $plugins_dir . $plugin );
		
	if ( ! $deleted ) {
		return array( 'status' => 'error', 'error' => 'Plugin uninstalled, but not deleted.' );
	} else {
		if ( $current = get_site_transient('update_plugins') ) {
			unset( $current->response[$plugin] );
			set_site_transient('update_plugins', $current);
		}
		return array( 'status' => 'success' );
	}

}