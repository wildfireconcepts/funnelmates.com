<?php
/**
 * The template for displaying the footer
 */
?>

            <?php
            /**
             * Action 'woffice_main_container_end'
             *
             * Used to output content within the #main-content div
             *
             */
            do_action('woffice_main_container_end');
            ?>
			
			</section>
			<!-- END CONTENT -->

		<!-- JAVSCRIPTS BELOW AND FILES LOADED BY WORDPRESS -->
		<?php custom_external_scripts('footer'); ?>
		<?php wp_footer(); ?>

	</body>
	<!-- END BODY -->
</html>