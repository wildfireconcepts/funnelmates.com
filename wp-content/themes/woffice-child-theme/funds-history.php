<?php
	global $wpdb;
	$transaction_history = $wpdb->get_results("SELECT * FROM wp_setc_pks_main_balance WHERE user=".get_current_user_id()." AND admin_share='0' ORDER BY id DESC");			   
	$count_transaction_history = $wpdb->num_rows;
	/*$transaction_history1 = $wpdb->get_results("SELECT * FROM wp_setc_pks_balance WHERE user_id =".get_current_user_id()." ORDER BY id DESC");
	$count_transaction1_history = $wpdb->num_rows;
	$total_count = $count_transaction_history + $count_transaction1_history; */
	$total_count = $count_transaction_history; 
	$limit = 100;
	$total_pages = ceil($total_count / $limit); 
	$transaction_history = array_merge($transaction_history, $transaction_history1);
	usort($transaction_history);
?>
<style>
table#table_transaction td {
	padding: 10px 0px;
	border-bottom: 1px solid #EEE;
}
.animation{
	animation-name: example;
	animation-duration: 3s;
}	
@keyframes example {
  from {background-color: #c4bdbd;}
  to {background-color: #f1e8e8;}
}

@media only screen and (min-width: 320px) and (max-width: 479px){
	/*body{width:100% !important;}
	#page-wrapper{width:100%;}
	.inner_funds_first{width:100% !important;}
	#main-content.full-width #left-content, #main-content.sidebar-hidden #left-content, body.sidebar-hidden #left-content {width:88% !important;}*/
}
</style>
<div id="buy_sell" style="overflow-x:auto;"></div>
<div class="clearfix">              
	<ul class="pagination" style="display:none;">
	<?php 
	if(!empty($total_pages)){
		for($i=1; $i<=$total_pages; $i++){
				if($i == 1){
					?>
				<li><span id="page_<?php echo $i;?>" class="page-link" style="color:#000;" onclick="get_sell_purchas_data(<?php echo $i; ?>);"><?php echo $i;?></span></li>
											
				<?php 
				}
				else{
					?>
				<li><span id="page_<?php echo $i;?>" class="page-link" style="color:#000;" onclick="get_sell_purchas_data(<?php echo $i; ?>);" ><?php echo $i;?></span></li>
				<?php
				}
		}
	}
	?>
	</ul>
</div>
<script>
jQuery(document).ready(function($) {
	get_sell_purchas_data(1);
	/*$(".page-link").click(function(){
		var id = $(this).attr("data-id");
		var limit = <?php echo $limit; ?>;
		alert(limit);
		var select_id = $(this).parent().attr("id");
		$.ajax({
			type:'POST',
			data: {
				"action" : "get_funnel_buy_purchase_data",
				"id" : id,
				"limit" : limit
			},
			url: "<?php echo admin_url('admin-ajax.php'); ?>",
			success: function( data ){
				alert(data);
				jQuery('#buy_sell').html(data);
				/*jQuery('#load_funnel').hide();
				//alert(data);
				var obj = jQuery.parseJSON(data);
				if(obj.html == ''){
					jQuery('#msg').html("<p id='alertFadeOut'>No more post</p>");
					jQuery('#alertFadeOut').fadeOut(10000, function () {
					  jQuery('#alertFadeOut').text('');
					});
				}
				if(page >= 1){
					jQuery('#store_div').append(obj.html);
				}else if(page == 'lastpage'){
					
				}else{
					jQuery('#store_div').html(obj.html);
				}
				jQuery('#page_id').html(obj.pagenav); *//*
			}
		});
	});*/
});

function get_sell_purchas_data(id){
	var id = id;
	var limit = <?php echo $limit; ?>;
	var select_id = jQuery(this).parent().attr("id");
	jQuery(".page-link").css('background-color', '#FFF');
	jQuery("#page_"+id).addClass("animation");
	jQuery.ajax({
		type:'POST',
		data: {
			"action" : "get_funnel_buy_purchase_data",
			"id" : id,
			"limit" : limit
		},
		url: "<?php echo admin_url('admin-ajax.php'); ?>",
		success: function( data ){
			jQuery('#buy_sell').html(data);
			jQuery("#page_"+id).removeClass("animation");
			jQuery("#page_"+id).css('background-color', '#e5e5e5');
			jQuery(".pagination").show();
		}
	});
}
</script>