<?php
function register_custom_event_type() {
$labels = array(
    'name' => _x('Events', 'event'),
    'singular_name' => _x('Event', 'event'),
    'add_new' => _x('Add New', 'event'),
    'add_new_item' => _x('Add New Event', 'event'),
    'edit_item' => _x('Edit Event', 'event'),
    'new_item' => _x('New Event', 'event'),
    'view_item' => _x('View Event', 'event'),
    'search_items' => _x('Search Events', 'event'),
    'not_found' => _x('No events found', 'event'),
    'not_found_in_trash' => _x('No events found in Trash', 'event'),
    'parent_item_colon' => _x('Parent Event:', 'event'),
    'menu_name' => _x('Events', 'event'),
);

$args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'author'),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'event',
    'capabilities' => array(
            'read_post' => 'read_event',
            'publish_posts' => 'publish_events',
            'edit_posts' => 'edit_events',
            'edit_others_posts' => 'edit_others_events',
            'delete_posts' => 'delete_events',
            'delete_others_posts' => 'delete_others_events',
            'read_private_posts' => 'read_private_events',
            'edit_post' => 'edit_event',
            'delete_post' => 'delete_event',

        ),
    'map_meta_cap' => true
);
register_post_type('event', $args);
}
add_action('init', 'register_custom_event_type');