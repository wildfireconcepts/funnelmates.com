<?php
class funnelmates_subscribe(){
	function __construct(){
		$this->wpdb = $GLOBALS['wpdb'];
	}
	function go_to_webinar_subscribe($user_id,$email){
		$rs = $this->go_to_webinar_refresh_token($user_id);							
		$organizer_key = $rs->organizer_key;
		$url_webinar = "https://api.getgo.com/G2W/rest/v2/organizers/{$organizer_key}/webinars/{$webinar_ar_integration}/registrants";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_webinar);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		$param_array = array(
			"firstName" => "Avesh",
			"lastName" => "Chauhan",
			"email" => $email,
			"source" => 'FunnelMates'
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param_array));
		$headers = array();
		$headers[] = 'Authorization: Bearer '.$rs->access_token;
		$headers[] = 'Accept: application/vnd.citrix.g2wapi-v1.1+json';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		$rs = json_decode($result);
		return $rs;
	}
	function go_to_webinar_refresh_token($user_id){
		$fm_gotowebinar = get_user_meta($user_id,'fm_gotowebinar',true);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.getgo.com/oauth/v2/token');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=".$fm_gotowebinar->refresh_token);

		$headers = array();
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Authorization: Basic '.base64_encode('d92b304a-9344-4cb4-a012-56947c8151ff:UcRhLAxv81il8dp2s8mxHrSN');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			return 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		$rs = json_decode($result);
		update_user_meta($user_id,'fm_gotowebinar', $rs);
		return $rs;
	}
}