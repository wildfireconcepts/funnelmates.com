<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */

get_header('landing'); 
/*
$url = $_SERVER[REQUEST_URI]; 
$word = "id=shot";
if(strpos($url, $word) !== false){
	session_start();
	$_SESSION["shot"] = "shot"; 
	$url = str_replace( $word,"", $url);
	header("location:$url");
	
}else{
	
} */
//echo $_POST['hid']; exit;
?>



	<div id="left-content">

		<?php  //GET THEME HEADER CONTENT

		//woffice_title(get_the_title()); ?> 	
			
		<?php // Start the Loop.
		while ( have_posts() ) : the_post(); ?>

		<!-- START THE CONTENT CONTAINER -->
		<div id="content-container">
			<span id="funnelid" style="display:none;"><?php echo get_the_ID(); ?></span>
			<!-- START CONTENT -->
			<div id="content">
				<?php 
				if (woffice_is_user_allowed()) {
					get_template_part( 'content', 'page' );
					
					$page_comments = woffice_get_settings_option('page_comments');
					// If comments are open or we have at least one comment, load up the comment template.
					if ( $page_comments == "show"){
						if ( comments_open() || get_comments_number()) {
							comments_template();
						}
					}
				}
				else { 
					get_template_part( 'content', 'private' );
				}
				?>
			</div>
				
		</div><!-- END #content-container -->
		
		<?php //woffice_scroll_top(); ?>

	</div><!-- END #left-content -->

<?php // END THE LOOP 
endwhile; ?>

<?php 
get_footer('landing');
