<style>
.agency_form_div{text-align:center; margin:20px 0;}
.agency_btn_submit{background-color:green; padding:10px 20px; border:2px solid #FFFFFF; border-radius:20px; color:#FFFFFF; font-weight:700; cursor:pointer;}
.radioerror{background-color:red;}
#agency_error_msj{min-height:40px;}
.checkbox_div{margin:20px 0;}
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 15px;
}
</style>
<div class="agency_form_div row">
	<?php 
	global $wpdb;
	$c_user_id = get_current_user_id();
	$account_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_agency_account_history` where creator_id = $c_user_id AND `date` BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()");
	$havemeta = get_user_meta($c_user_id,"is_agency_limited", true);
	// $unlimited = 0;
	if(!$havemeta || $havemeta == ''){
		$unlimited = 1;
	}else{
		$unlimited = 0;
	}
	if(($account_count <= 10 && get_user_meta($c_user_id,"is_agency_limited") == '1') || $unlimited == 1){ ?>
	<div style="width:25%;"></div>
	<div style="width:50%;">
		<div id="agency_error_msj"></div>
		<input type="text" name="agency_first_name" id="agency_first_name" placeholder="First Name" class="">					
		<input type="text" name="agency_last_name" id="agency_last_name" placeholder="Last Name" class="">							
		<input type="email" name="agency_user_email" id="agency_user_email" placeholder="Email" class="">
		<input type="hidden" name="current_user_id" id="current_user_id" value="<?php echo get_current_user_id(); ?>">
		<div class="checkbox_div">Transfer Funnels (Optional) <input type="checkbox" name="agency_chek" id="agency_chek"></div>
			<select name="author_funnels" id="agency_author_funnels" style="display:none" class="funnel_select" multiple>
			<option value="select_funnel" selected>Select Funnel</option>
			<?php
			// echo "SELECT * FROM `wp_posts` INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id INNER JOIN wp_fm_marketplace_items on wp_posts.ID = wp_fm_marketplace_items.post_id WHERE (wp_posts.post_author = $c_user_id and wp_posts.post_type = 'landing') and wp_posts.post_status = 'publish' and (wp_postmeta.meta_key = 'is_funnel_published' and wp_postmeta.meta_value = 1) and wp_posts.ID = wp_fm_marketplace_items.post_id";
			$author_funnels_id = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id INNER JOIN wp_fm_marketplace_items on wp_posts.ID = wp_fm_marketplace_items.post_id WHERE (wp_posts.post_author = $c_user_id and wp_posts.post_type = 'landing') and wp_posts.post_status = 'publish' and (wp_postmeta.meta_key = 'is_funnel_published' and wp_postmeta.meta_value = 1) and wp_posts.ID = wp_fm_marketplace_items.post_id ORDER BY wp_posts.ID DESC");
			foreach($author_funnels_id as $autho_f){
				$f_id = $autho_f->ID;
				$f_title = $autho_f->post_title;
				$pricing = maybe_unserialize($autho_f->pricing);
				foreach($pricing as $k => $p){
					if($k == 'free' || $k == 'premium'){
						$price = "done";
					}else{
						$price = "";
					}
				}
				if($price){
				?>
					<option value="<?php echo $f_id;?>"><?php echo $f_title; ?></option>
				<?php
				}
			}
			/*
			$author_funnels = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id WHERE (wp_posts.post_author = $c_user_id and wp_posts.post_type = 'landing') and wp_posts.post_status = 'publish' and (wp_postmeta.meta_key = 'is_funnel_published' and wp_postmeta.meta_value = 1)");
			foreach($author_funnels as $autho_f){
			$f_id = $autho_f->ID;
			$f_title = $autho_f->post_title;
			?>
			<option value="<?php echo $f_id;?>"><?php echo $f_title; ?></option>
			<?php
			}*/
			?>
			</select>
		<div>
			<span class="agency_btn_submit" >Create Account</span>
			<p id="load_agency" style="display:none;"><img src="https://funnelmates.com/wp-content/uploads/screenshot/loading.gif" style="width:20% !important;" class="loader"></p>
		</div>
	</div>
	<div style="width:25%;"></div>
	<?php }else{ ?>
		<h1>You've Crossed limit of creating account for this month</h1>
	<?php } ?>
	<?php
	$get_funnel_creation_data = $wpdb->get_results("SELECT * FROM `wp_setc_agency_account_history` where creator_id = $c_user_id");
	if($get_funnel_creation_data){
	?>
	<div style="margin-top:20px; width:100%;">
		<h3 style="text-align:center;">Account Creation History</h3>
		<table>
		  <tr>
			<th>Name</th>
			<th>Email</th>
			<th>Date Created</th>
			<th>Funnel</th>
		  </tr>
		  <?php
		  foreach($get_funnel_creation_data as $data){
			  $name = $data->user_name;
			  $email = $data->email;
			  $date = $data->date;
			  $funnel = $data->funnels_id;
			  $fun_id =  maybe_unserialize($funnel);
		  ?>
		  <tr>
			<td><?php echo $name; ?></td>
			<td><?php echo $email; ?></td>
			<td><?php echo $date; ?></td>
			<td style="width:30%;">
				<?php 
				if($funnel != '' && $funnel != 'none'){
					foreach($fun_id as $fid){
						echo get_the_title($fid);
						echo ", ";
					}
				}else{
					echo "None";
				}
				?>
			</td>
		  </tr>
		  <?php
		  }
		  ?>
		</table>
	</div>
	<?php
		}
	?>
</div>