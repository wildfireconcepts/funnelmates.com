<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */
get_header();
define('LIMIT','9');
?>

	<div id="left-content" class="category_page ">

		<?php  //GET THEME HEADER CONTENT
		$title = sprintf( __( 'Category Archives: <span>%s</span>', 'woffice' ), single_cat_title( '', false ));
		woffice_title($title); ?> 	

		<!-- START THE CONTENT CONTAINER -->
		<div id="content-container">

		<div id="content" class=""><!-- START CONTENT -->
			

                <?php
				
				$category = get_queried_object();
				$cur_cat = $category->term_id;
				
                $blog_layout = 'classic';
                $content_type = 'content';
                if(get_post_type() == 'post'){

                    $blog_layout = woffice_get_settings_option('blog_layout');
	                $blog_layout = (isset($_GET['blog_masonry'])) ? 'masonry' : $blog_layout;

	                $masonry_columns = woffice_get_settings_option('masonry_columns');
	                $masonry_columns_class = 'masonry-layout--'.$masonry_columns.'-columns';

                    echo ($blog_layout === "masonry") ? '<div id="directory" class="masonry-layout '.$masonry_columns_class.'">' : '';
	                $content_type = ($blog_layout == "masonry") ? 'content-masonry' : 'content';
                }
                ?>

                <?php
                /**
                 * Reset the query before displaying the post
                 * A loop must be unclosed before this call
                 * See: WOF-161
                 */
                // wp_reset_query(); ?>

				<?php /*if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php // We check for the role : 
						if (woffice_is_user_allowed()) { ?>
							<?php get_template_part( $content_type ); ?>
						<?php } ?>
					<?php endwhile; ?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>

                <?php echo ($blog_layout == "masonry") ? '</div>' : ''; ?>

				<!-- THE NAVIGATION --> 
				<?php woffice_paging_nav();*/ 
				/*
				global $wpdb;
				$items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items ORDER BY id DESC");
				if(count($items) > 0){
					?>
					<article id="post-" class="box content category_div" style="display:inline-block; ">
						<?php
						foreach($items as $i){
							$post_id = $i->post_id;
							$cat = get_the_category($post_id);
							// print_r($cat[0]);
							if($cur_cat == $cat[0]->term_id){
							$pricing = maybe_unserialize($i->pricing);
							$funnel = get_post($post_id);
							$count_price = count($pricing);
							?>
							<div class="marketplace_item <?php echo $post_id; ?>" >
								<div class="category_item_inner">
								  <div class="intern-thumbnail" style="">
										<a href="">
											<picture>
												<source srcset="https://funnelmates.com/wp-content/uploads/2015/06/photodune-2191223-university-of-tennessee-hill-m.jpg" media="(min-width: 1920px)"><img src="https://funnelmates.com/wp-content/uploads/2015/06/photodune-2191223-university-of-tennessee-hill-m-800x600.jpg">
											</picture>
										</a>
									</div>
									<?php
									$freecolor = "background-color:#f37021;color:#fff;border-radius: 4px 0 0 0;";
									$premiumcolor = "background-color:#446095;color:#fff;";
									$whitelabelcolor = "background-color:#a8519f;color:#fff;";
									$exclusivecolor = "background-color:#f4ea00;color:#000;border-radius: 0 4px 0 0;";
									foreach($pricing as $k => $p){
										if($count_price == 4){
											$width = "25%";
										}else if($count_price == 3){
											$width = "33.33%";
										}else if($count_price == 2){
											$width = "50%";
										}else{
											$width = "100%";
										}
										if($k == 'free'){
											echo '<div style="width:'.$width.';float:left;text-align:center;'.$freecolor.'">'.ucfirst($k).'</div>';
										}else if($k == 'premium'){
											echo '<div style="width:'.$width.';float:left;text-align:center;'.$premiumcolor.'">'.ucfirst($k).'</div>';
										}else if($k == 'whitelabel'){
											echo '<div style="width:'.$width.';float:left;text-align:center;'.$whitelabelcolor.'">'.ucfirst($k).'</div>';
										}else if($k == 'exclusive'){
											echo '<div style="width:'.$width.';float:left;text-align:center;'.$exclusivecolor.'">'.ucfirst($k).'</div>';
										}
									}
									echo '<div class="funnel_details">';
										echo '<h5 style="text-align: left;">'.$funnel->post_title.'</h5>';
										echo '<div class="funnel_details_inner">';
											echo '<div style="width:100%;float:left;font-size: 12px;">';
												echo '<table>';
													echo '<tr>';
														echo '<td>Created By: <a href="'.get_author_posts_url($funnel->post_author).'">'.get_the_author_meta('first_name',$funnel->post_author).'</a></td>';
													echo '</tr>';
													echo '<tr>';
														$cat = get_the_category($post_id);
														echo '<td>Category: <a href="'.get_category_link($cat[0]).'">'.$cat[0]->name.'</a></td>';
													echo '</tr>';
													echo '<tr>';
														echo '<td>Funnel Rating:</td>';
													echo '</tr>';
													echo '<tr>';
														echo '<td>Activate Users:0</td>';
													echo '</tr>';
													echo '<tr>';
														echo '<td>Affiliate Products:0</td>';
													echo '</tr>';
													echo '<tr>';
														$pm_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_fm_promotional_tools` WHERE post_id=".$post_id);
														if($pm_count == 0){
															$pt = 'No';
														}else{
															$pt = 'Yes';
														}
														echo '<td>Promo Tools: '.$pt.'</td>';
													echo '</tr>';
												echo '</table>';
											echo '</div>';
										echo '</div>';
										echo '<div class="marketplace_item_desc">';
											echo '<p style="text-align: left;margin: 10px 0;font-size: 15px;">'.$i->description.'</p>';
											echo '<button onclick="unlock_funnel('.$i->post_id.');" type="button" style="/*background-color:#82b440;padding: 5px 30px;border-radius: 20px;*//*" class="btn btn-default"><i class="fa fa-arrow-right"></i>Unlock this Funnel</button>';
										echo '</div>';
									echo '</div>';
									?>
								</div>
							</div>
							<?php
							if($i%3==0){echo '<div style="clear:both;"></div>';}
							$i++;
							}
						}
						?>
					</article>
				<?php }else{ ?>
					<article id="post-" class="box content">

						<div class="intern-padding">
							<div class="special-404 text-center">
								<i class="fa fa-meh text-light"></i>
							</div>
							<div class="heading text-center">
								<h2>
									Nothing Found				</h2>
							</div>
						</div>
						<div class="intern-padding">
							<p class="blog-sum-up text-center">
								Apologies, but no results were found. Perhaps searching will help find a related post.			</p>

							<div class="blog-button text-center">
								<a href="https://funnelmates.com" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back on the home page</a>
							</div>
						</div>
					</article>
				<?php }
				*/
				?>
				<article id="post-" class="box content category_div" style="display:inline-block; ">
				</article>
				<div id="page_id"></div>
				<span id="load_funnel" style="display:none;"><img src="https://funnelmates.com/wp-content/uploads/screenshot/loading.gif" style="" class="loader"></span>
				<div id="msg"></div>						
			</div>
				
		</div><!-- END #content-container -->
		
		<?php woffice_scroll_top(); ?>

	</div><!-- END #left-content -->
<script>
jQuery( document ).ready(function($) {
	get_category();
	/*var h = $('#content-container').height();
	alert(h);*/
	var position = $(window).scrollTop(); 
    $(window).on('scroll', function() {
		var scroll = $(window).scrollTop();
		if(scroll > position) {
			var h = $('#content-container').height();
			var y_scroll_pos = window.pageYOffset;
			if(y_scroll_pos > h) {
				get_category('next');
			}
		}else {
			
		}
		position = scroll;
    });
	/*var element_position = $('.category_div').offset().top;
	$(window).on('scroll', function() {
		var y_scroll_pos = window.pageYOffset;
		var scroll_pos_test = element_position;
		if(y_scroll_pos > scroll_pos_test) {
			//get_category('next');
		}
	}); */
});

function get_category(){
	var limit = <?php echo LIMIT;?>;
	var curcat = <?php echo $cur_cat;?>;
	var page = jQuery("#page").val();
	
	if(page == 'lastpage'){
		return false;
	}
	
	if(page != null){
		jQuery('#load_funnel').show();
	}else{		
	}
	
	jQuery.ajax({
		type:'POST',
		data: {
			"action" : "get_category_post",
			"limit" : limit,
			"page" : page,
			"cur_cat" : curcat
		},
		url: "<?php echo admin_url('admin-ajax.php'); ?>",
		success: function( data ){
			jQuery('#load_funnel').hide();
			var obj = jQuery.parseJSON(data);
			if(obj.html == ''){
				jQuery('#msg').html("<p id='alertFadeOut'>No more post</p>");
				jQuery('#alertFadeOut').fadeOut(10000, function () {
				  jQuery('#alertFadeOut').text('');
				});
			}
			if(page >= 1){
				jQuery('#post-').append(obj.html);
			}else if(page == 'lastpage'){
				
			}else{
				jQuery('#post-').html(obj.html);
			}
			jQuery('#page_id').html(obj.pagenav);
		}
	});
}
</script>
<?php 
get_footer();
