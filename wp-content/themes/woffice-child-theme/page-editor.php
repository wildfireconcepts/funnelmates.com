<?php
	get_header();
?>
<div style="width:100%;padding:0 2px;" id="test">
<?php
$editor_id_immediate = 'immediate_123';
$settings_immediate =   array(
	'wpautop' => true, // use wpautop?
	'media_buttons' => false, // show insert/upload button(s)
	'textarea_name' => $editor_id_immediate, // set the textarea name to something different, square brackets [] can be used here
	'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
	'tabindex' => '',
	'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
	'editor_class' => '', // add extra class(es) to the editor textarea
	'teeny' => false, // output the minimal editor config used in Press This
	'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
	'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

	'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
);
wp_editor( $data->content, $editor_id_immediate, $settings_immediate );
/*
<!DOCTYPE html>
<html>
<head>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script type='text/javascript' src='https://funnelmates.com/wp-content/themes/woffice-child-theme/scripts/tiny.js' id='child-tiny-script-js'></script>
  <script>	
 	/*tinymce.init({
	  selector: 'textarea#custom-toolbar-button',
	  toolbar: 'customInsertButton customDateButton',
	  setup: function (editor) {

		editor.ui.registry.addButton('customInsertButton', {
		  text: 'My Button',
		  onAction: function (_) {
			editor.insertContent('&nbsp;<strong>It\'s my button!</strong>&nbsp;');
		  }
		});

		var toTimeHtml = function (date) {
		  return '<time datetime="' + date.toString() + '">' + date.toDateString() + '</time>';
		};

		editor.ui.registry.addButton('customDateButton', {
		  icon: 'insert-time',
		  tooltip: 'Insert Current Date',
		  disabled: true,
		  onAction: function (_) {
			editor.insertContent(toTimeHtml(new Date()));
		  },
		  onSetup: function (buttonApi) {
			var editorEventCallback = function (eventApi) {
			  buttonApi.setDisabled(eventApi.element.nodeName.toLowerCase() === 'time');
			};
			editor.on('NodeChange', editorEventCallback);

			/* onSetup should always return the unbind handlers *//*
			return function (buttonApi) {
			  editor.off('NodeChange', editorEventCallback);
			};
		  }
		});
	  },
	  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
	});*//*
  </script>
</head>
<body>
  <div id="heloo_123">
   </div>
</body>
</html>
*/ ?>
</div>
<?php
	get_footer();
?>