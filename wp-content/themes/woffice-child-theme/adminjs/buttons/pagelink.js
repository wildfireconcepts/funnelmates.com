(function () {
	 tinymce.PluginManager.add('personalisation', function(editor, url) {
        editor.addButton('personalisation', {
            title: 'Personalisation',
			image : url+'/images/btn_personalisation.png',
            onclick: function (e) {
				var cur_editor = editor.id;
				cur_editor = cur_editor.replace ( /[^\d.]/g, '' );
				editor.windowManager.open( {
					title: 'Add Personalisation',
					body: [{
						type   : 'listbox',
						name   : 'personalisation',
						label  : 'Personalisation',
						values : [
							{text:'Subscribers Email',value:'SUBSCRIBER_EMAIL'},
							{text:'Funnel Creator\'s Name',value:'CAMPAIGN_FROM_NAME'},
							{text:'Current Year',value:'CURRENT_YEAR'},
							{text:'Current Month',value:'CURRENT_MONTH'},
							{text:'Current Day',value:'CURRENT_DAY'}
						],
					}],
					onsubmit: function( e ) {
						editor.insertContent('{'+e.data.personalisation+'}');
					}
				});
            }
        });
    });
	
	// var languages = ['css', 'php', 'html', 'javascript'];
	
	tinymce.PluginManager.add( 'load_template', function( editor ){
		function get_set_template_data(tid,template_title){
			var cur_editor = editor.id;
			// alert(cur_editor);
			var deliverable = '';
			var subject_id  = '';
			var cur_editor_id = cur_editor.split("_").pop();
			if(cur_editor.indexOf("immediate_") != '-1'){
				deliverable = 'Immediate';
				subject_id = 'immediate_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after1days_") != '-1'){
				deliverable = 'Day 1';
				subject_id = 'after1days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after2days_") != '-1'){
				deliverable = 'Day 2';
				subject_id = 'after2days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after3days_") != '-1'){
				deliverable = 'Day 3';
				subject_id = 'after3days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after4days_") != '-1'){
				deliverable = 'Day 4';
				subject_id = 'after4days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after5days_") != '-1'){
				deliverable = 'Day 5';
				subject_id = 'after5days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after6days_") != '-1'){
				deliverable = 'Day 6';
				subject_id = 'after6days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after7days_") != '-1'){
				deliverable = 'Day 7';
				subject_id = 'after7days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after14days_") != '-1'){
				deliverable = 'Day 14';
				subject_id = 'after14days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after21days_") != '-1'){
				deliverable = 'Day 21';
				subject_id = 'after21days_subject_'+cur_editor_id;
			}else if(cur_editor.indexOf("after28days_") != '-1'){
				deliverable = 'Day 28';
				subject_id = 'after28days_subject_'+cur_editor_id;
			}
			cur_editor = cur_editor.replace ( /[^\d.]/g, '' );
			var content = editor.getContent();
			// alert(content);
			if(content){
				Swal.fire({
				  title: 'Are you sure you want to do this?',
				  text: "'"+template_title+"' template will overwrite any existing content for the email delivering '"+deliverable+"' of your sequence.",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#425F94',
				  cancelButtonColor: '#FF6347',
				  confirmButtonText: 'Yes'
				}).then((result) => {
				  if (result.isConfirmed) {
					editor.setProgressState(1);
					var form_data = new FormData();
					form_data.append('action', 'get_email_template');
					form_data.append('template_id', tid);
					tinymce.util.XHR.send({
						url: ajaxurl,
						data: form_data,
						type: 'POST',
						success: function(data) {
							var obj = jQuery.parseJSON(data);
							editor.setContent(obj.content);
							if(obj.subject){
								// alert(subject_id);
								document.getElementById(subject_id).value = obj.subject;
							}
							editor.setProgressState(0);
						}
					});
				  }
				})
			}else{
				editor.setProgressState(1);
				var form_data = new FormData();
				form_data.append('action', 'get_email_template');
				form_data.append('template_id', tid);
				tinymce.util.XHR.send({
					url: ajaxurl,
					data: form_data,
					type: 'POST',
					success: function(data) {
						var obj = jQuery.parseJSON(data);
						editor.setContent(obj.content);
						if(obj.subject){
							// alert(subject_id);
							document.getElementById(subject_id).value = obj.subject;
						}
						editor.setProgressState(0);
					}
				});
			}
		}
		editor.addButton('load_template', {
            type: 'menubutton',
            text: 'Load Template',
            icon: false,
			/*********** Below data is connected to mysql table 'wp_setc_followup_email_templates' *****************/
            menu: [{
					text: 'Download Your Gift',
					onclick: function (){
						get_set_template_data(1,'Download Your Gift');
					}
				},{
					text: 'Download Followup',
					onclick: function (){
						get_set_template_data(2,'Download Followup');
					}
				},{
					text: 'Download Content',
					onclick: function (){
						get_set_template_data(3,'Download Content');
					}
				},{
					text: 'Affiliate Promo Starting Over',
					onclick: function (){
						get_set_template_data(4,'Affiliate Promo Starting Over');
					}
				},{
					text: 'Affiliate Promo Have You Tried?',
					onclick: function (){
						get_set_template_data(5,'Affiliate Promo Have You Tried?');
					}
				},{
					text: 'Affiliate Promo Magic Wand',
					onclick: function (){
						get_set_template_data(6,'Affiliate Promo Magic Wand');
					}
				},{
					text: 'Affiliate Promo Success',
					onclick: function (){
						get_set_template_data(7,'Affiliate Promo Success');
					}
				},{
					text: 'Affiliate Promo Avatar',
					onclick: function (){
						get_set_template_data(8,'Affiliate Promo Avatar');
					}
				},{
					text: 'Affiliate Promo Hard Lessons',
					onclick: function (){
						get_set_template_data(9,'Affiliate Promo Hard Lessons');
					}
				},{
					text: 'Affiliate Promo Comparison',
					onclick: function (){
						get_set_template_data(10,'Affiliate Promo Comparison');
					}
				},{
					text: 'Affiliate Promo Running Out',
					onclick: function (){
						get_set_template_data(11,'Affiliate Promo Running Out');
					}
				},{
					text: 'Affiliate Promo Last Chance',
					onclick: function (){
						get_set_template_data(12,'Affiliate Promo Last Chance');
					}
				},{
					text: 'Coaching Promo Transformation',
					onclick: function (){
						get_set_template_data(13,'Coaching Promo Transformation');
					}
				},{
					text: 'Coaching Promo Proof',
					onclick: function (){
						get_set_template_data(14,'Coaching Promo Proof');
					}
				},{
					text: 'Know, Like, Trust Elephant',
					onclick: function (){
						get_set_template_data(15,'Know, Like, Trust Elephant');
					}
				},{
					text: 'Know, Like, Trust Secret',
					onclick: function (){
						get_set_template_data(16,'Know, Like, Trust Secret');
					}
				}/*,{
					text: 'Know, Like, Trust Overnight',
					onclick: function (){
						get_set_template_data(17,'Know, Like, Trust Overnight');
					}
				}*/,{
					text: 'Know, Like, Trust Before/After',
					onclick: function (){
						get_set_template_data(18,'Know, Like, Trust Before/After');
					}
				},{
					text: 'Authority Builder 3 Steps',
					onclick: function (){
						get_set_template_data(19,'Authority Builder 3 Steps');
					}
				},{
					text: 'Authority Builder Emotional Benefit',
					onclick: function (){
						get_set_template_data(20,'Authority Builder Emotional Benefit');
					}
				},{
					text: 'Authority Builder Failure',
					onclick: function (){
						get_set_template_data(21,'Authority Builder Failure');
					}
				},{
					text: 'Authority Builder Client',
					onclick: function (){
						get_set_template_data(22,'Authority Builder Client');
					}
				},{
					text: 'Storytelling Gratitude',
					onclick: function (){
						get_set_template_data(23,'Storytelling Gratitude');
					}
				},{
					text: 'Storytelling Lighthearted',
					onclick: function (){
						get_set_template_data(24,'Storytelling Lighthearted');
					}
				},{
					text: 'Storytelling Reflection',
					onclick: function (){
						get_set_template_data(25,'Storytelling Reflection');
					}
				},{
					text: 'Storytelling Overwhelm',
					onclick: function (){
						get_set_template_data(26,'Storytelling Overwhelm');
					}
				},{
					text: 'FunnelMates Promo 1',
					onclick: function (){
						get_set_template_data(27,'FunnelMates Promo 1');
					}
				},{
					text: 'FunnelMates Promo 2',
					onclick: function (){
						get_set_template_data(28,'FunnelMates Promo 2');
					}
				},{
					text: 'FunnelMates Promo 3',
					onclick: function (){
						get_set_template_data(29,'FunnelMates Promo 3');
					}
				},
            ]
        });
	});
	
    tinymce.PluginManager.add('pagelink', function(editor, url) {
        editor.addButton('pagelink', {
            title: 'Page Link',
			image : url+'/images/btn_pagelink.png',
            onclick: function (e) {
				editor.setProgressState( 1 );
				var result = [];
				var cur_editor = editor.id;
				// cur_editor = cur_editor.replace ( /[^\d.]/g, '' );
				cur_editor = cur_editor.split("_").pop();
				if(cur_editor){
					var form_data = new FormData();
					form_data.append('post_id', cur_editor);
					form_data.append('action', 'get_pagelink_values');
					tinymce.util.XHR.send({
						url: ajaxurl,
						data: form_data,
						type: 'POST',
						success: function(data) {
							var obj = jQuery.parseJSON(data);
							jQuery.each(obj, function(key,valueObj){
								var d = {};
								d['text'] = valueObj.name;
								d['value'] = valueObj.aff_link;
								result.push(d);
							});
							editor.setProgressState( 0 );
							editor.windowManager.open( {
								title: 'Page Link',
								body: [{
									type   : 'listbox',
									name   : 'pagelink',
									label  : 'Page Link',
									values : result,
								}/*,
								{
									type   : 'checkbox',
									name   : 'pagelink_blank',
									label  : 'Open in New Tab?',
								}*/],
								onsubmit: function( e ) {
									/*var target = '';
									if(e.data.pagelink_blank == true){
										var target = 'target="_BLANK"';
									}
									if(e.data.pagelink_label){
										editor.insertContent('<a '+target+' href="'+e.data.pagelink +'" >'+e.data.pagelink_label+'</a>');
									}else{
										editor.insertContent('<a '+target+' href="'+e.data.pagelink +'" >'+e.data.pagelink+'</a>');
									}*/
									
									// var target = '0';
									// if(e.data.pagelink_blank == true){
										// var target = '1';
									// }
									var select1 = editor.selection.getContent();
									if(select1){
										editor.insertContent("[funnel_link id="+e.data.pagelink+"]"+select1+"[/funnel_link]");
									}else{
										editor.insertContent("[funnel_link id="+e.data.pagelink+"][/funnel_link]");
									}
								}
							});
						}
					});
				}
            }
        });
    });
	
	tinymce.PluginManager.add('downloadlink', function(editor, url) {
        editor.addButton('downloadlink', {
            title: 'Download Link',
			image : url+'/images/btn_download.png',
            onclick: function (e) {
				editor.setProgressState( 1 );
				var result = [];
				var cur_editor = editor.id;
				// cur_editor = cur_editor.replace ( /[^\d.]/g, '' );
				cur_editor = cur_editor.split("_").pop();
				if(cur_editor){
					var form_data = new FormData();
					form_data.append('post_id', cur_editor);
					form_data.append('action', 'get_downloadlink_values');
					tinymce.util.XHR.send({
						url: ajaxurl,
						data: form_data,
						type: 'POST',
						success: function(data) {
							var obj = jQuery.parseJSON(data);
							jQuery.each(obj, function(key,valueObj){
								var d = {};
								d['text'] = valueObj.name;
								d['value'] = valueObj.id;
								result.push(d);
							});
							editor.setProgressState( 0 );
							editor.windowManager.open( {
								title: 'Download Link',
								body: [{
									type   : 'listbox',
									name   : 'downloadlink',
									label  : 'Download Link',
									values : result,
								},/*{
									type   : 'checkbox',
									name   : 'downloadlink_blank',
									label  : 'Open in New Tab?',
								},*/{
									type   : 'button',
									name   : 'new_download_link',
									label  : 'Add New Download Link',
									text   : 'Add New',
									onclick: function(e) {
										editor.windowManager.close();
										editor.windowManager.open( {
										title: 'Add Download Link',
										body: [{
											type   : 'textbox',
											name   : 'downloadlink_name',
											label  : 'Link Name'
										},{
											type   : 'textbox',
											name   : 'downloadlink_link',
											label  : 'Download Link'
										}/*,{
											type   : 'textbox',
											name   : 'downloadlink_identifier',
											label  : 'Identifier'
										}*//*,{
											type   : 'checkbox',
											name   : 'downloadlink_blank',
											label  : 'Open in New Tab?',
										}*/],
										onsubmit: function( e ) {
											// console.log(e);
											// alert(e);
											if(e.data.downloadlink_name && e.data.downloadlink_link){
												if(validURL(e.data.downloadlink_link)){
													// e.preventDefault();
													editor.setProgressState(1);
													var form_data = new FormData();
													form_data.append('post_id', cur_editor);
													form_data.append('action', 'register_download_link');
													form_data.append('name', e.data.downloadlink_name);
													form_data.append('link', e.data.downloadlink_link);
													tinymce.util.XHR.send({
														url: ajaxurl,
														data: form_data,
														type: 'POST',
														success: function(data) {
															editor.setProgressState(0);
															if(data == 0){
																alert("Identifier Exists, Please Change.");
																return false;
															}else{
																editor.windowManager.close();
																// var target = '0';
																// if(e.data.downloadlink_blank == true){
																	// var target = '1';
																// }
																var select1 = editor.selection.getContent();
																if(select1){
																	editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																}else{
																	editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																}
																/*if(e.data.downloadlink_label){
																	editor.insertContent("[funnelmates_link id='"+data+"' target='"+target+"' label='"+e.data.downloadlink_label+"']");
																}else{
																	editor.insertContent("[funnelmates_link id='"+data+"' target='"+target+"']");
																}*/
															}
														}
													});
												}else{
													alert("Enter Valid URL.");
													return false;
												}
											}else{
												alert("Fill Required Fields.");
												return false;
											}
										}
									});
									}
								}],
								onsubmit: function( e ) {
									// var target = '0';
									// if(e.data.downloadlink_blank == true){
										// var target = '1';
									// }
									var select1 = editor.selection.getContent();
									if(select1){
										editor.insertContent("[funnelmates_link id="+e.data.downloadlink+"]"+select1+"[/funnelmates_link]");
									}else{
										editor.insertContent("[funnelmates_link id="+e.data.downloadlink+"][/funnelmates_link]");
									}
									/*if(e.data.downloadlink_label){
										editor.insertContent("[funnelmates_link id='"+e.data.downloadlink+"' target='"+target+"' label='"+e.data.downloadlink_label+"']");
									}else{
										editor.insertContent("[funnelmates_link id='"+e.data.downloadlink+"' target='"+target+"']");
									}*/
								}
							});
						}
					});
				}
            }
        });
    });
	
	tinymce.PluginManager.add('affiliatelink', function(editor, url) {
        editor.addButton('affiliatelink', {
            title: 'Affiliate Link',
			image : url+'/images/btn_aff.png',
            onclick: function (e) {
				editor.setProgressState( 1 );
				var result = [];
				var cur_editor = editor.id;
				// cur_editor = cur_editor.replace ( /[^\d.]/g, '' );
				cur_editor = cur_editor.split("_").pop();
				if(cur_editor){
					var form_data = new FormData();
					form_data.append('post_id', cur_editor);
					form_data.append('action', 'get_affiliatelink_values');
					tinymce.util.XHR.send({
						url: ajaxurl,
						data: form_data,
						type: 'POST',
						success: function(data) {
							var obj = jQuery.parseJSON(data);
							jQuery.each(obj, function(key,valueObj){
								var d = {};
								d['text'] = valueObj.name;
								d['value'] = valueObj.id;
								result.push(d);
							});
							editor.setProgressState( 0 );
							editor.windowManager.open( {
								title: 'Affiliate Link',
								body: [{
									type   : 'listbox',
									name   : 'affiliatelink',
									label  : 'Affiliate Link',
									values : result,
								},{
									type   : 'listbox',
									name   : 'affiliatelink_type',
									label  : 'Add New Affiliate Link',
									values : [{text:'Choose',value:'choose'},{text:'JVZoo',value:'jvzoo'},{text:'ClickBank',value:'clickbank'},{text:'WarriorPlus',value:'warriorplus'},{text:'PayKickStart',value:'paykickstart'}],
								}],
								onselect: function(e) {
									var curvalue = e.control.settings.value;
									if(curvalue == 'warriorplus'){
										editor.setProgressState(1);
										editor.windowManager.close();
										var result_1 = [];
										var form_data = new FormData();
										form_data.append('post_id', cur_editor);
										form_data.append('action', 'get_warriorplus_products');
										tinymce.util.XHR.send({
											url: ajaxurl,
											data: form_data,
											type: 'POST',
											success: function(data) {
												var obj = jQuery.parseJSON(data);
												jQuery.each(obj, function(key,valueObj){
													var d = {};
													d['text'] = valueObj.offer.name;
													d['value'] = valueObj.offer.id;
													result_1.push(d);
												});
												editor.setProgressState(0);
												// editor.windowManager.close();
												editor.windowManager.open({
													title: 'WarriorPlus Affiliate Link',
													body: [{
														type   : 'textbox',
														name   : 'affiliatelink_name',
														label  : 'Link Name'
													},{
														type   : 'listbox',
														name   : 'affiliatelink_link',
														label  : 'WarriorPlus Product',
														values : result_1,
													}/*,{
														type   : 'textbox',
														name   : 'affiliatelink_identifier',
														label  : 'Identifier'
													}*/],
													onsubmit: function( e ) {
														if(e.data.affiliatelink_name && e.data.affiliatelink_link){
															editor.setProgressState(1);
															var form_data = new FormData();
															form_data.append('post_id', cur_editor);
															form_data.append('action', 'register_affiliate_link');
															form_data.append('linktype', 'warriorplus');
															form_data.append('name', e.data.affiliatelink_name);
															form_data.append('link', e.data.affiliatelink_link);
															tinymce.util.XHR.send({
																url: ajaxurl,
																data: form_data,
																type: 'POST',
																success: function(data) {
																	editor.setProgressState(0);
																	if(data == 0){
																		alert("Identifier Exists, Please Change.");
																		return false;
																	}else{
																		editor.windowManager.close();
																		var select1 = editor.selection.getContent();
																		if(select1){
																			editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																		}else{
																			editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																		}
																	}
																}
															});
														}else{
															alert("Fill Required Fields.");
															return false;
														}
													}
												});
											}
										});
									}else if(curvalue == 'jvzoo'){
										editor.windowManager.close();
										editor.windowManager.open({
											title: 'JVZoo Affiliate Link',
											body: [{
												type   : 'textbox',
												name   : 'affiliatelink_name',
												label  : 'Link Name'
											},{
												type   : 'textbox',
												name   : 'affiliatelink_link',
												label  : 'Affiliate Link'
											}/*,{
												type   : 'textbox',
												name   : 'affiliatelink_identifier',
												label  : 'Identifier'
											}*/],
											onsubmit: function( e ) {
												if(e.data.affiliatelink_name && e.data.affiliatelink_link){
													if(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)+(jvzoo|jvz1|jvz2|jvz3|jvz4|jvz5|jvz6+)\.[a-z]{3}?(\/ac|\/affiliate+)+(\/[0-9]{6}|\/affiliateinfo)+(\/[0-9]{6}|\/index)+(\/[0-9]{6}|\/)?$/igm.test(e.data.affiliatelink_link) == true){
														editor.setProgressState(1);
														var form_data = new FormData();
														form_data.append('post_id', cur_editor);
														form_data.append('action', 'register_affiliate_link');
														form_data.append('linktype', 'jvzoo');
														form_data.append('name', e.data.affiliatelink_name);
														form_data.append('link', e.data.affiliatelink_link);
														tinymce.util.XHR.send({
															url: ajaxurl,
															data: form_data,
															type: 'POST',
															success: function(data) {
																editor.setProgressState(0);
																if(data == 0){
																	alert("Identifier Exists, Please Change.");
																	return false;
																}else{
																	editor.windowManager.close();
																	var select1 = editor.selection.getContent();
																	if(select1){
																		editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																	}else{
																		editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																	}
																}
															}
														});
													}else{
														alert("Enter Valid JVZoo URL.");
														return false;
													}
												}else{
													alert("Fill Required Fields.");
													return false;
												}
											}
										});
									}else if(curvalue == 'clickbank'){
										editor.windowManager.close();
										editor.windowManager.open({
											title: 'ClickBank Affiliate Link',
											body: [{
												type   : 'textbox',
												name   : 'affiliatelink_name',
												label  : 'Link Name'
											},{
												type   : 'textbox',
												name   : 'affiliatelink_link',
												label  : 'ClickBank ID'
											}/* ,{
												type   : 'textbox',
												name   : 'affiliatelink_identifier',
												label  : 'Identifier'
											} */,{
												type   : 'textbox',
												name   : 'affiliatelink_alt_page',
												label  : 'alt page'
											}],
											onsubmit: function( e ) {
												if(e.data.affiliatelink_name && e.data.affiliatelink_link){
													// if(validURL(e.data.affiliatelink_link)){
														editor.setProgressState(1);
														var form_data = new FormData();
														form_data.append('post_id', cur_editor);
														form_data.append('action', 'register_affiliate_link');
														form_data.append('linktype', 'clickbank');
														form_data.append('name', e.data.affiliatelink_name);
														form_data.append('link', e.data.affiliatelink_link);
														// form_data.append('identifier', e.data.affiliatelink_identifier);
														if(e.data.affiliatelink_alt_page){
															form_data.append('notes', e.data.affiliatelink_alt_page);
														}
														tinymce.util.XHR.send({
															url: ajaxurl,
															data: form_data,
															type: 'POST',
															success: function(data) {
																editor.setProgressState(0);
																if(data == 0){
																	alert("Identifier Exists, Please Change.");
																	return false;
																}else{
																	editor.windowManager.close();
																	var select1 = editor.selection.getContent();
																	if(select1){
																		editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																	}else{
																		editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																	}
																}
															}
														});
													/*}else{
														alert("Enter Valid URL.");
														return false;
													}*/
												}else{
													alert("Fill Required Fields.");
													return false;
												}
											}
										});
									}else if(curvalue == 'paykickstart'){
										editor.windowManager.close();
										editor.windowManager.open({
											title: 'PayKickStart Affiliate Link',
											body: [{
												type   : 'textbox',
												name   : 'affiliatelink_name',
												label  : 'Link Name'
											},{
												type   : 'textbox',
												name   : 'affiliatelink_link',
												label  : 'Campaign ID'
											}/* ,{
												type   : 'textbox',
												name   : 'affiliatelink_identifier',
												label  : 'Identifier'
											} */,{
												type   : 'textbox',
												name   : 'affiliatelink_notes',
												label  : 'Notes'
											}],
											onsubmit: function( e ) {
												if(e.data.affiliatelink_name && e.data.affiliatelink_link){
													// if(validURL(e.data.affiliatelink_link)){
														editor.setProgressState(1);
														var form_data = new FormData();
														form_data.append('post_id', cur_editor);
														form_data.append('action', 'register_affiliate_link');
														form_data.append('linktype', 'paykickstart');
														form_data.append('name', e.data.affiliatelink_name);
														form_data.append('link', e.data.affiliatelink_link);
														// form_data.append('identifier', e.data.affiliatelink_identifier);
														if(e.data.affiliatelink_notes){
															form_data.append('notes', e.data.affiliatelink_notes);
														}
														tinymce.util.XHR.send({
															url: ajaxurl,
															data: form_data,
															type: 'POST',
															success: function(data) {
																editor.setProgressState(0);
																if(data == 0){
																	alert("Identifier Exists, Please Change.");
																	return false;
																}else{
																	editor.windowManager.close();
																	var select1 = editor.selection.getContent();
																	if(select1){
																		editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																	}else{
																		editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																	}
																}
															}
														});
													/*}else{
														alert("Enter Valid URL.");
														return false;
													}*/
												}else{
													alert("Fill Required Fields.");
													return false;
												}
											}
										});
									}
								},
								onsubmit: function( e ) {
									// var target = '0';
									// if(e.data.affiliatelink_blank == true){
										// var target = '1';
									// }
									var select1 = editor.selection.getContent();
									if(select1){
										editor.insertContent("[funnelmates_link id="+e.data.affiliatelink+"]"+select1+"[/funnelmates_link]");
									}else{
										editor.insertContent("[funnelmates_link id="+e.data.affiliatelink+"][/funnelmates_link]");
									}
									/*if(e.data.affiliatelink_label){
										// editor.insertContent('<a '+target+' href="'+e.data.affiliatelink +'" >'+e.data.affiliatelink_label+'</a>');
										editor.insertContent("[funnelmates_link id='"+e.data.affiliatelink+"' target='"+target+"' label='"+e.data.affiliatelink_label+"']");
									}else{
										// editor.insertContent('<a '+target+' href="'+e.data.affiliatelink +'" >'+e.data.affiliatelink+'</a>');
										editor.insertContent("[funnelmates_link id='"+e.data.affiliatelink+"' target='"+target+"']");
									}*/
								}
							});
						}
					});
				}
            }
        });
    });
	
	tinymce.PluginManager.add('externallink', function(editor, url) {
        editor.addButton('externallink', {
            title: 'External Link',
			image : url+'/images/btn_external.png',
            onclick: function (e) {
				editor.setProgressState( 1 );
				var result = [];
				var cur_editor = editor.id;
				// cur_editor = cur_editor.replace ( /[^\d.]/g, '' );
				cur_editor = cur_editor.split("_").pop();
				if(cur_editor){
					var form_data = new FormData();
					form_data.append('post_id', cur_editor);
					form_data.append('action', 'get_externallink_values');
					tinymce.util.XHR.send({
						url: ajaxurl,
						data: form_data,
						type: 'POST',
						success: function(data) {
							var obj = jQuery.parseJSON(data);
							jQuery.each(obj, function(key,valueObj){
								var d = {};
								d['text'] = valueObj.name;
								d['value'] = valueObj.id;
								result.push(d);
							});
							editor.setProgressState( 0 );
							editor.windowManager.open( {
								title: 'External Link',
								body: [{
									type   : 'listbox',
									name   : 'externallink',
									label  : 'External Link',
									values : result,
								}/*,
								{
									type   : 'checkbox',
									name   : 'externallink_blank',
									label  : 'Open in New Tab?',
								}*/,{
									type   : 'button',
									name   : 'new_external_link',
									label  : 'Add New External Link',
									text   : 'Add New',
									onclick: function(e) {
										editor.windowManager.close();
										editor.windowManager.open( {
										title: 'Add External Link',
										body: [{
											type   : 'textbox',
											name   : 'externallink_name',
											label  : 'Link Name'
										},{
											type   : 'textbox',
											name   : 'externallink_link',
											label  : 'External Link'
										}/*,{
											type   : 'textbox',
											name   : 'externallink_identifier',
											label  : 'Identifier'
										}*/,{
											type   : 'textbox',
											name   : 'externallink_notes',
											label  : 'Notes'
										}/*,{
											type   : 'checkbox',
											name   : 'externallink_blank',
											label  : 'Open in New Tab?',
										}*/],
										onsubmit: function( e ) {
											// console.log(e);
											// alert(e);
											if(e.data.externallink_name && e.data.externallink_link){
												if(validURL(e.data.externallink_link)){
													// e.preventDefault();
													editor.setProgressState(1);
													var form_data = new FormData();
													form_data.append('post_id', cur_editor);
													form_data.append('action', 'register_external_link');
													form_data.append('name', e.data.externallink_name);
													form_data.append('link', e.data.externallink_link);
													// form_data.append('identifier', e.data.externallink_identifier);
													form_data.append('notes', e.data.externallink_notes);
													tinymce.util.XHR.send({
														url: ajaxurl,
														data: form_data,
														type: 'POST',
														success: function(data) {
															editor.setProgressState(0);
															if(data == 0){
																alert("Identifier Exists, Please Change.");
																return false;
															}else{
																editor.windowManager.close();
																// var target = '0';
																// if(e.data.externallink_blank == true){
																	// var target = '1';
																// }
																var select1 = editor.selection.getContent();
																if(select1){
																	// editor.insertContent('<a '+target+' href="'+e.data.externallink_link +'" >'+e.data.externallink_label+'</a>');
																	editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																}else{
																	// editor.insertContent('<a '+target+' href="'+e.data.externallink_link +'" >'+e.data.externallink_link+'</a>');
																	editor.insertContent("[funnelmates_link id="+data+"]"+select1+"[/funnelmates_link]");
																}
															}
														}
													});
												}else{
													alert("Enter Valid URL.");
													return false;
												}
											}else{
												alert("Fill Required Fields.");
												return false;
											}
										}
									});
									}
								}],
								onsubmit: function( e ) {
									// var target = '0';
									// if(e.data.externallink_blank == true){
										// var target = '1';
									// }
									var select1 = editor.selection.getContent();
									if(select1){
										editor.insertContent("[funnelmates_link id="+e.data.externallink+"]"+select1+"[/funnelmates_link]");
									}else{
										editor.insertContent("[funnelmates_link id="+e.data.externallink+"][/funnelmates_link]");
									}
								}
							});
						}
					});
				}
            }
        });
    });
})();
function validURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(str);
}