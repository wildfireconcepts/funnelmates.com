<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/wp-load.php");
global $wpdb;
$params = explode('/', $_REQUEST['l']);
// [0] = Member Id
// [1] = Identifier
// [2..n] = Anything else that was in the query string
// https://funnelmates.com/ex/1/ex1
$jumplinkRec = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` WHERE `aff_network`='external' AND `link_identifier` = '{$params[1]}'");
// print_r($jumplinkRec);exit;
if(empty($jumplinkRec->aff_link)){
	wp_redirect("https://live.funnelmates.com/sales");
	exit;
}
wp_redirect($jumplinkRec->aff_link);
exit;