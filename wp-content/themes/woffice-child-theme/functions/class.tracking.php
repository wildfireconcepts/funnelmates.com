<?php
class FM_Tracking {
	function __construct() {
		add_filter('wp_footer', array($this, 'counter'));
	}
	public function counter(){
		$stop = false;
		$current_year = current_time('Y');
		$current_month = current_time('m');
		$current_day = current_time('d');
		$current_now = current_time("Y-m-d");
		$current_yesterday = date( 'Y-m-d', strtotime( '-1 days', current_time('timestamp') ) );
		$current_week = date( 'Y-m-d', strtotime( '-7 days', current_time('timestamp') ) );
		$ua = $_SERVER['HTTP_USER_AGENT'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$traking_status = '0';
		
		global $wpdb;
		$table_name = $wpdb->prefix . 'setc_view_counter';
		
		if(get_the_title() === ''){$stop = true;}
		if(get_bloginfo('name') === ''){$stop = true;}
		$post_id = get_the_ID();
		if(get_post_type($post_id) != 'landing'){$stop = true;}
		$CCID = '';
		if(isset($_COOKIE['CCID'])) {
			$CCID = $_COOKIE['CCID'];
		}
		$trackingid = '';
		if(isset($_COOKIE['trackingid'])) {
			$trackingid = $_COOKIE['trackingid'];
		}
		

		
		$query = $wpdb->prepare("SELECT * FROM $table_name WHERE pageid=%d AND time='%s'" , $post_id , $current_now);
		$rows = $wpdb->get_results($query);
		
		// $postid_query = $wpdb->prepare("SELECT * FROM $table_name WHERE pageid=%d" , $post_id);
		// $postid_rows = $wpdb->get_results($postid_query);
		foreach($rows as $row) {
			if($row->title == get_the_title() and $row->ipaddress == $ip and $row->ccid == $CCID and $row->refresh_delete == '0'){
				$stop == true;
				return $stop;
			}
		}
		
		
		if(!($stop == true)){
			$wpdb->insert(
				$table_name,
				array(
				  'time' => $current_now,
				  'title' => get_the_title(),
				  'url' => get_permalink(),
				  'ccid' => $CCID,
				  'pageid' => get_the_ID(),
				  'trackingid' => $trackingid,
				  'ipaddress' => $ip,
				  'useragent' => $ua,
				  'refresh_delete' => $traking_status
				)
			);
		}
	}
	public function email($post_id,$email,$CCID){
		$stop = false;
		$current_year = current_time('Y');
		$current_month = current_time('m');
		$current_day = current_time('d');
		$current_now = current_time("Y-m-d");
		$current_yesterday = date( 'Y-m-d', strtotime( '-1 days', current_time('timestamp') ) );
		$current_week = date( 'Y-m-d', strtotime( '-7 days', current_time('timestamp') ) );
		$ua = $_SERVER['HTTP_USER_AGENT'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$traking_status = '0';
		
		global $wpdb;
		$table_name = $wpdb->prefix . 'setc_email_counter';
		
		$trackingid = '';
		if(isset($_COOKIE['trackingid'])) {
			$trackingid = $_COOKIE['trackingid'];
		}
		if(!($stop == true)){
			$wpdb->insert(
				$table_name,
				array(
				  'time' => $current_now,
				  'pageid' => $post_id,
				  'email' => $email,
				  'ccid' => $CCID,
				  'trackingid' => $trackingid,
				  'ipaddress' => $ip,
				  'useragent' => $ua,
				  'refresh_delete' => $traking_status
				)
			);	
		}		
	}
}
new FM_Tracking();
?>