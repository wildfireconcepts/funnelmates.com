<?php
add_shortcode('ccv3_ar_aweber_settings','aweber_settings_shortcode');
add_shortcode('ccv3_ar_mailchimp_settings','mailchimp_settings_shortcode');
add_shortcode('ccv3_ar_getresponse_settings','getresponse_settings_shortcode');
add_shortcode('ccv3_ar_mailsqaud_settings','mailsqaud_settings_shortcode');
add_shortcode('ccv3_ar_sendlane_settings','sendlane_settings_shortcode');
add_shortcode('ccv3_ar_webinar_settings','webinar_settings_shortcode');
add_shortcode('ccv3_ar_everwebinar_settings','everwebinar_settings_shortcode');
add_shortcode('ccv3_ar_convertkit_settings','convertkit_settings_shortcode');
add_shortcode('ccv3_ar_drip_settings','drip_settings_shortcode');
add_shortcode('ccv3_ar_mailerlite_settings','mailerlite_settings_shortcode');
add_shortcode('ccv3_ar_sendiio_settings','sendiio_settings_shortcode');
//add_shortcode('ccv3_ar_constant_contact_settings','constant_contact_settings_shortcode');

function sendiio_settings_shortcode(){
	$api = get_user_meta(get_current_user_id(),'_ccv3_sendiio_apikey',true);	
	$token = get_user_meta(get_current_user_id(),'_ccv3_sendiio_apitoken',true);	
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Token:</th>';
			$html .= '<td style="width:91%;">';
				if($token){
					$html .= '<input style="width:100%;" type="text" name="sendiio_api_token" value="'.$token.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="sendiio_api_token" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<th>API Secret:</th>';
			$html .= '<td style="width:91%;">';
				if($api){
					$html .= '<input style="width:100%;" type="text" name="sendiio_api_key" value="'.$api.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="sendiio_api_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message_sendi" style="/* display:none; */ ">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($api && $token){
					$html .= '<button type="button" class="btn_red" id="btn_sendiio_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_sendiio_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>'; 
	return $html;
}
function mailerlite_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_mailerlite_apikey',true);
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Key:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="mailerlite_api_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="mailerlite_api_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message_ml" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_mailerlite_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_mailerlite_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>'; 
	return $html;
}

function drip_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_drip_apikey',true);
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Key:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="drip_api_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="drip_api_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message_dp" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_drip_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_drip_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>'; 
	return $html;
}
function convertkit_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_convertkit_apikey',true);
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Key:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="convertkit_api_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="convertkit_api_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message_ck" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_convertkit_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_convertkit_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>'; 
	return $html;
}
/*function constant_contact_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_constant_contact_apikey',true);
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Key:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="constant_contact_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="constant_contact_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_constant_contact_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_constant_contact_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>'; 
	return $html;
}*/
function everwebinar_settings_shortcode(){
	$everwebinar_key = get_user_meta(get_current_user_id(),'fm_everwebinar_apikey',true);
	if($everwebinar_key){
		$html .= '<p>Account is Connected<p>';
		$html .= '<input type="text" name="everwebinar_key" value="'.$everwebinar_key.'" placeholder="EverWebinar API Key" />';
		$html .= '<button type="button" class="btn_red" id="btn_everwebinar_disconnect">Disconnect</button>';
	}else{
		$html .= '<input type="text" name="everwebinar_key" placeholder="EverWebinar API Key" />';
		$html .= '<button type="button" class="btn_green" id="everwebinar_connect" style="float: none;">Connect</button>';
	}
	return $html;
}
function webinar_settings_shortcode(){
	$ruri = site_url().'/settings/#1614337426314-c40234df-9260';
	$gotowebinar = get_user_meta(get_current_user_id(),'fm_gotowebinar',true);
	if($gotowebinar->access_token){
		$html .= '<p><strong>'.$gotowebinar->email.'</strong> is Connected<p>';
		$html .= '<button type="button" class="btn_red" id="btn_webinar_disconnect">Disconnect</button>';
	}else{
		if(isset($_GET['code'])){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://api.getgo.com/oauth/v2/token');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=authorization_code&redirect_uri=$ruri&code=".$_GET['code']);
			$headers = array();
			$headers[] = 'Authorization: Basic '.base64_encode('d92b304a-9344-4cb4-a012-56947c8151ff:UcRhLAxv81il8dp2s8mxHrSN');
			$headers[] = 'Accept: application/json';
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				$html .= 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			$rs = json_decode($result);
			update_user_meta(get_current_user_id(),'fm_gotowebinar', $rs);
			$html .= '<p><strong>'.$rs->email.'</strong> is Connected<p>';
			$html .= '<button type="button" class="btn_red" id="btn_webinar_disconnect">Disconnect</button>';
		}else{
			$uri = 'https://api.getgo.com/oauth/v2/authorize?client_id=d92b304a-9344-4cb4-a012-56947c8151ff&response_type=code&redirect_uri='.$ruri;
			$html .= '<form action="https://api.getgo.com/oauth/v2/authorize" method="GET">';
				$html .= '<input type="hidden" name="redirect_uri" value="'.$ruri.'" />';
				$html .= '<input type="hidden" name="client_id" value="d92b304a-9344-4cb4-a012-56947c8151ff" />';
				$html .= '<input type="hidden" name="response_type" value="code" />';
				$html .= '<input type="submit" name="submit" value="Authorize" style="float: none;" />';
			$html .= '</form>';
		}
	}
	return $html;
}
function aweber_settings_shortcode(){
	require_once(get_stylesheet_directory().'/includes/oauth2/vendor/autoload.php');
	$accessToken = get_user_meta(get_current_user_id(),'fm_aweber_access_token',true);
	$accessTokenRefresh = get_user_meta(get_current_user_id(),'fm_aweber_token_refresh',true);
	$scopes = array(
		'account.read',
		'list.read',
		'list.write',
		'subscriber.read',
		'subscriber.write',
		// 'email.read',
		// 'email.write',
		'subscriber.read-extended',
		// 'landing-page.read'
	);
	$provider = new League\OAuth2\Client\Provider\GenericProvider([
		'clientId'              	=> 'CGPdJJYONkMmvDYO4P6JCDnNzJtvoYUq',    // The client ID assigned to you by the provider
		'clientSecret'          	=> 'np8YZI3zpI6MjAYQxbZTuKPS0mi2mv5F',    // The client password assigned to you by the provider
		'redirectUri'           	=> 'https://funnelmates.com/settings/',
		'scopes' 					=> $scopes,
		'scopeSeparator' 			=> ' ',
		'urlAuthorize'          	=> 'https://auth.aweber.com/oauth2/authorize',
		'urlAccessToken'        	=> 'https://auth.aweber.com/oauth2/token',
		'urlResourceOwnerDetails' 	=> 'https://api.aweber.com/1.0/accounts'
	]);
	if(!empty($_GET['code'])){
		if (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])){
			if (isset($_SESSION['oauth2state'])) {
				unset($_SESSION['oauth2state']);
			}
			$html .= 'Invalid State';
		}else if(!empty($_GET['code'])){
			try {
				$accessToken = $provider->getAccessToken('authorization_code', [
					'code' => $_GET['code']
				]);
				// echo 'Access Token: ' . $accessToken->getToken() . "<br>";
				// echo 'Refresh Token: ' . $accessToken->getRefreshToken() . "<br>";
				update_user_meta(get_current_user_id(),'fm_aweber_access_token',$accessToken->getToken());
				update_user_meta(get_current_user_id(),'fm_aweber_token_refresh',$accessToken->getRefreshToken());			
				$resourceOwner = $provider->getResourceOwner($accessToken);
				update_user_meta(get_current_user_id(),'fm_aweber_resourceowner',$resourceOwner->toArray());
				update_user_meta(get_current_user_id(),'fm_aweber_accesstoken_parent',$accessToken);
				$html .= '<button type="button" class="btn_red" id="btn_aweber_disconnect">Disconnect</button>';
				return $html;

			} catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

				// Failed to get the access token or user details.
				exit($e->getMessage());

			}
		}
	}
	if($accessToken == '' || $accessTokenRefresh == ''){
		$html .= '<button class="btn_green" id="btn_aweber_connect" type="button">Authorize</button>';
	}else{
		$html .= '<button type="button" class="btn_red" id="btn_aweber_disconnect">Disconnect</button>';
	}
	return $html;
}
function getCollection($client, $accessToken, $url) {
    $collection = array();
    while (isset($url)) {
        $request = $client->get($url,
            ['headers' => ['Authorization' => 'Bearer ' . $accessToken]]
        );
        $body = $request->getBody();
        $page = json_decode($body, true);
        $collection = array_merge($page['entries'], $collection);
        $url = isset($page['next_collection_link']) ? $page['next_collection_link'] : null;
    }
    return $collection;
}
/*function aweber_settings_shortcode(){
	// require_once(get_stylesheet_directory().'/includes/AWeber-API-PHP-Library-master/aweber_api/aweber.php');
	$accessToken = get_user_meta(get_current_user_id(),'fm_aweber_access_token',true);
	$accessTokenSecret = get_user_meta(get_current_user_id(),'fm_aweber_token_secret',true);
	// $html .= 'here'.$accessToken.'<br/>'.$accessTokenSecret;
	$aweber = new \AWeberAPI('NyEAXPfk0LUSMwR5i6yqOdfIFZlc5efU', 'jiVDFUaulAn3oWnT0Zzu8lumZO8FqqUs');
	if (!empty($_GET['oauth_token'])) {
		$aweber->user->tokenSecret = $_COOKIE['requestTokenSecret'];
		$aweber->user->requestToken = $_GET['oauth_token'];
		$aweber->user->verifier = $_GET['oauth_verifier'];
		// echo '<pre>';
		// print_r($aweber->getAccessToken());
		// echo '</pre>';
		list($accessToken, $accessTokenSecret) = $aweber->getAccessToken();
		update_user_meta(get_current_user_id(),'fm_aweber_access_token', $accessToken);
		update_user_meta(get_current_user_id(),'fm_aweber_token_secret', $accessTokenSecret);
	}
	if($accessToken == '' || $accessTokenSecret == ''){
		$html .= '<button class="btn_green" id="btn_aweber_connect" type="button">Authorize</button>';
	}else{
		// $account = $aweber->getAccount($accessToken, $accessTokenSecret);
		// $html .= '<pre>';
		// $html .= print_r($account,true);
		// $html .= '</pre>';
		$html .= '<button type="button" class="btn_red" id="btn_aweber_disconnect">Disconnect</button>';
	}
	return $html;
}*/
function mailsqaud_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apitoken',true);
	$apilogin = get_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apilogin',true);	
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Token:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="mailsqaud_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="mailsqaud_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<th>API Login:</th>';
			$html .= '<td style="width:91%;">';
				if($apilogin){
					$html .= '<input style="width:100%;" type="text" name="mailsqaud_login" value="'.$apilogin.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="mailsqaud_login" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message_ms" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_mailsqaud_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_mailsqaud_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>';
	return $html;
}
function getresponse_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_getresponse_apikey',true);
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Key:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="getresponse_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="getresponse_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message_gr" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_getresponse_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_getresponse_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>';
	return $html;
}
function sendlane_settings_shortcode(){
	$apikey = get_user_meta(get_current_user_id(),'_ccv3_sendlane_apikey',true);
	$apihashkey = get_user_meta(get_current_user_id(),'_ccv3_sendlane_apihashkey',true);
	$html .= '<div>';
		$html .= '<table>';
			$html .= '<tr>';
				$html .= '<th>API Key:</th>';
				$html .= '<td style="width:91%;">';
					if($apikey){
						$html .= '<input style="width:100%;" type="text" name="sendlane_api_key" value="'.$apikey.'" disabled />';
					}else{
						$html .= '<input style="width:100%;" type="text" name="sendlane_api_key" />';
					}
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<th>API Hash Key:</th>';
				$html .= '<td style="width:91%;">';
					if($apihashkey){
						$html .= '<input style="width:100%;" type="text" name="sendlane_api_hash_key" value="'.$apihashkey.'" disabled />';
					}else{
						$html .= '<input style="width:100%;" type="text" name="sendlane_api_hash_key" />';
					}
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr class="hidden_tr_message_sl" style="display:none;">';
				$html .= '<th></th>';
				$html .= '<td></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td colspan="2">';
					if($apikey && $apihashkey){
						$html .= '<button type="button" class="btn_red" id="btn_sendlane_disconnect">Disconnect</button>';
					}else{
						$html .= '<button type="button" class="btn_green" id="btn_sendlane_connect">Connect</button>';
					}
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';
	$html .= '</div>';
	return $html;
}
function mailchimp_settings_shortcode(){
	$ifconnected = get_user_meta(get_current_user_id(),'_ccv3_mailchimp_apikey',true);
	$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th>API Key:</th>';
			$html .= '<td style="width:91%;">';
				if($ifconnected){
					$html .= '<input style="width:100%;" type="text" name="mailchimp_key" value="'.$ifconnected.'" disabled />';
				}else{
					$html .= '<input style="width:100%;" type="text" name="mailchimp_key" />';
				}
			$html .= '</td>';
		$html .= '</tr>';
		$html .= '<tr class="hidden_tr_message" style="display:none;">';
			$html .= '<th></th>';
			$html .= '<td></td>';
		$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				if($ifconnected){
					$html .= '<button type="button" class="btn_red" id="btn_mailchimp_disconnect">Disconnect</button>';
				}else{
					$html .= '<button type="button" class="btn_green" id="btn_mailchimp_connect">Connect</button>';
				}
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>';
	return $html;
}
function mailchimp_curl_connect( $url, $request_type, $api_key, $data = array() ) {
	if( $request_type == 'GET' )
		$url .= '?' . http_build_query($data);
 
	$mch = curl_init();
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Basic '.base64_encode( 'user:'. $api_key )
	);
	curl_setopt($mch, CURLOPT_URL, $url );
	curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
	//curl_setopt($mch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($mch, CURLOPT_RETURNTRANSFER, true); // do not echo the result, write it into variable
	curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
	curl_setopt($mch, CURLOPT_TIMEOUT, 10);
	curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false); // certificate verification for TLS/SSL connection
 
	if( $request_type != 'GET' ) {
		curl_setopt($mch, CURLOPT_POST, true);
		curl_setopt($mch, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
	}
 
	return curl_exec($mch);
}