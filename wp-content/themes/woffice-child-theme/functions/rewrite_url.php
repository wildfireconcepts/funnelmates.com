<?php
add_action( 'init', 'funnelmates_rewrite_add_rewrites' );
function funnelmates_rewrite_add_rewrites(){
	// Download Link Jumplinks
	add_rewrite_rule(
        '^dll\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/dll.php?l=$1',
        'top'
    );
	// JVZoo Jumplinks
    add_rewrite_rule(
        '^jvz\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/jvz.php?l=$1',
        'top'
    );
	// ClickBank Jumplinks
	add_rewrite_rule(
        '^cb\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/cb.php?l=$1',
        'top'
    );
	// WarriorPlus Jumplinks
	add_rewrite_rule(
        '^wp\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/wp.php?l=$1',
        'top'
    );
	// PayKickStart Jumplinks
	add_rewrite_rule(
        '^pks\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/pks.php?l=$1',
        'top'
    );
	// Landing Page Jumplinks
	add_rewrite_rule(
        '^f\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/f.php?l=$1',
        'top'
    );
	// Download Page Jumplinks
	add_rewrite_rule(
        '^dl\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/dl.php?l=$1',
        'top'
    );
	// Confirm Page Jumplinks
	add_rewrite_rule(
        '^c\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/cnf.php?l=$1',
        'top'
    );
	// Thank you Page Jumplinks
	add_rewrite_rule(
        '^th\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/t.php?l=$1',
        'top'
    );
	// External Jumplinks
	add_rewrite_rule(
        '^ex\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/ex.php?l=$1',
        'top'
    );
	// Facebook Share Jumplinks
	add_rewrite_rule(
        '^fb\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/fb.php?l=$1',
        'top'
    );
	// Linkedin Share Jumplinks
	add_rewrite_rule(
        '^in\/(.+)?$',
        '/wp-content/themes/woffice-child-theme/jumplink/in.php?l=$1',
        'top'
    );
}
?>