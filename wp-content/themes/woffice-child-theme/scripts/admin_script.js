jQuery(document).ready(function(){
	
	// withdrawal_tab(event, 'pending'); // call pending record on load in withdrawal_reqiest
	
	jQuery(document).on('click','.delete_free_funnel', function(){
		var id = jQuery(this).attr('data');
		if(id){
			var form_data = new FormData();
				form_data.append('action', 'delete_free_funnel');
				form_data.append('id', id);
				jQuery.ajax({
						url: ajaxurl,
						cache: false,
						contentType: false,
						processData: false,
						context:this,
						data: form_data,
						type: 'POST',
						success: function(data) {
							if(data){
								window.location = window.location;
							}
						}
				});
		}
	});
	jQuery(document).on('click','#search_by_email', function(){
		var c_tab = jQuery('#active_tab').val();
		var search = jQuery('input[name=s_email]').val();
		if(search == ''){
			jQuery('input[name=s_email]').css('border','1px solid red');
			return false;
		}else{
			jQuery('input[name=s_email]').css('border','');
		}
		var form_data = new FormData();
		form_data.append('action', 'withdrawal_search_email');
		form_data.append('search', search);
		form_data.append('c_tab', c_tab);
		jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
						//alert(data);
						jQuery('#customers').html(data);
				}
		});
	});
	
	jQuery(document).on('click','.withdrawal_accept', function(){
		//alert(jQuery(this).val());
		var accept = jQuery(this).val();
		/* var txt = this.id; 
		alert(txt); */
		var form_data = new FormData();
		form_data.append('action', 'withdrawal_accept_request');
		form_data.append('accept', accept);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				withdrawal_tab(event ,'accepted')
				jQuery('#accept_text'+accept).html("Accepted");
				jQuery('#reject_text'+accept).css("display", "none");
			}
		});
	});
	jQuery(document).on('click', ".paid_button", function(){
		var id = jQuery(this).data("id");
		// alert(id);
		if(id){
			var form_data = new FormData();
			form_data.append('action', 'withdrawal_accept_request');
			form_data.append('id', id);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					window.location = window.location;
				}
			});
		}
	});
	
	jQuery(document).on('click','.withdrawal_reject', function(){
		//alert(jQuery(this).val());
		var reject = jQuery(this).val();
		/* var txt = this.id; 
		alert(txt); */
		var form_data = new FormData();
		form_data.append('action', 'withdrawal_reject_request');
		form_data.append('reject', reject);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				withdrawal_tab(event ,'rejected')
				jQuery('#accept_text'+reject).css("display", "none");
				jQuery('#reject_text'+reject).html("Rejected");
			}
		});
	});
	
	//var page = 1; // What page we are on.
	jQuery(document).on('click','#withdrawal_more', function(){
		var status = jQuery(this).val();
		var page = jQuery('#page_value').val();
		//alert(status);
		//alert(page);
		var ppp = 2; // Record per page
		var offset = (page * ppp);
		jQuery("#withdrawal_more").attr("disabled",true); // Disable the button, temp.
		jQuery("#withdrawal_more").html("Loading...");
		var form_data = new FormData();
		form_data.append('action', 'withdrawal_status_tab');
		form_data.append('offset', offset);
		form_data.append('ppp', ppp);
		form_data.append('status', status);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				var page_no = parseInt(page) + 1;		
				var obj = jQuery.parseJSON(data);
				jQuery("#page_value").val(page_no);
				//var pag_val = obj.pagenav;
				//alert(pag_val);
							
				if(obj.pending_data != '' && obj.pending_data != null){
					jQuery('#customers').append(obj.pending_data);
					jQuery("#withdrawal_more").attr("disabled",false); // enable the button, temp.
					jQuery("#withdrawal_more").html("Load More");
				}else if(obj.accepted_data != '' && obj.accepted_data != null){
					jQuery('#customers').append(obj.accepted_data);
					jQuery("#withdrawal_more").attr("disabled",false); // enable the button, temp.
					jQuery("#withdrawal_more").html("Load More");
				}else if(obj.rejected_data != '' && obj.rejected_data != null){
					jQuery('#customers').append(obj.rejected_data);
					jQuery("#withdrawal_more").attr("disabled",false); // enable the button, temp.
					jQuery("#withdrawal_more").html("Load More");					
				}else{
					jQuery("#withdrawal_more").attr("disabled",true); // Disable the button, temp.
					jQuery("#withdrawal_more").html("No More Records");
				}
			}
		});
	});
	jQuery(document).on('change', 'input[name=add_a_note]', function() {
		if(this.checked) {
			jQuery('.add_a_note_div').show();
		}else{
			jQuery('.add_a_note_div').hide();
		}
	});
	jQuery(document).on('change', '.ewc-filter-type', function() {
	// $('.ewc-filter-cat').live('change', function(){
		var catFilter = jQuery(this).val();
		if( catFilter != '' ){
			if(catFilter == 'all'){
				document.location.href = 'admin.php?page=fmadmin_funnels';    
			}else{
				document.location.href = 'admin.php?page=fmadmin_funnels'+catFilter;    
			}
		}
	});
});

/*function withdrawal_tab(evt, withdrawal_status){
	  var status = withdrawal_status;
	  var form_data = new FormData();
      form_data.append('action', 'withdrawal_status_tab');
      form_data.append('status', status);
	  jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				/*alert("das");
				jQuery('#pending').html(data);*
				var obj = jQuery.parseJSON(data);
				jQuery('#pending').html(obj.pending_data);
				jQuery('#accepted').html(obj.accepted_data);
				jQuery('#rejected').html(obj.rejected_data);
			}
	  });
      var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(withdrawal_status).style.display = "block";
	  evt.currentTarget.className += " active"; 
}*/
function copy_emails_to_mailfm(post_id){
	if(post_id){
		// alert(post_id);
		jQuery('#btn_add_followup_emails_'+post_id).html("Adding..");
		jQuery('#btn_add_followup_emails_'+post_id).prop('disabled',true);
		var form_data = new FormData();
		form_data.append('action', 'add_followup_emails_to_mailserver');
		form_data.append('post_id', post_id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					window.location = window.location;
				}
			}
		});
	}
}
function open_reviws(id){
	tb_show('Funnel Reviews', 'admin-ajax.php?action=get_review_data_admin&data='+id+'&inlineId=funnel_reviews');
}
function open_feedback_insert(id){
	tb_show('Insert Feedback', 'admin-ajax.php?action=get_feedback_data_admin&data='+id+'&inlineId=feedback_thickbox&height=500');
}
function open_followupemails_insert(id){
	tb_show('Followup Emails', 'admin-ajax.php?action=get_followupemails_admin&data='+id+'&inlineId=followupemails_thickbox&height=500');
}
function open_promotionaltools(id){
	tb_show('Promotional Tools', 'admin-ajax.php?action=get_promotionaltools_admin&data='+id+'&inlineId=promotionaltools_thickbox&height=500');
}
function admin_affiliate_links(id){
	tb_show('Affiliate Request Links', 'admin-ajax.php?action=get_affiliaterequests_admin&data='+id+'&inlineId=affiliatelinks_thickbox&height=500');
}
function view_followupemail_admin(email_id){
	if(jQuery('#view_followupemail_'+email_id).hasClass("active")){
		jQuery('#view_followupemail_'+email_id).removeClass("active");
	}else{
		jQuery('#view_followupemail_'+email_id).addClass("active");
	}
	// tb_show('Followup Email', 'admin-ajax.php?action=get_single_followupemail&email_id='+email_id+'&inlineId=single_followupemail_thickbox&height=500');
}
function feedback_text_submit(id){
	// var texta = jQuery('#feedback_textarea_'+id).val();
	var array = [];
	if(jQuery('input[name=incomplete_email_sequence]').is(':checked')){
		array[0] = "incomplete_email_sequence";
	}
	if(jQuery('input[name=no_download_link]').is(':checked')){
		array[1] = "no_download_link";
	}
	if(jQuery('input[name=missing_optin_form]').is(':checked')){
		array[2] = "missing_optin_form";
	}
	if(jQuery('input[name=no_affiliate_links]').is(':checked')){
		array[3] = "no_affiliate_links";
	}
	if(jQuery('input[name=broken_affiliate_links]').is(':checked')){
		array[4] = "broken_affiliate_links";
	}
	if(jQuery('input[name=page_error]').is(':checked')){
		array[5] = "page_error";
	}
	if(jQuery('input[name=add_a_note]').is(':checked')){
		var texta = jQuery('#feedback_textarea_'+id).val();
		array[6] = "add_a_note";
		array[7] = texta;
	}
	if(array){
		jQuery('#feedback_text_submit_'+id).html("Adding..");
		jQuery('#feedback_text_submit_'+id).prop('disabled',true);
		var form_data = new FormData();
		form_data.append('action', 'save_requested_feedback');
		form_data.append('post_id', id);
		form_data.append('content', JSON.stringify(array));
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data == '1'){
					jQuery('#feedback_text_submit_'+id).html("Feedback Requested");
				}else{
					jQuery('#feedback_text_submit_'+id).html("Something went wrong..");
				}
			}
		});
	}
}
function force_review(id){
	if(id){
		if (!confirm('Are you sure?')) return false;
		var form_data = new FormData();
		form_data.append('action', 'force_review');
		form_data.append('post_id', id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				window.location = window.location;
			}
		});
	}
}
function approve_funnel(id){
	if(id){
		if (!confirm('Are you sure?')) return false;
		var form_data = new FormData();
		form_data.append('action', 'approve_funnel_final');
		form_data.append('post_id', id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				window.location = window.location;
			}
		});
	}
}
function settings_page_fmstripe(){
	
}
function free_funnel_submit(){
	// alert('here');
	var x = jQuery('form#frm_free_funnels').serializeArray(),dataObj = {};
	var newArray = x.filter(function(v){return v!==''});
	jQuery.each(newArray, function(i, field){
		dataObj[field.name] = field.value;
	});
	var month_free_funnel = dataObj['month_free_funnel'];
	var funnel_free_funnel = dataObj['funnel_free_funnel'];
	// alert(month_free_funnel);
	// alert(funnel_free_funnel);
	if(month_free_funnel && funnel_free_funnel){
		var form_data = new FormData();
		form_data.append('action', 'save_free_funnel');
		form_data.append('month_free_funnel', month_free_funnel);
		form_data.append('funnel_free_funnel', funnel_free_funnel);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					window.location = window.location;
				}
			}
		});
	}
	return false;
}