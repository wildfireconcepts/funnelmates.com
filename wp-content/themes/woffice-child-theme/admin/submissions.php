<h1>Submissions</h1>
<style>
.table_followup_emails{width:100%;border-collapse:collapse;}
.table_followup_emails td{padding:5px;}
</style>
<?php
global $wpdb, $automation_array;
$args = array(
    // 'meta_key' => 'is_funnel_published',
    // 'meta_value' => 'review',
    'post_type' => 'landing',
    'post_status' => 'publish',
    'posts_per_page' => -1,
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key'   => 'is_funnel_published',
			'value' => 'review'
		),
		array(
			'key'   => 'is_funnel_published',
			'value' => '1'
		),
		array(
			'key'   => 'is_funnel_published',
			'value' => '0'
		),
		array(
			'key'   => 'is_funnel_published',
			'value' => 'feedback_requested'
		),
		array(
			'key'   => 'is_funnel_published',
			'value' => 'triggered_review'
		),
		array(
			'key'   => 'is_funnel_published',
			'value' => 'resubmitted'
		)
	)
);
$posts = get_posts($args);
echo '<table class="table_followup_emails" border="1">';
	echo '<tr>';
		echo '<td>Funnel Name</td>';
		echo '<td>Funnel Link</td>';
		echo '<td>Automation Link</td>';
		echo '<td>List Link</td>';
		echo '<td>Action</td>';
	echo '</tr>';
foreach($posts as $p){
	echo '<tr>';
		echo '<td>';
			echo $p->post_title;
		echo '</td>';
		echo '<td>';
			echo '<a href="'.get_permalink($p->ID).'" target="_BLANK">'.get_permalink($p->ID).'</a>';
		echo '</td>';
		echo '<td>';
			$automation = get_post_meta($p->ID,'acelle_automation_uid',true);
			$automation = maybe_unserialize($automation);
			$automation_link = 'https://mail.funnelmates.com/automation2/'.$automation->uid.'/edit';
			if($automation->uid){
				echo '<a href="'.$automation_link.'" target="_BLANK">Automation</a>';
			}
		echo '</td>';
		echo '<td>';
			$list = get_post_meta($p->ID,'acelle_list_uid',true);
			$list_link = 'https://mail.funnelmates.com/lists/'.$list.'/overview';
			if($list){
				echo '<a href="'.$list_link.'" target="_BLANK">List</a>';
			}
		echo '</td>';
		echo '<td>';
			if($automation->uid && $list){
				$count_emails = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_followup_emails` WHERE funnel_id=".$p->ID);
				if($count_emails > 0){
					echo '<button id="btn_add_followup_emails_'.$p->ID.'" style="cursor:pointer;border: 0;background: green;color: white;padding: 10px;border-radius: 5px;font-weight: 600;" onclick="copy_emails_to_mailfm('.$p->ID.')" type="button">Add Emails to Automation ('.$count_emails.')</button>';
				}else{
					echo '<button style="border: 0;background: red;color: white;padding: 10px;border-radius: 5px;font-weight: 600;"  type="button">No Followup Emails ('.$count_emails.')</button>';
				}
			}
		echo '</td>';
	echo '</tr>';
}
echo '</table>';

/*exit;
$emails = $wpdb->get_results("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id=6035 ORDER BY type ASC");

$automation = get_post_meta(6035,'acelle_automation_uid',true);
$list = get_post_meta(6035,'acelle_list_uid',true);

$maildb = new wpdb("funnelma_mailu","RX.N1d~jwveA","funnelma_mailserver","localhost");
$automation = maybe_unserialize($automation);

$ms_automation = $maildb->get_row("SELECT * FROM `fmmsqautomation2s` WHERE uid='".$automation->uid."' ");
$ms_automation_id = $ms_automation->id;

$ms_list = $maildb->get_row("SELECT * FROM `fmmsqmail_lists` WHERE uid='".$list."' ");
$ms_list_id = $ms_list->id;

$random_first = rand(0,999999999);
$first = new stdClass();
$first->title = "New contact subscribes to mail list";
$first->id = "trigger";
$first->type = "ElementTrigger";
$first->options = new stdClass();
$first->options->key = "welcome-new-subscriber";
$first->options->type = "list-subscription";
$first->options->init = "true";
$first->child = $random_first;
$automation_array[] = $first;

$default_plain = " Is this email not displayed properly? Open the web viewPaste email content here Copyright © {CONTACT_NAME}, All rights reserved.{LIST_NAME}{CONTACT_ADDRESS_1},&nbsp;{CONTACT_ADDRESS_2},&nbsp;{CONTACT_CITY},&nbsp;{CONTACT_STATE}{CONTACT_COUNTRY}Want to change how you receive these emails?You can update your preferences or unsubscribe from this list ";

$default_content_start = '<html lang="en" data-bubblesppbfhhgcdehhbjigifndnfmcihegokbbprevent-events-conflict-installed="true"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><meta name="description" content=""><meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors"><meta name="generator" content="AcelleSystemLayouts"><!-- Bootstrap core CSS --><link href="css/bootstrap.min.css" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link rel="stylesheet" type="text/css" id="u0" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/skin.min.css"><link rel="stylesheet" type="text/css" id="u1" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/content.inline.min.css"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link rel="stylesheet" type="text/css" id="u0" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/skin.min.css"><link rel="stylesheet" type="text/css" id="u1" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/content.inline.min.css"></head><body class="vsc-initialized" spellcheck="false"><main role="main"><div class="py-5 bg-light" id="mce_15" style="position: relative; line-height: 1;" spellcheck="false"><div class="container">';
$default_content_end = '</div></div><div class="py-5 bg-light" id="mce_15" style="position: relative; line-height: 1;" spellcheck="false"><div class="container"><!-- <div class="text-center mb-4"><div><img src="image/logo.png" width="350px" /></div></div> --><p class="" id="mce_16" style="position: relative; line-height: 1;" spellcheck="false"><em class="">Copyright © {CONTACT_NAME}, All rights reserved.<br><br><br>{LIST_NAME}<br>{CONTACT_ADDRESS_1},&nbsp;{CONTACT_ADDRESS_2},&nbsp;{CONTACT_CITY},&nbsp;{CONTACT_STATE}<br>{CONTACT_COUNTRY}</em><br><br>Want to change how you receive these emails?<br>You can <a href="{UPDATE_PROFILE_URL}">update your preferences</a> or <a href="{UNSUBSCRIBE_URL}">unsubscribe from this list</a></p></div></div></main><footer class="text-muted py-5" id="mce_8" style="position: relative;" spellcheck="false"><div class="container">&nbsp;</div></footer></body></html>';
$child_wait = $random_first;
$previousValue = 0;
foreach($emails as $key=>$e){
	$content = addslashes($default_content_start.$e->content.$default_content_end);
	$email_uid = rand(0,999999999);
	$maildb->query("INSERT INTO fmmsqemails (`uid`,`automation2_id`,`subject`,`from`,`from_name`,`reply_to`,`content`,`created_at`,`updated_at`,`sign_dkim`,`track_open`,`track_click`,`action_id`,`plain`) VALUES ('".$email_uid."','".$ms_automation_id."','".addslashes($e->subject)."','".$ms_list->from_email."','".$ms_list->from_name."','".$ms_list->from_email."','".$content."',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,0,'".$random_first."','".$default_plain."');");
	if($previousValue != 0){
		$wait_days = $e->type - $previousValue;
		$child_wait = insert_wait_mail($child_wait,$wait_days);
	}
	$child_wait = insert_email_mail($email_uid, $child_wait);
	$previousValue = $e->type;
}
$data = json_encode($automation_array);
$maildb->query("UPDATE `fmmsqautomation2s` SET status='active', data='".$data."', updated_at=CURRENT_TIMESTAMP WHERE id='".$ms_automation_id."' ");*/