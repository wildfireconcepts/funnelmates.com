<div class="wrap">
	<div id="icon-users" class="icon32"></div>
	<h2>Free Rebill Funnel</h2>
	<div class="container">
		<?php
		global $wpdb;
		$array_month = array(
			"1" => "January",
			"2" => "February",
			"3" => "March",
			"4" => "April",
			"5" => "May",
			"6" => "June",
			"7" => "July",
			"8" => "August",
			"9" => "September",
			"10" => "October",
			"11" => "November",
			"12" => "December",
		);
		$free_funnels = $wpdb->get_results("SELECT * FROM `wp_setc_free_funnels` WHERE YEAR(date) = YEAR(CURDATE());");
		$table_free_funnels = '<table class="widefat fixed">';
		$array_selected_month = array();
		$table_free_funnels .= '<tr><th style="font-weight:900;">Month</th><th style="font-weight:900;">Funnel Name</th><th style="font-weight:900;">Action</th></tr>';
		foreach($free_funnels as $ff){
			$year = date('Y', strtotime($ff->date));
			if($year == date('Y')){
				array_push($array_selected_month,$ff->month);
			}
			$table_free_funnels .= '<tr>';
				$table_free_funnels .= '<td>'.$array_month[$ff->month].'</td>';
				$table_free_funnels .= '<td>'.get_the_title($ff->funnel_id).'</td>';
				$table_free_funnels .= '<td><a href="javascript:void(0);" data="'.$ff->id.'" class="delete_free_funnel">Delete</a></td>';
			$table_free_funnels .= '</tr>';
		}
		$table_free_funnels .= '</table>';
		$args = array(
			'numberposts' => -1,
			'post_type' => 'landing',
			'post_parent' => 0,
			'author' => 5,
			'orderby' => 'post_date',
			'order' => 'DESC',
		);
		$cindy_posts = get_posts($args);
		?>
		<form id="frm_free_funnels" onsubmit="return free_funnel_submit();">
			<table class="widefat fixed">
			<tr>
				<td>Month of Free Funnel</td>
				<td>
					<select name="month_free_funnel" id="month_free_funnel" required>
					<option value="" selected disabled>Select Month</option>
					<?php
					foreach($array_month as $key=>$am){
						if(in_array($key,$array_selected_month)){
							$disabled = 'disabled';
						}else{
							$disabled = '';
						}
						?>
						<option value="<?php echo $key; ?>" <?php echo $disabled; ?> ><?php echo $am; ?></option>
						<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Funnel</td>
				<td>
					<select name="funnel_free_funnel" id="funnel_free_funnel" required>
					<option value="" selected disabled>Select Funnel</option>
					<?php
					foreach($cindy_posts as $cposts){
						?>
						<option value="<?php echo $cposts->ID; ?>"><?php echo $cposts->post_title; ?></option>
						<?php
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Add Funnel" name="submit" id="add_funnel" class="button button-primary" />
				</td>
			</tr>
			</table>
		</form>
		<h1>Free Funnels For <?php echo date('Y'); ?></h1>
		<?php echo $table_free_funnels; ?>
	</div>
</div>