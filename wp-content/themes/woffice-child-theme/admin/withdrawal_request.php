<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  /*padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;*/
}
table.paypal, table.manual{
    border: 1px solid;
    width: 100%;
}
table.paypal th, table.manual th{
	text-align:left;
}
table.paypal td, table.manual td{
	padding: 10px;
    border: 1px solid;
}
.paid_button{
    border: none;
    background: green;
    color: white;
    padding: 5px 35px;
    font-weight: 600;
    border-radius: 10px;
	cursor:pointer;
}
.already_paid_button{
    border: none;
    background: red;
    color: white;
    padding: 5px 35px;
    font-weight: 600;
    border-radius: 10px;
	cursor:not-allowed;
}
.paid_row{background:#DDFFDD;}
.unpaid_row{background:#FFDDDD;}
</style>
<h1>Withdrawal Request</h1>
<input type="text" name="s_email" value="<?php echo isset($_GET['s_email']) ? $_GET['s_email'] : ''; ?>">
<a id="search_by_email" style="padding: 5px 10px; background-color: #4caf50; color: #FFF; border: none; cursor:pointer;">search</a>
<br><br><br>

<?php
global $wpdb;
$paypal_withdrawal_request = $wpdb->get_results(" SELECT * FROM `wp_withdrawal` WHERE type='1' "); 
$manual_withdrawal_request = $wpdb->get_results(" SELECT * FROM `wp_withdrawal` WHERE type='0' "); 
?>
<h3>Paypal Withdrawal Requests</h3>
<table class="paypal">
	<tr>
		<th>User ID</th>
		<th>Email</th>
		<th>Amount</th>
		<th>Action</th>
	</tr>
<?php
foreach($paypal_withdrawal_request as $p_w_r){
	if($p_w_r->request_process == '1'){
		$tr_class="paid_row";
	}else{
		$tr_class="unpaid_row";
	}
	?>
	<tr class="<?php echo $tr_class; ?>">
		<td><?php echo $p_w_r->user_id ?></td>
		<td><?php echo $p_w_r->email; ?></td>
		<td style="font-size: 30px;font-weight: 600;"><?php echo $p_w_r->request_amount; ?></td>
		<td style="width:15%;">
			<?php if($p_w_r->request_process == '1'){ ?>
				<button class="already_paid_button">Already Paid</button>
			<?php }else{ ?>
				<button class="paid_button" data-id="<?php echo $p_w_r->id; ?>">Mark as Paid</button>
			<?php } ?>
		</td>
	</tr>
	<?php 
}
?>
</table>
<h3>Bank Transfer Withdrawal Requests</h3>
<table class="manual">
	<tr>
		<th>User ID</th>
		<th>Name</th>
		<th>Email</th>
		<th>Home Address</th>
		<th>City</th>
		<th>Postal Code</th>
		<th>Bank Name</th>
		<th>Bank Address</th>
		<th>Account Name</th>
		<th>Account Number</th>
		<th>Swift Code</th>
		<th>IFSC Code</th>
		<th>Currency</th>
		<th>Amount</th>
		<th>Action</th>
	</tr>
	<?php
	foreach($manual_withdrawal_request as $m_w_r){ 
	if($m_w_r->request_process == '1'){
		$tr_class="paid_row";
	}else{
		$tr_class="unpaid_row";
	}
	?>
	<tr class="<?php echo $tr_class; ?>">
		<td><?php echo $m_w_r->user_id; ?></td>
		<td><?php echo $m_w_r->full_name; ?></td>
		<td><?php echo $m_w_r->email; ?></td>
		<td><?php echo $m_w_r->home_address; ?></td>
		<td><?php echo $m_w_r->city; ?></td>
		<td><?php echo $m_w_r->postal_code; ?></td>
		<td><?php echo $m_w_r->bank_name; ?></td>
		<td><?php echo $m_w_r->bank_address; ?></td>
		<td><?php echo $m_w_r->account_name; ?></td>
		<td><?php echo $m_w_r->account_number; ?></td>
		<td><?php echo $m_w_r->swift_code; ?></td>
		<td><?php echo $m_w_r->ifsc_code; ?></td>
		<td><?php echo $m_w_r->currency; ?></td>
		<td style="font-size: 30px;font-weight: 600;"><?php echo $m_w_r->request_amount; ?></td>
		<td style="width:15%;">
			<?php if($m_w_r->request_process == '1'){ ?>
				<button class="already_paid_button">Already Paid</button>
			<?php }else{ ?>
				<button class="paid_button" data-id="<?php echo $m_w_r->id; ?>">Mark as Paid</button>
			<?php } ?>
		</td>
	</tr>
	<?php 
}
?>
</table>
<?php
	/*$page = $_GET['pg'];
	<div class="tab">
  <button class="tablinks" onclick="withdrawal_tab(event ,'pending')">Pending</button>
  <button class="tablinks" onclick="withdrawal_tab(event ,'accepted')">Accepted</button>
  <button class="tablinks" onclick="withdrawal_tab(event ,'rejected')">Rejected</button>
</div>
<br><br>

<div id="pending" class="tabcontent">
 
</div>

<div id="accepted" class="tabcontent">
  
</div>

<div id="rejected" class="tabcontent">

</div>
	global $wpdb;
	$per_page = 10;
	$query = $wpdb->get_results("SELECT count(`id`) as 'num' FROM `wp_withdrawal`");
    $total = $query[0]->num;
    $lastpage = ceil($total/$per_page);
	if(isset($_GET['pg'])){
		$page = $_GET['pg'];
	}else{
		$page = 1;
	}
	$start = ($page - 1) * $per_page;       	
	$sql_page_query = "LIMIT $start, $per_page";
	//echo "SELECT * FROM `wp_withdrawal $sql_page_query";
	$search_query_part ='';
	if(isset($_GET['s_email']) && $_GET['s_email']!=''){
		$s_email = $_GET['s_email'];
	    $search_query_part = "where user_email LIKE '%".$s_email."%'";
	}
	$query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`$search_query_part $sql_page_query");
?>
	<table  id="customers" align="center" border="1" class="withdrawal_data">
	<tr class="head_title">
		<th>user ID</th>
		<th>user name</th>
		<th>user email</th>
		<th>user address</th>
		<th>amount</th>
		<th>action</th>
	</tr>
<?php
	foreach($query as $q){
		$w_id = $q->id;
		$id = $q->user_id;
		$email = $q->user_email;
		$name = $q->user_name;
		$address = $q->user_address;
		$amount = $q->request_amount;
		$amount_process = $q->request_process;
		if($amount_process == 0){
			$accept = "<button id='accept_text$w_id' class='withdrawal_accept btn_accept_withdrawal' value=".$w_id.">Accept</button>";
			$reject = "<button id='reject_text$w_id' class='withdrawal_reject btn_reject_withdrawal' value=".$w_id.">Reject</button>";
		}elseif($amount_process == 1){
			$accept = "<button class='btn_accept_withdrawal'>Accepted</button>";
			$reject  = "";
		}else{
			$accept = "";
			$reject = "<button class='btn_reject_withdrawal'>Rejected</button>";
		}
	?>
		<tr>
			<td><?php echo $id; ?></td>
			<td><?php echo $name; ?></td>
			<td><?php echo $email; ?></td>
			<td><?php echo $address; ?></td>
			<td><?php echo $amount; ?></td>
			<td>
				<span><?php echo $accept; ?></span>
				<span><?php echo $reject; ?></span>
			</td>
		</tr>
	<?php
	}
?>
	</table>
<?php
	$pagination = "";
	$prev = $page - 1;
	$next = $page + 1;
	$siteurl = "https://funnelmates.com/wp-admin/admin.php?page=withdrawal_reqiest&";
    if($lastpage > 1){   
        $pagination .= "<ul class='pagination'>";
             
            if ($page > 1) $pagination.= "<li><a href='{$siteurl}pg={$prev}'><<</a></li>";
             
            for ($counter = 1; $counter <= $lastpage; $counter++){
                if ($counter == $page)
                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
                else
                    $pagination.= "<li><a href='{$siteurl}pg={$counter}'>{$counter}</a></li>";                    
            }
         
            if ($page < $counter - 1) {
				$pagination.= "<li><a href='{$siteurl}pg={$next}'>>></a></li>";
				$pagination.= "<li><a href='{$siteurl}pg=$lastpage'>{$lastlabel}</a></li>";
			}
         
        $pagination.= "</ul>";        
    }
     
    echo $pagination; */
?>