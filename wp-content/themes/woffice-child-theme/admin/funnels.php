<div class="wrap">
<div id="icon-users" class="icon32"></div>
<h2>Funnels</h2>
<?php
add_thickbox();
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
class My_List_Table extends WP_List_Table {
	var $found_data = array();
	
	function get_columns(){
	  $columns = array(
		'cb'        => '<input type="checkbox" />',
		'funnel_status' => 'Funnel Status',
		'date_submitted'    => 'Date submitted',
		'member_name'      => 'Member name',
		'funnel_identifier'      => 'Funnel Identifier',
		'funnel_reviews'      => 'Funnel Reviews',
		'feedback'      => 'Add Feedback',
		'followup_emails'      => 'Followup Emails',
		'promo_tools'      => 'Promotional Tools',
		'approvea'      => 'Approve',
		'whitelabel_for'      => 'Whitelabel For',
	  );
	  return $columns;
	}
	function no_items() {
	  _e( 'No Funnels found, dude.' );
	}
	function prepare_items() {
	  $columns = $this->get_columns();
	  $hidden = array();
	  $sortable = $this->get_sortable_columns();
	  $this->_column_headers = array($columns, $hidden, $sortable);
	  usort( $this->table_data(), array( &$this, 'usort_reorder' ) );
	  
	  $per_page = $this->get_items_per_page('funnels_per_page', 5);
	  $current_page = $this->get_pagenum();
	  $total_items = count($this->table_data());
	  $this->found_data = array_slice($this->table_data(),(($current_page-1)*$per_page),$per_page);
	  $this->set_pagination_args( array(
		'total_items' => $total_items,
		'per_page'    => $per_page
	  ) );
	  
	  if(isset($_POST['s'])){
		$this->items = $this->table_data();
	  }else if(isset($_POST['action2'])){
		$this->items = $this->table_data();
	  }else{
		$this->items = $this->found_data;
	  }
	}
	function column_default( $item, $column_name ) {
		$is_funnel_published = get_post_meta($item['ID'],'is_funnel_published',true);
		$is_user_improvising = get_post_meta($item['ID'],'is_user_improvising',true);
		$is_whitelabel = get_post_meta($item['ID'],'is_whitelabel',true);
		$is_funnel_public = get_post_meta($item['ID'],'is_funnel_public',true);
	  switch( $column_name ) { 
		case 'cb':
		case 'funnel_status':
		case 'date_submitted':
		case 'member_name':
		case 'funnel_identifier':
			return $item[ $column_name ];
		case 'funnel_reviews':
			if($is_funnel_published == 'triggered_review'){
				return '<button type="button" onclick="open_reviws('.$item['ID'].')" class="button button-primary thickbox">'.$item[ $column_name ].'</button>';
			}else{
				return '';
			}
		case 'feedback':
			if($is_funnel_published == 'review' || $is_funnel_published == 'feedback_requested' || $is_funnel_published == 'triggered_review' || $is_funnel_published == 'resubmitted' || $is_user_improvising == '1'){
				return '<button type="button" onclick="open_feedback_insert('.$item['ID'].')" class="button button-primary thickbox">'.$item[ $column_name ].'</button>';
			}else{
				return '';
			}
		case 'followup_emails':
			if($item['email_sequence']){
				global $wpdb;
				
				$automation = get_post_meta($item['ID'],'acelle_automation_uid',true);
				$automation = maybe_unserialize($automation);
				$automation_uid = $automation->uid;
				
				// $maildb = new wpdb("mailuser","8CcKYRp<~7ZA","mail_server","localhost");
				$maildb = new wpdb("root","Secutv643!!sf##~~6rfa","mail_server","localhost");
				$ms_automation = $maildb->get_row("SELECT * FROM `fmmsqautomation2s` WHERE uid='".$automation->uid."' ");
				$automation_emails = $maildb->get_row("SELECT COUNT(*) as counter,updated_at FROM `fmmsqemails` WHERE automation2_id='".$ms_automation->id."' ");
				// return print_r($automation_emails,true);
				$count_emails = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_followup_emails` WHERE funnel_id=".$item['ID']);
				if($automation_emails->counter > 0){
					return '<button type="button" onclick="open_followupemails_insert('.$item['ID'].')" class="button button-primary thickbox">Update Emails ('.$count_emails.')</button><span>Last Updated at : '.$automation_emails->updated_at.'</span>';
				}else{
					return '<button type="button" onclick="open_followupemails_insert('.$item['ID'].')" class="button button-primary thickbox">'.$item[ $column_name ].'('.$count_emails.')</button>';
				}
			}else{
				return 'No Email Sequence';
			}
		case 'promo_tools';
			global $wpdb;
			$count_promo = $wpdb->get_var("SELECT COUNT(*) FROM `wp_fm_promotional_tools` WHERE post_id=".$item['ID']);
			return '<button type="button" onclick="open_promotionaltools('.$item['ID'].')" class="button button-primary thickbox">'.$item[ $column_name ].'('.$count_promo.')</button>';
		case 'approvea':
			if($is_funnel_published == 1){
				return '<button type="button" onclick="force_review('.$item['ID'].')" class="button button-primary thickbox">Force Review</button>';
			}else{
				return '<button type="button" onclick="approve_funnel('.$item['ID'].')" class="button button-primary thickbox">'.$item[ $column_name ].'</button>';
			}
		case 'whitelabel_for':
		//if($_SERVER["REMOTE_ADDR"]=='219.91.196.81'){	
			if($is_whitelabel == 1){
				if($is_funnel_public == 1){
					return 'Store Whitelabel';
				}else{
					return 'Personal Whitelabel';
				}				
			}else{
				return '-';
			}
		//}		
		default:
		  return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
	  }
	}
	function get_bulk_actions() {
	  $actions = array(
		'approve_selected'    => 'Approve',
	  );
	  return $actions;
	}
	function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="funnel_status[]" value="%s" />', $item['ID']
        );    
    }
	function extra_tablenav( $which ) {
		global $wpdb, $testiURL, $tablename, $tablet;
		$move_on_url = '&funnel-type=';
		if ( $which == "top" ){
			$selected_pending = $selected_resubmitted = $selected_notsubmitted = $selected_active = $selected_triggeredreview = $selected_feedbackrequested = '';
			if( $_GET['funnel-type'] == 'pending' ){
				$selected_pending = ' selected = "selected"';   
			}
			if( $_GET['funnel-type'] == 'resubmitted' ){
				$selected_resubmitted = ' selected = "selected"';   
			}
			if( $_GET['funnel-type'] == 'notsubmitted' ){
				$selected_notsubmitted = ' selected = "selected"';   
			}
			if( $_GET['funnel-type'] == 'active' ){
				$selected_active = ' selected = "selected"';   
			}
			if( $_GET['funnel-type'] == 'triggeredreview' ){
				$selected_triggeredreview = ' selected = "selected"';   
			}
			if( $_GET['funnel-type'] == 'feedbackrequested' ){
				$selected_feedbackrequested = ' selected = "selected"';   
			}
			?>
			<div class="alignleft actions bulkactions">
				<select name="funnel_type" class="ewc-filter-type">
					<option value="all">All Funnels</option>
					<option value="<?php echo $move_on_url; ?>pending" <?php echo $selected_pending; ?> >Pending</option>
					<option value="<?php echo $move_on_url; ?>resubmitted" <?php echo $selected_resubmitted; ?> >Resubmitted</option>
					<option value="<?php echo $move_on_url; ?>notsubmitted" <?php echo $selected_notsubmitted; ?> >Not Submitted</option>
					<option value="<?php echo $move_on_url; ?>active" <?php echo $selected_active; ?> >Active</option>
					<option value="<?php echo $move_on_url; ?>triggeredreview" <?php echo $selected_triggeredreview; ?> >Triggered Review</option>
					<option value="<?php echo $move_on_url; ?>feedbackrequested" <?php echo $selected_feedbackrequested; ?> >Feedback Requested</option>
				</select>
			</div>
			<?php
		}
		if ( $which == "bottom" ){
			//The code that goes after the table is there

		}
	}
	private function table_data(){
		global $wpdb;
        $data = array();
		if( $_GET['funnel-type'] == 'pending' ){
            // $query = $query . ' where cat_id=' . $_GET['cat-filter'];   
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'is_funnel_published',
					'value'   => "review",
					'compare' => 'LIKE',
				),
			),));
        }else if( $_GET['funnel-type'] == 'resubmitted' ){
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'is_funnel_published',
					'value'   => "resubmitted",
					'compare' => 'LIKE',
				),
			),));
		}else if( $_GET['funnel-type'] == 'notsubmitted' ){
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'is_funnel_published',
					'value'   => "",
					'compare' => '=',
				),
				'relation' => 'OR',
				array(
					'key'     => 'is_funnel_published',
					'value'   => "0",
					'compare' => '=',
				),
				array(
				 'key' => 'is_funnel_published',
				 'compare' => 'NOT EXISTS' // this should work...
				),
			),));
		}else if( $_GET['funnel-type'] == 'active' ){
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'is_funnel_published',
					'value'   => "1",
					'compare' => '=',
				),
			),));
		}else if( $_GET['funnel-type'] == 'triggeredreview' ){
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids',
			'meta_query' => array(
				array(
					'key'     => 'is_funnel_published',
					'value'   => "triggered_review",
					'compare' => 'LIKE',
				),
			),));
		}else if( $_GET['funnel-type'] == 'feedbackrequested' ){
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids',
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key'     => 'is_funnel_published',
					'value'   => "feedback_requested",
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'is_user_improvising',
					'value'   => "1",
					'compare' => '=',
				)
			),));
		}else{
			$wpb_all_query = new WP_Query(array('post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids'));
		}
		if ( $wpb_all_query->have_posts() ) :
			while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
				$is_funnel_published = get_post_meta(get_the_ID(),'is_funnel_published',true);
				$is_user_improvising = get_post_meta(get_the_ID(),'is_user_improvising',true);
				$review_date = get_post_meta(get_the_ID(),'review_date',true);
				$review_count = $wpdb->get_var("SELECT COUNT(*) FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id='".get_the_ID()."'");
				if(empty($review_date)){
					$review_date = get_the_date();
				}
				$author_id = get_post_field ('post_author', get_the_ID());
				$display_name = get_the_author_meta( 'display_name' , $author_id ); 
				$automation = get_post_meta(get_the_ID(),'acelle_automation_uid',true);
				$is_whitelabel = get_post_meta(get_the_ID(),'is_whitelabel',true);
				$is_funnel_public = get_post_meta(get_the_ID(),'is_funnel_public',true);
				$automation = maybe_unserialize($automation);
				$automation_link = $automation->uid;
				$active = false;
				if($is_funnel_published == 'review'){
					$funnel_status = '<span style="color:red;">Pending</span>';
					if($is_whitelabel == '1'){
						$funnel_status .= '<span style="font-weight: 600;font-size: 15px;color: #12AA00;"> [WHITELABEL] </span>';
						if($is_funnel_public == '1'){
							$funnel_status .= '<span style="font-weight: 600;font-size: 15px;color: #12AA00;"> [PUBLIC] </span>';
						}else{
							$funnel_status .= '<span style="font-weight: 600;font-size: 15px;color: #12AA00;"> [PRIVATE] </span>';
						}
					}
				}else if($is_funnel_published == 'feedback_requested' || $is_user_improvising == '1'){
					$funnel_status = '<span style="color:blue;">Feedback Requested</span>';
				}else if($is_funnel_published == 'resubmitted'){
					$funnel_status = '<span style="color:#c3c300;">Resubmitted</span>';
				}else if($is_funnel_published == 'triggered_review'){
					$funnel_status = '<span style="color:#c3c300;">Triggered Review</span>';
				}else if($is_funnel_published == 0 || $is_funnel_published == ''){
					$funnel_status = 'Not Submitted';
				}else if($is_funnel_published == 1){
					$active = true;
					$funnel_status = '<span style="color:green;">Active</span>';
				}
				$data[] = array(
					'ID' => get_the_ID(),
					'funnel_status' => $funnel_status,
					'date_submitted' => $review_date,
					'member_name' => $display_name,
					'funnel_identifier' => get_post_field( 'post_name', get_the_ID() ),
					'funnel_reviews' => 'Reviews ('.$review_count.')',
					'feedback' => 'Insert Feedback',
					'followup_emails' => 'Followup Emails',
					'promo_tools' => 'Tools',
					'approvea' => 'Approve',
					'email_sequence' => $automation_link,
				);
			endwhile;
		endif;
		return $data;
    }
	function get_sortable_columns() {
	  $sortable_columns = array(
		'funnel_status'  => array('funnel_status',false),
		'date_submitted' => array('date_submitted',false),
		'member_name'   => array('member_name',false),
		'funnel_identifier'   => array('funnel_identifier',false)
	  );
	  return $sortable_columns;
	}
	function usort_reorder( $a, $b ) {
	  // If no sort, default to title
	  $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'funnel_status';
	  // If no order, default to asc
	  $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';
	  // Determine sort order
	  $result = strcmp( $a[$orderby], $b[$orderby] );
	  // Send final sort direction to usort
	  return ( $order === 'asc' ) ? $result : -$result;
	}
	function column_funnel_status($item) {
		if($item['email_sequence']){
		  $actions = array(
				'landing_page'    => sprintf('<a href="%s" target="_BLANK">Landing Page</a>',get_permalink($item['ID'])),
				'thankyou_page'    => sprintf('<a href="%sthank-you" target="_BLANK">Thank You Page</a>',get_permalink($item['ID'])),
				'confirm_page'    => sprintf('<a href="%sconfirm" target="_BLANK">Confirm Page</a>',get_permalink($item['ID'])),
				'download_page'    => sprintf('<a href="%sdownload" target="_BLANK">Download Page</a>',get_permalink($item['ID'])),
				'email_sequence'    => sprintf('<a href="%s" target="_BLANK">Email Sequence</a>','https://mail.funnelmates.com/automation2/'.$item['email_sequence'].'/edit'),
				'affiliate_links'    => sprintf('<a href="javascript:void(0);" onclick="%s" >Affiliate Links</a>','admin_affiliate_links('.$item['ID'].')'),
			);
		}else{
			$actions = array(
				'landing_page'    => sprintf('<a href="%s" target="_BLANK">Landing Page</a>',get_permalink($item['ID'])),
				'thankyou_page'    => sprintf('<a href="%sthank-you" target="_BLANK">Thank You Page</a>',get_permalink($item['ID'])),
				'confirm_page'    => sprintf('<a href="%sconfirm" target="_BLANK">Confirm Page</a>',get_permalink($item['ID'])),
				'download_page'    => sprintf('<a href="%sdownload" target="_BLANK">Download Page</a>',get_permalink($item['ID'])),
				'affiliate_links'    => sprintf('<a href="javascript:void(0);" onclick="%s" >Affiliate Links</a>','admin_affiliate_links('.$item['ID'].')'),
			);
		}
	  return sprintf('%1$s %2$s', $item['funnel_status'], $this->row_actions($actions) );
	}
}

$myListTable = new My_List_Table();
$myListTable->prepare_items(); 
// $myListTable->search_box('search', 'search_id');
?>
<form id="events-filter" method="POST">
    <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
	<?php $myListTable->search_box('search', 'search_id'); 
	$myListTable->display(); 
	?>
</form>
<div id="funnel_reviews" style="display:none;"></div>
<div id="feedback_thickbox" style="display:none;"></div>
<div id="followupemails_thickbox" style="display:none;"></div>
<div id="promotionaltools_thickbox" style="display:none;"></div>
<div id="affiliatelinks_thickbox" style="display:none;"></div>
<?php /*<div id="single_followupemail_thickbox" style="display:none;"></div>*/ ?>
<style>div#TB_ajaxContent {width: 100% !important;padding: 0;text-align:center;}
div#TB_ajaxContent div.active {
    display: block !important;
}
</style>
</div>