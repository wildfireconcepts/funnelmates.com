const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const merge = require('merge-stream');
const concat = require('gulp-concat');
const del = require('del');
const uglify = require('gulp-uglify');
const replace = require('gulp-replace');
const gulpUtil = require('gulp-util');
const zip = require('gulp-zip');
const rename = require("gulp-rename");

const { version } = require('./package.json');
const distFolder = '../dist/';

// Style.css
gulp.task('main-css', function () {
    return gulp.src('./scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({ advanced: true }))
        .pipe(replace('2.9.3', version, { skipBinary: true }))
        .pipe(gulp.dest('./'));
});

// Assets.css
gulp.task('assets-css', function () {

    // Bootstrap
    var scssStream = gulp.src(['./scss/assets.scss'])
        .pipe(sass({
            outputStyle: 'nested',
            precison: 3,
            includePaths: [
                './node_modules/bootstrap/scss',
                './node_modules/@fortawesome/fontawesome-free/scss'
            ]
        }))
        .pipe(cleanCSS({ advanced: true }))
        .pipe(concat('scss-asssets.scss'));

    // Animate CSS & Bootstrap
    var cssStream = gulp.src([
        './node_modules/animate.css/animate.min.css'
    ])
        .pipe(concat('css-asssets.css'));

    return merge(scssStream, cssStream)
        .pipe(concat('assets.min.css'))
        .pipe(gulp.dest('./css'));

});

// Print.css
gulp.task('print-css', function () {

    return gulp.src(['./scss/print.scss'])
        .pipe(sass())
        .pipe(cleanCSS({ advanced: true }))
        .pipe(concat('print.min.css'))
        .pipe(gulp.dest('./css'));

});

// Backend.css
gulp.task('backend-css', function () {

    return gulp.src(['./scss/backend.scss'])
        .pipe(sass())
        .pipe(cleanCSS({ advanced: true }))
        .pipe(concat('backend.min.css'))
        .pipe(gulp.dest('./css'));

});

// Removing dest folder
gulp.task('clean', function () {
    return del([
        distFolder
    ], {
        force: true
    });
});

// Removing all ds_stores files
gulp.task('clean:ds_stores', function () {
    return del([
        '.DS_store',
        '*/.DS_store',
        '*/**/.DS_store'
    ]);
});

// Scripts.js
gulp.task('compress', function () {
    // returns a Node.js stream, but no handling of error messages
    return gulp.src([
        'js/vue.min.js',
        'js/plugins.js',
        'js/scripts.js',
        'js/addableItems.vue.js'
    ])
        .pipe(uglify().on('error', gulpUtil.log))
        .pipe(concat('woffice.min.js'))
        .pipe(gulp.dest('js/'));
});

// Dashboard.js
gulp.task('dashboard-js', function () {
    // returns a Node.js stream, but no handling of error messages
    return gulp.src([
        './node_modules/draggabilly/dist/draggabilly.pkgd.min.js',
        './node_modules/packery/dist/packery.pkgd.min.js'
    ])
        .pipe(uglify())
        .pipe(concat('dashboard.min.js'))
        .pipe(gulp.dest('js/'));
});

// Woffice core zip
gulp.task('zip:woffice-core', function () {
    return gulp.src(distFolder + version + '/woffice/inc/plugins/woffice-core/**')
        .pipe(rename(function (path) {
            path.dirname = 'woffice-core/' + path.dirname;
        }))
        .pipe(zip('woffice-core.zip'))
        .pipe(gulp.dest(distFolder + version + '/woffice/inc/plugins'))
})

// Woffice core zip
gulp.task('clean:woffice-core', function () {
    return del([distFolder + version + '/woffice/inc/plugins/woffice-core'], { force: true })
})

// Woffice zip
gulp.task('zip:woffice', function () {
    return gulp.src(distFolder + version + '/woffice/**')
        .pipe(rename(function (path) {
            path.dirname = 'woffice/' + path.dirname;
        }))
        .pipe(zip(`woffice-${version}.zip`))
        .pipe(gulp.dest(distFolder))
})

// Watch task
gulp.task('watch', function () {
    gulp.watch('./scss/**/*.scss', gulp.series('main-css', 'assets-css'));
    gulp.watch('./js/scripts.js', gulp.series('compress'));
    gulp.watch('./inc/plugins/woffice-core/extensions/**/*.js', gulp.series('compress'));
});

gulp.task('default', gulp.series(
    'clean',
    'main-css',
    'assets-css',
    'print-css',
    'backend-css',
    'clean:ds_stores',
    'compress',
    'dashboard-js'
));

// Build command
gulp.task('build', gulp.series('default', function () {
    return gulp.src([
        './!(node_modules|plugins|dist)/**/*',
        '!./node_modules/',
        '!./plugins/',
        '!./dist/',
        '!.idea',
        '!.DS_store',
        '!*/.DS_store',
        '!*/**/.DS_store',
        '!.git',
        '!.gitignore',
        '!/.gitignore',
        '!/**/.gitignore',
        '!.gitmodules',
        '!*/.gitmodules',
        './!*/**/.gitmodules',
        './!(*.log)'
    ], { base: "." })
        .pipe(replace('2.9.3', version, { skipBinary: true }))
        .pipe(gulp.dest(distFolder + version + '/woffice'))
}));

// Zip command
gulp.task('zip', gulp.series(['build', 'zip:woffice-core', 'clean:woffice-core', 'zip:woffice']));
